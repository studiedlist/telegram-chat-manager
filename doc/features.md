<cap>Features</cap>
<h1 features_overview>Overview</h1>
<h2 features_captcha>Captcha</h2>
Captcha protects chat from bots.

Images are generated in-place, with no external services involved, so you can tune captcha as you
wish.

For example, you can apply following filters as many times as you need:
- cow (negative-filled circles)
- dots
- grid
- noise
- wave

You can also change number of chars in captcha and image size.

<h2 features_report>Reports</h2>
User can report other chat members to admins with report command.

<note>Reporting is designed to be anonymous (neither reported user nor other chat members know about who
and which message reported)</note>

Reported message with other report information is saved in recent actions. Admins can take actions
manually or use buttons under report notification message (buttons differ depending on configuration)

<note>When buttons pressed by non-admins, nothing happens in order to keep report anonymous</note>

When action is performed with buttons, reason like `Banned due to report <report_id>` is
automatically generated and sent

<h2 features_join_request>Chat join requests</h2>
<note>This feature mostly makes sense for private supergroups. If your supergroup is public, user
can read its messages without joining</note>

Bot can handle join requests of your chat in private messages.

This feature is an alternative to <a #features_captcha>Captcha</a> bot protection. Main difference
between two is that with join requests potential bot cannot access neither group member list nor
messages until he solves captcha (or confirms that he is human another way according to your
configuration)

In order to make bot handle requests, send users bot link (in format of
`t.me/<chat_manager_nickname>?start=<your_chat_id>`) instead of chat link.

Feature workflow is the following:
1. User starts bot with special link
1. Bot sends chat join link and waits for user to send join request
1. User confirms that he is human using `method` specified in configuration (for example, with captcha)
1. Bot confirms chat join request if confirmation is successful, and rejects otherwise

<warn>Bot will not handle join requests he isn't waiting for</warn>

<h3 autocommands_confirmation>Confirmation methods</h3>
For now, there is only captcha confirmation method available.

Configuration is the same as in <a #features_captcha>Captcha</a> feature. If no configuration
present, configuration from captcha feature is used

<h2 features_autocommands>Autocommands</h2>
Autocommands perform actions when fired by trigger.

For example, one can configure bot for his chat in such a way that bot kicks users for sending voice
messages, or sends warning if message text is matched by a specified regex

Each autocommand has one trigger and one or more actions.

<h3 autocommands_performance>Performance</h3>
<note>According to our tests, bot can check 100 simple regex autocommands in < 3ms in release build
on 8th gen U-series Intel CPU</note>

There is no limit for autocommands count, yet bear in mind that too many autocommands with regex
trigger may slow down processing because each trigger is checked until there is a match for incoming
update

Other triggers are lightweight and should not affect performance

<h3 autocommands_fail_safety>Fail safety</h3>
Actions are not transactional nor parallel: if one fails, others would not be performed.

So you should place most important actions (like ban or mute) first in actions list

<h2 features_raidmode>Raid mode</h2>

If your chat is under active attack, activate raid mode.

While raid mode is active, anyone who joins the chat assumed to be an attacker and will be instantly banned

<warn>Raid mode state can be changed only by admins, by default it is off.
State of raid mode for all chats will be reset on restart</warn>

When state of raid mode changed bot will send message with hashtag `#raid`.
This message will contain user who changed state of raid mode.

Everytime someone get banned during raid mode, bot will send message with
`#raid_ban` hashtag.

