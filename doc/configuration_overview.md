<cap>Configuration</cap>
<h1 configuration_overview>Overview</h1>

<h2>Global configuration</h2>
Global configuration is managed manually by maintainer and read by bot at startup. Not only it
serves as configuration for chats with no per-chat config, but also as a base for per-chat
configuration.

In order to check global configuration, start bot. It will panic and print error message if there is
something wrong with config.

<h2>Per-chat configuration</h2>
Per-chat configuration is stored in `cfg.d` and should be managed automatically. It acts as an
extension to global configuration, so admins can reuse default (super) values if they don't need to
configure some features.

Admins can create and edit it via bot private messages. Per-chat config checked the same way as
global config when editing via bot, so per-chat config should always be valid.

<warn>Per-chat configuration is not checked at startup, be careful when editing it
manually</warn>
