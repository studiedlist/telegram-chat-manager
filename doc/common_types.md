<h1>Commonly used types</h1>
<h2>Pattern</h2>
Pattern is a string with placeholders for runtime variables.<br>
Placeholders format is similar to rust format rules: `"{var_name}"`<br>
If there is only one available variable left (either because pattern have only one or you have used
others before), you can omit name and write `"{}"`

<h3 pattern_examples>Examples</h3>
<h4>Pattern with one variable</h4>
Say we have a pattern called `Username` with the only variable called `username`<br>
The simplest thing we can do is: `"Hello {username}!"`<br>
We can also omit variable name (since there are no other): `"Hello {}!"`<br>

<h4>Pattern with multiple variables</h4>
Say we have a pattern called `Counter` with variables `name`, `current` and `max`<br>
Now we cannot do `"Hello {}!"` since there are three variables fitting this placeholder, so we must
write `"Hello {name}!"`<br>
<note>You don't have to use all pattern variables</note>

<h2>Duration</h2>
Duration format is `X[y|w|d|h|m|s]*`. Where `X` is any number followed by one of: <b>y</b>ears, <b>w</b>eeks, <b>d</b>ays, <b>h</b>ours, <b>m</b>inutes, <b>s</b>econds. This sequence can be repeated any number of times.<br>
Plain number represents seconds
<h3 duration_examples>Examples</h3>
`10s` — 10 seconds<br>
`65s` — 1 minute 5 seconds<br>
`10m3s` — 10 minutes 3 seconds<br>
