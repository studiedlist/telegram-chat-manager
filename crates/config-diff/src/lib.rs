use chrono::Duration;
use std::fmt::Error;
use std::fmt::Write;

pub mod macros {
    pub use config_diff_macros::Diff;
}

#[derive(Debug)]
pub enum DiffState<T> {
    Unchanged,
    Changed(T, T),
    FromEmpty(T),
    ToEmpty(T),
}

impl<T: PartialEq> From<(T, T)> for DiffState<T> {
    fn from((a, b): (T, T)) -> Self {
        if a == b {
            Self::Unchanged
        } else {
            Self::Changed(a, b)
        }
    }
}

pub fn diff_to_string<D: Differ, T: Diff>(a: &T, b: &T) -> Result<String, std::fmt::Error> {
    let mut res = String::new();
    a.diff::<D, _>(b, &mut res)?;
    Ok(res)
}

pub trait Diff {
    fn diff<D: Differ, W: Write>(&self, other: &Self, w: &mut W) -> Result<(), Error>;

    fn has_diff(&self, other: &Self) -> bool
    where
        Self: PartialEq,
    {
        self != other
    }

    fn diff_named<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        name: &str,
    ) -> Result<(), Error> {
        w.write_fmt(format_args!("{name}: "))?;
        self.diff::<D, W>(other, w)
    }

    fn diff_indent<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        indent: usize,
    ) -> Result<(), Error> {
        w.write_fmt(format_args!("{:indent$}", " "))?;
        Self::diff::<D, W>(self, other, w)
    }

    fn diff_named_indent<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        name: &str,
        indent: usize,
    ) -> Result<(), Error> {
        w.write_fmt(format_args!("{:indent$}", " "))?;
        Self::diff_named::<D, W>(self, other, w, name)
    }

    fn entry(&self) -> String;
}

impl<T: Diff> Diff for &T {
    fn diff<D: Differ, W: Write>(&self, other: &Self, w: &mut W) -> Result<(), Error> {
        (*self).diff::<D, W>(other, w)
    }
    fn entry(&self) -> String {
        (*self).entry()
    }
}

macro_rules! primitive_diff{
    ($($type:ty),*) => {
        $(
            impl Diff for $type {
                fn diff<D: Differ, W: Write>(&self, other: &Self, w: &mut W) -> Result<(), Error> {
                    D::write_state(&(self, other).into(), w)
                }
                fn entry(&self) -> String {
                    format!("{self:?}")
                }
            }
        )*
    }
}

impl<T: Diff> Diff for Option<T> {
    fn diff<D: Differ, W: Write>(&self, other: &Self, w: &mut W) -> Result<(), Error> {
        let state = match (self, other) {
            (Some(a), Some(b)) => return a.diff::<D, W>(b, w),
            (Some(a), None) => DiffState::ToEmpty(a),
            (None, Some(b)) => DiffState::FromEmpty(b),
            (None, None) => DiffState::Unchanged,
        };
        D::write_state(&state, w)
    }
    fn diff_indent<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        indent: usize,
    ) -> Result<(), Error> {
        let state = match (self, other) {
            (Some(a), Some(b)) => return a.diff_indent::<D, W>(b, w, indent + 1),
            (Some(a), None) => DiffState::ToEmpty(a),
            (None, Some(b)) => DiffState::FromEmpty(b),
            (None, None) => DiffState::Unchanged,
        };
        D::write_state(&state, w)
    }
    fn diff_named_indent<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        name: &str,
        indent: usize,
    ) -> Result<(), Error> {
        match (self, other) {
            (Some(a), Some(b)) => a.diff_named_indent::<D, W>(b, w, name, indent),
            _ => {
                w.write_fmt(format_args!("{:indent$}{name}: ", " "))?;
                Self::diff_indent::<D, W>(self, other, w, indent)
            }
        }
    }
    fn entry(&self) -> String {
        match self {
            Some(a) => a.entry(),
            None => "none".to_string(),
        }
    }
}

impl<T: Diff> Diff for Vec<T> {
    fn diff<D: Differ, W: Write>(&self, other: &Self, w: &mut W) -> Result<(), Error> {
        w.write_str("[")?;
        for (a, b) in self.iter().zip(other.iter()) {
            w.write_str("\n ")?;
            a.diff::<D, W>(b, w)?;
        }
        w.write_str("\n]")?;
        Ok(())
    }
    fn diff_indent<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        indent: usize,
    ) -> Result<(), Error> {
        w.write_fmt(format_args!("{:indent$}[", " "))?;
        for (a, b) in self.iter().zip(other.iter()) {
            w.write_fmt(format_args!("\n{:width$}", " ", width = indent + 1))?;
            a.diff::<D, W>(b, w)?;
        }
        w.write_fmt(format_args!("\n{:indent$}]", " "))?;
        Ok(())
    }
    fn diff_named_indent<D: Differ, W: Write>(
        &self,
        other: &Self,
        w: &mut W,
        name: &str,
        indent: usize,
    ) -> Result<(), Error> {
        w.write_fmt(format_args!("{:indent$}{name}:\n", " "))?;
        Self::diff_indent::<D, W>(self, other, w, indent)
    }
    fn entry(&self) -> String {
        let s: Vec<_> = self.iter().map(Diff::entry).collect();
        format!("{s:?}")
    }
}

impl<T> Diff for std::marker::PhantomData<T> {
    fn diff<D: Differ, W: Write>(&self, _other: &Self, _w: &mut W) -> Result<(), Error> {
        Ok(())
    }
    fn diff_named_indent<D: Differ, W: Write>(
        &self,
        _other: &Self,
        _w: &mut W,
        _name: &str,
        _indent: usize,
    ) -> Result<(), Error> {
        Ok(())
    }
    fn entry(&self) -> String {
        String::new()
    }
}

impl<T: Diff> Diff for (T, T) {
    fn diff<D: Differ, W: Write>(&self, _other: &Self, _w: &mut W) -> Result<(), Error> {
        Ok(())
    }
    fn entry(&self) -> String {
        format!("({}, {})", self.0.entry(), self.1.entry())
    }
}

primitive_diff! {
    &str,
    String,
    usize,
    u32,
    u16,
    u8,
    bool,
    Duration
}

pub trait Differ {
    fn write_state<T: Diff, W: Write>(state: &DiffState<T>, w: &mut W) -> Result<(), Error> {
        match state {
            DiffState::Changed(from, to) => {
                w.write_fmt(format_args!("{} -> {}", from.entry(), to.entry()))
            }
            DiffState::Unchanged => w.write_str("unchanged"),
            DiffState::FromEmpty(to) => w.write_fmt(format_args!("None -> {}", to.entry())),
            DiffState::ToEmpty(from) => w.write_fmt(format_args!("{} -> None", from.entry())),
        }
    }
}

pub struct DefaultDiffer;

impl Differ for DefaultDiffer {}

#[cfg(test)]
mod tests;
