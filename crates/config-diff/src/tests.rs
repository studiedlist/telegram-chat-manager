use crate::diff_to_string;
use crate::macros::Diff;
use crate::DefaultDiffer;
use crate::Diff;
use crate::Differ;
use std::fmt;

fn diff<T: Diff>(a: T, b: T) -> Result<String, fmt::Error> {
    diff_to_string::<DefaultDiffer, T>(&a, &b)
}

#[test]
fn diffs_strings() {
    let a = "foo".to_string();
    let b = "bar".to_string();

    assert_eq!("\"foo\" -> \"bar\"", diff(a, b).unwrap());
}

#[test]
fn diffs_some_and_some() {
    let a = Some("foo".to_string());
    let b = Some("bar".to_string());

    assert_eq!("\"foo\" -> \"bar\"", diff(a, b).unwrap());
}

#[test]
fn diffs_some_and_none() {
    let a = Some("foo".to_string());
    let b = None;

    assert_eq!("\"foo\" -> None", diff(a, b).unwrap());
}

#[test]
fn diffs_none_and_none() {
    let a: Option<String> = None;
    let b: Option<String> = None;

    assert_eq!("unchanged", diff(a, b).unwrap());
}

#[test]
fn diffs_struct() {
    #[derive(Diff, PartialEq)]
    struct Foo {
        foo: usize,
        abc: &'static str,
    }
    let a = Foo { foo: 0, abc: "a" };
    let b = Foo { foo: 0, abc: "b" };

    let expected = "\n foo: unchanged\n abc: \"a\" -> \"b\"";

    assert_eq!(expected, diff(a, b).unwrap());
}

#[test]
fn diffs_nested_struct() {
    #[derive(Diff, PartialEq)]
    struct Foo {
        foo: usize,
        bar: Bar,
    }

    #[derive(Debug, PartialEq, Diff)]
    struct Bar {
        abc: &'static str,
    }
    let a = Foo {
        foo: 0,
        bar: Bar { abc: "a" },
    };
    let b = Foo {
        foo: 0,
        bar: Bar { abc: "b" },
    };

    let expected = "\n foo: unchanged\n bar\n  abc: \"a\" -> \"b\"";

    assert_eq!(expected, diff(a, b).unwrap());
}

#[test]
fn diffs_option_field() {
    #[derive(Diff, PartialEq)]
    struct Foo {
        foo: usize,
        bar: Option<Bar>,
    }

    #[derive(Debug, PartialEq, Diff)]
    struct Bar {
        abc: &'static str,
    }
    let a = Foo {
        foo: 0,
        bar: Some(Bar { abc: "a" }),
    };
    let b = Foo {
        foo: 0,
        bar: Some(Bar { abc: "b" }),
    };

    let expected = "\n foo: unchanged\n bar\n  abc: \"a\" -> \"b\"";

    assert_eq!(expected, diff(a, b).unwrap());
}

#[test]
fn diffs_enum() {
    #[derive(Diff, PartialEq, Debug)]
    enum Foo {
        Bar,
        Abc,
    }
    let a = Foo::Bar;
    let b = Foo::Abc;

    assert_eq!("Foo::Bar -> Foo::Abc", diff(a, b).unwrap());
}

#[test]
fn diffs_option_enum() {
    #[derive(Diff, PartialEq, Debug)]
    enum Foo {
        Bar,
        Abc,
    }
    let a = Some(Foo::Bar);
    let b = Some(Foo::Abc);

    assert_eq!("Foo::Bar -> Foo::Abc", diff(a, b).unwrap());
}

#[test]
fn diffs_struct_variant() {
    #[derive(Diff, PartialEq, Debug)]
    enum Foo {
        Bar { bar: &'static str },
        Abc,
    }
    let a = Foo::Bar { bar: "bar" };
    let b = Foo::Abc;

    assert_eq!("Foo::Bar { bar: \"bar\" } -> Foo::Abc", diff(a, b).unwrap());
}

#[test]
fn diffs_tuple_variant() {
    #[derive(Diff, PartialEq, Debug)]
    enum Foo {
        Bar(&'static str),
        Abc,
    }
    let a = Foo::Bar("bar");
    let b = Foo::Abc;

    assert_eq!("Foo::Bar(\"bar\") -> Foo::Abc", diff(a, b).unwrap());
}

#[test]
fn diffs_vec() {
    let a = vec!["a", "b", "c"];
    let b = vec!["b", "b", "c"];

    let expected = "[\n \"a\" -> \"b\"\n unchanged\n unchanged\n]";

    assert_eq!(expected, diff(a, b).unwrap());
}

#[test]
fn diffs_with_indent() {
    let a = vec!["a", "b", "c"];
    let b = vec!["b", "b", "c"];

    let mut res = String::new();
    a.diff_indent::<DefaultDiffer, _>(&b, &mut res, 2).unwrap();
    let expected = "  [\n   \"a\" -> \"b\"\n   unchanged\n   unchanged\n  ]";
    assert_eq!(expected, res);
}

#[test]
fn collapses_unchanged_struct() {
    #[derive(Diff, PartialEq)]
    struct Foo {
        foo: usize,
        bar: Option<Bar>,
    }

    #[derive(Clone, Debug, PartialEq, Diff)]
    struct Bar {
        abc: &'static str,
    }

    let bar = Some(Bar { abc: "test" });
    let a = Foo {
        foo: 1,
        bar: bar.clone(),
    };
    let b = Foo { foo: 2, bar };
    let expected = "\n foo: 1 -> 2\n bar: unchanged";
    assert_eq!(expected, diff(a, b).unwrap());
}
