use proc_macro2::Ident;
use proc_macro2::Span;
use quote::quote;
use syn::Type;
use syn::parse_macro_input;
use syn::Data;
use syn::DeriveInput;
use syn::Field;
use syn::Fields;

#[proc_macro_derive(Extension, attributes(extend))]
pub fn extension_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    match &input.data {
        Data::Struct(_) => derive_struct(&input),
        //Data::Enum(_) => derive_enum(&input),
        _ => panic!("Only structs and enums are supported"),
    }
}

fn derive_struct(input: &DeriveInput) -> proc_macro::TokenStream {
    let name = &input.ident;
    let fields: Vec<_> = get_fields(&input.data).into_iter().collect();
    let types: Vec<_> = fields
        .iter()
        .filter(|x| match &x.ty {
            Type::Path(p) => p.path.segments.first().unwrap().ident != "Option",
            _ => false,
        })
        .map(|x| &x.ty)
        .collect();
    let idents: Vec<_> = fields
        .iter()
        .filter(|x| match &x.ty {
            Type::Path(p) => p.path.segments.first().unwrap().ident != "Option",
            _ => false,
        })
        .map(|x| x.ident.as_ref().unwrap())
        .collect();
    let optional_types: Vec<_> = fields
        .iter()
        .filter(|x| match &x.ty {
            Type::Path(p) => p.path.segments.first().unwrap().ident == "Option",
            _ => false,
        })
        .map(|x| &x.ty)
        .collect();
    let optional_idents: Vec<_> = fields
        .iter()
        .filter(|x| match &x.ty {
            Type::Path(p) => p.path.segments.first().unwrap().ident == "Option",
            _ => false,
        })
        .map(|x| x.ident.as_ref().unwrap())
        .collect();
    let extension = Ident::new(&(name.to_string() + "Extension"), Span::call_site());
    let expanded = quote! {
        #[derive(serde::Serialize, serde::Deserialize, Clone)]
        pub struct #extension {
            #(
                #idents: config_extension::Entry<#types>,
            )*
            #(
                #optional_idents: config_extension::OptionalEntry<#optional_types>,
            )*
        }

        impl Extender<#name> for #extension {
            type Error = anyhow::Error;

            fn try_extend(self, t: &#name) -> Result<#name, Self::Error> {
                Ok(#name {
                    #(
                        #idents: self.#idents.option().unwrap_or_else(|| t.#idents.clone()),
                    )*
                    #(
                        #optional_idents: self.#optional_idents.unzip().flatten().unwrap_or_else(|| t.#optional_idents.clone()),
                    )*
                })
            }
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn get_fields(data: &Data) -> Vec<&Field> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => fields.named.iter().collect(),
            _ => panic!("Only named fields are supported"),
        },
        _ => panic!("Only structs are supported"),
    }
}
