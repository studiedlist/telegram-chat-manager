use super::Pattern;
use super::Argument;
use super::Arguments;
use non_empty_vec::ne_vec;
use non_empty_vec::NonEmpty;

#[derive(Debug)]
struct OneArgument {
    name: String,
}
impl Arguments for OneArgument {
    fn names() -> NonEmpty<Argument> {
        ne_vec![Argument::new("name".to_string()).unwrap()]
    }
    fn by_argument(&self, arg: &Argument) -> &str {
        match arg.inner() {
            "name" => &self.name,
            _ => unreachable!(),
        }
    }
}
struct MultipleArguments {
    name1: String,
    name2: String,
}
impl Arguments for MultipleArguments {
    fn names() -> NonEmpty<Argument> {
        ne_vec![
            Argument::new("name1".to_string()).unwrap(),
            Argument::new("name2".to_string()).unwrap(),
        ]
    }
    fn by_argument(&self, arg: &Argument) -> &str {
        match arg.inner() {
            "name1" => &self.name1,
            "name2" => &self.name2,
            _ => unreachable!(),
        }
    }
}

#[test]
fn one_arg() {
    let s = "Hello {name}".to_string();
    let pattern: Pattern<OneArgument> = s.try_into().unwrap();
    assert_eq!(
        "Hello world",
        pattern.apply(OneArgument {
            name: "world".to_string()
        })
    );
}

#[test]
fn invalid_arg() {
    let s = "Hello {n}".to_string();
    let pattern: Result<Pattern<OneArgument>, _> = s.try_into();
    assert!(pattern.is_err());

    let s = "Hello {name} and {n}".to_string();
    let pattern: Result<Pattern<OneArgument>, _> = s.try_into();
    assert!(pattern.is_err());
}

#[test]
fn empty_valid_arg() {
    let s = "Hello {}".to_string();
    let pattern: Pattern<OneArgument> = s.try_into().unwrap();
    assert_eq!(
        "Hello world",
        pattern.apply(OneArgument {
            name: "world".to_string()
        })
    );
}

#[test]
fn multiple_args() {
    let s = "Hello {name1} and {name2}".to_string();
    let pattern: Pattern<MultipleArguments> = s.try_into().unwrap();
    assert_eq!(
        "Hello world and rust",
        pattern.apply(MultipleArguments {
            name1: "world".to_string(),
            name2: "rust".to_string(),
        })
    );
}

#[test]
fn multiple_with_valid_empty() {
    let s = "Hello {} and {name2}".to_string();
    let pattern: Pattern<MultipleArguments> = s.try_into().unwrap();
    assert_eq!(
        "Hello world and rust",
        pattern.apply(MultipleArguments {
            name1: "world".to_string(),
            name2: "rust".to_string(),
        })
    );
}

#[test]
fn multiple_with_invalid_empty() {
    let s = "Hello {} and {}".to_string();
    let pattern: Result<Pattern<MultipleArguments>, _> = s.try_into();
    assert!(pattern.is_err());
}
