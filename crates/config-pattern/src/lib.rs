pub mod macros {
    pub use config_pattern_macros::Args;
}

#[cfg(test)]
mod tests;

use anyhow::anyhow;
use anyhow::Error;
use config_diff::Diff;
use config_diff::Differ;
use derive_more::Deref;
use derive_more::Display;
use itertools::Itertools;
use lazy_regex::lazy_regex;
use lazy_regex::Lazy;
use lazy_regex::Regex;
use non_empty_vec::NonEmpty;
use serde::Deserialize;
use serde::Serialize;
use std::fmt;
use std::marker::PhantomData;
use std::num::NonZeroUsize;
use std::str::FromStr;

static PATTERN_REGEX: Lazy<Regex> = lazy_regex!(r"\{[a-z0-9_]+\}");
static EMPTY_PATTERN_REGEX: Lazy<Regex> = lazy_regex!(r"\{\}");

pub trait Arguments {
    fn names() -> NonEmpty<Argument>;
    fn by_argument(&self, a: &Argument) -> &str;
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
#[serde(try_from = "String")]
#[serde(into = "String")]
pub struct Pattern<A: Arguments> {
    text: String,
    #[serde(skip)]
    _p: PhantomData<A>,
}

impl<A: Arguments> fmt::Display for Pattern<A> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.text.fmt(f)
    }
}

impl<A: Arguments> Diff for Pattern<A> {
    fn diff<D: Differ, W: std::fmt::Write>(
        &self,
        other: &Self,
        w: &mut W,
    ) -> Result<(), std::fmt::Error> {
        D::write_state(&(&self.text, &other.text).into(), w)?;
        Ok(())
    }
    fn diff_named_indent<D: Differ, W: std::fmt::Write>(
        &self,
        other: &Self,
        w: &mut W,
        name: &str,
        indent: usize,
    ) -> Result<(), std::fmt::Error> {
        for _ in 0..indent {
            w.write_str(" ")?;
        }
        Self::diff_named::<D, W>(self, other, w, name)
    }
    fn entry(&self) -> String {
        self.text.entry()
    }
}

impl<A: Arguments> Pattern<A> {
    pub fn apply<T: Into<A>>(&self, a: T) -> String {
        let a = a.into();
        let mut text = self.text.clone();
        let names = A::names();
        if names.len() == NonZeroUsize::try_from(1).unwrap() {
            let n = names.first();
            let value = a.by_argument(n);
            text = text
                .replace(&format!("{{{n}}}"), value)
                .replace("{}", value);
        } else {
            let names: Vec<_> = names.into_iter().map(|x| x.into_inner()).collect();
            let parsed: Vec<_> = parse_names(&text).filter(|x| names.contains(x)).collect();
            let diff = names.len() - parsed.iter().filter(|x| names.contains(x)).unique().count();
            for n in A::names() {
                let value = a.by_argument(&n);
                if diff == 1 && !parsed.contains(&n) {
                    text = text.replace("{}", value);
                } else {
                    text = text.replace(&format!("{{{n}}}"), value)
                }
            }
        }
        text
    }
    pub fn text(&self) -> &str {
        &self.text
    }
}

#[derive(Deref, Display, PartialEq, Eq)]
pub struct Argument(String);

impl Argument {
    pub fn new(s: String) -> Option<Self> {
        if s.is_empty() {
            None
        } else {
            Some(Self(s))
        }
    }
    pub fn into_inner(self) -> String {
        self.0
    }
    pub fn inner(&self) -> &str {
        &self.0
    }
}

impl<A: Arguments> FromStr for Pattern<A> {
    type Err = <Self as TryFrom<String>>::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.to_string().try_into()
    }
}

impl<A: Arguments> TryFrom<String> for Pattern<A> {
    type Error = Error;
    fn try_from(text: String) -> Result<Self, Error> {
        let names = A::names();
        let names: Vec<_> = names.into_iter().map(|x| x.into_inner()).collect();
        let parsed: Vec<_> = parse_names(&text).collect();
        let diff = names.len() - parsed.iter().filter(|x| names.contains(x)).unique().count();
        let empty_count = EMPTY_PATTERN_REGEX.find_iter(&text).count();
        if empty_count != 0 && diff > 1 {
            let not_present: Vec<_> = names.iter().filter(|n| !parsed.contains(n)).collect();
            return Err(anyhow!(
                "Ambiguous pattern {{}}\nIt could refer to either of: {not_present:?}"
            ));
        }
        for p in parsed {
            if !names.contains(&p) {
                return Err(anyhow!(
                    "Unknown name: \"{}\"\nAvailable names are: {names:?}",
                    p
                ));
            }
        }
        Ok(Pattern {
            text,
            _p: PhantomData,
        })
    }
}

impl<A: Arguments> TryFrom<&str> for Pattern<A> {
    type Error = Error;
    fn try_from(text: &str) -> Result<Self, Error> {
        text.to_string().try_into()
    }
}

impl<A: Arguments> From<Pattern<A>> for String {
    fn from(val: Pattern<A>) -> Self {
        val.text
    }
}

impl<A: Arguments> Clone for Pattern<A> {
    fn clone(&self) -> Self {
        Self {
            text: self.text.clone(),
            _p: PhantomData,
        }
    }
}

fn parse_names(s: &str) -> impl Iterator<Item = String> + '_ {
    PATTERN_REGEX
        .find_iter(s)
        .map(|x| x.as_str().replace(['{', '}'], ""))
}
