use quote::quote;
use syn::parse_macro_input;
use syn::parse_quote;
use syn::Data;
use syn::DeriveInput;
use syn::Field;
use syn::Fields;
use syn::GenericParam;
use syn::Generics;

#[proc_macro_derive(Args)]
pub fn arguments_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let fields: Vec<_> = get_fields(&input.data)
        .into_iter()
        .map(|x| x.ident.as_ref().unwrap())
        .collect();

    let generics = add_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let expanded = quote! {
        impl #impl_generics config_pattern::Arguments for #name #ty_generics #where_clause {
            fn names() -> non_empty_vec::NonEmpty<Argument> {
                non_empty_vec::ne_vec![
                    #(
                    Argument::new(stringify!(#fields).to_string()).unwrap(),
                    )*
                ]
            }
            fn by_argument(&self, a: &Argument) -> &str {
                match a.inner() {
                    #(
                    stringify!(#fields) => &self.#fields,
                    )*
                    _ => unreachable!()
                }
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

fn get_fields(data: &Data) -> Vec<&Field> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => fields.named.iter().collect(),
            _ => panic!("Only named fields are supported"),
        },
        _ => panic!("Only structs are supported"),
    }
}

fn add_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(heapsize::HeapSize));
        }
    }
    generics
}
