use serde::de;
use serde::de::Visitor;
use serde::Deserialize;
use serde::Deserializer;
use serde::Serialize;
use serde::Serializer;
use std::fmt;
use std::marker::PhantomData;
use std::str::FromStr;

pub use config_extension_macros::Extension;

pub trait Extendable<T>
where
    Self: Sized,
{
    type Error;

    fn try_extend_with(self, t: &T) -> Result<Self, Self::Error>;
}

pub trait Extender<T> {
    type Error;

    fn try_extend(self, t: &T) -> Result<T, Self::Error>;
}

impl<T: Clone> Extender<T> for Entry<T> {
    type Error = anyhow::Error;

    fn try_extend(self, t: &T) -> Result<T, Self::Error> {
        match self {
            Entry::Super => Ok(t.clone()),
            Entry::Some(t) => Ok(t),
        }
    }
}

impl<T: Clone> Extender<Option<T>> for OptionalEntry<T> {
    type Error = anyhow::Error;

    fn try_extend(self, t: &Option<T>) -> Result<Option<T>, Self::Error> {
        match self {
            OptionalEntry::Super => Ok(t.clone()),
            OptionalEntry::Some(t) => Ok(Some(t)),
            OptionalEntry::None => Ok(None),
        }
    }
}

#[derive(Clone, Debug)]
pub enum Entry<T> {
    Super,
    Some(T),
}

impl<T: FromStr> FromStr for Entry<T> {
    type Err = T::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        T::from_str(s).map(Self::Some)
    }
}

impl<T> TryFrom<String> for Entry<T>
where
    T: TryFrom<String>,
{
    type Error = <T as TryFrom<String>>::Error;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        let m: &str = &s.to_lowercase();
        match m {
            "super" => Ok(Self::Super),
            _ => T::try_from(s).map(Self::Some),
        }
    }
}

impl<T> Entry<T> {
    pub fn option(self) -> Option<T> {
        match self {
            Entry::Some(t) => Some(t),
            Entry::Super => None,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(transparent)]
pub struct OptionalExtensionEntry<T>(Option<Entry<T>>);

impl<T> From<Option<Entry<T>>> for OptionalExtensionEntry<T> {
    fn from(o: Option<Entry<T>>) -> Self {
        Self(o)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(from = "OptionalExtensionEntry<T>")]
#[serde(into = "OptionalExtensionEntry<T>")]
pub enum OptionalEntry<T: Clone> {
    Some(T),
    #[serde(skip_serializing)]
    None,
    Super,
}

impl<T: FromStr + Clone> FromStr for OptionalEntry<T> {
    type Err = T::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        T::from_str(s).map(Self::Some)
    }
}

impl<T: Clone> TryFrom<String> for OptionalEntry<T>
where
    T: TryFrom<String>,
{
    type Error = <T as TryFrom<String>>::Error;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        let m: &str = &s.to_lowercase();
        match m {
            "super" => Ok(Self::Super),
            "none" | "" => Ok(Self::None),
            _ => T::try_from(s).map(Self::Some),
        }
    }
}

impl<T: Clone> OptionalEntry<T> {
    pub fn unzip(self) -> Option<Option<T>> {
        match self {
            Self::Some(t) => Some(Some(t)),
            Self::None => Some(None),
            Self::Super => None,
        }
    }
    pub fn unwrap_or(self, t: Option<T>) -> Option<T> {
        self.unzip().unwrap_or(t)
    }
}

impl<T: Clone> From<OptionalExtensionEntry<T>> for OptionalEntry<T> {
    fn from(e: OptionalExtensionEntry<T>) -> Self {
        match e.0 {
            Some(Entry::Some(x)) => Self::Some(x),
            Some(Entry::Super) => Self::Super,
            None => Self::None,
        }
    }
}

impl<T: Clone> From<OptionalEntry<T>> for OptionalExtensionEntry<T> {
    fn from(e: OptionalEntry<T>) -> Self {
        match e {
            OptionalEntry::Some(x) => Self(Some(Entry::Some(x))),
            OptionalEntry::Super => Self(Some(Entry::Super)),
            OptionalEntry::None => Self(None),
        }
    }
}

impl<T> Serialize for Entry<T>
where
    T: Serialize,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Self::Super => serializer.serialize_str("super"),
            Self::Some(t) => t.serialize(serializer),
        }
    }
}

impl<'de, T: Deserialize<'de>> Deserialize<'de> for Entry<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct EntryVisitor<T>(PhantomData<T>);

        impl<'de, T> Visitor<'de> for EntryVisitor<T>
        where
            T: Deserialize<'de>,
        {
            type Value = Entry<T>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("Extension entry")
            }

            fn visit_str<E: de::Error>(self, s: &str) -> Result<Self::Value, E> {
                if s == "super" {
                    Ok(Entry::Super)
                } else {
                    Ok(Entry::Some(Deserialize::deserialize(
                        de::value::StrDeserializer::new(s),
                    )?))
                }
            }

            fn visit_map<A: de::MapAccess<'de>>(self, map: A) -> Result<Self::Value, A::Error> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::MapAccessDeserializer::new(map),
                )?))
            }

            fn visit_unit<E: de::Error>(self) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::UnitDeserializer::new(),
                )?))
            }
            fn visit_bool<E: de::Error>(self, b: bool) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::BoolDeserializer::new(b),
                )?))
            }
            fn visit_i64<E: de::Error>(self, b: i64) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::I64Deserializer::new(b),
                )?))
            }
            fn visit_i128<E: de::Error>(self, b: i128) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::I128Deserializer::new(b),
                )?))
            }
            fn visit_u64<E: de::Error>(self, b: u64) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::U64Deserializer::new(b),
                )?))
            }
            fn visit_u128<E: de::Error>(self, b: u128) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::U128Deserializer::new(b),
                )?))
            }
            fn visit_f64<E: de::Error>(self, b: f64) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::F64Deserializer::new(b),
                )?))
            }
            fn visit_bytes<E: de::Error>(self, b: &[u8]) -> Result<Self::Value, E> {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::BytesDeserializer::new(b),
                )?))
            }
            fn visit_newtype_struct<D>(self, d: D) -> Result<Self::Value, D::Error>
            where
                D: Deserializer<'de>,
            {
                Ok(Entry::Some(Deserialize::deserialize(d)?))
            }
            fn visit_seq<A>(self, a: A) -> Result<Self::Value, A::Error>
            where
                A: de::SeqAccess<'de>,
            {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::SeqAccessDeserializer::new(a),
                )?))
            }
            fn visit_enum<A>(self, a: A) -> Result<Self::Value, A::Error>
            where
                A: de::EnumAccess<'de>,
            {
                Ok(Entry::Some(Deserialize::deserialize(
                    de::value::EnumAccessDeserializer::new(a),
                )?))
            }
        }

        deserializer.deserialize_any(EntryVisitor(PhantomData))
    }
}
