use quote::quote;
use syn::parse_macro_input;
use syn::Data;
use syn::DeriveInput;
use syn::Field;
use syn::Fields;

#[proc_macro_derive(Diff)]
pub fn diff_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    match &input.data {
        Data::Struct(_) => derive_struct(&input),
        Data::Enum(_) => derive_enum(&input),
        _ => panic!("Only structs and enums are supported"),
    }
}

fn derive_struct(input: &DeriveInput) -> proc_macro::TokenStream {
    let name = &input.ident;
    let fields: Vec<_> = get_fields(&input.data)
        .into_iter()
        .map(|x| x.ident.as_ref().unwrap())
        .collect();
    let generics = &input.generics;
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let expanded = quote! {
        impl #impl_generics Diff for #name #ty_generics #where_clause {
            fn diff<D: Differ, W: std::fmt::Write>(
                &self,
                other: &Self,
                w: &mut W
            ) -> Result<(), std::fmt::Error> {
                if !self.has_diff(other) {
                    return w.write_str("unchanged");
                }
                #(
                    w.write_str("\n")?;
                    self.#fields.diff_named_indent::<D, W>(
                        &other.#fields,
                        w,
                        stringify!(#fields),
                        1
                    )?;
                )*
                Ok(())
            }
            fn diff_indent<D: Differ, W: std::fmt::Write>(
                &self,
                other: &Self,
                w: &mut W,
                indent: usize,
            ) -> Result<(), std::fmt::Error> {
                w.write_fmt(format_args!("{:width$}", " ", width = indent))?;
                #(
                    w.write_str("\n")?;
                    self.#fields.diff_named_indent::<D, W>(
                        &other.#fields,
                        w,
                        stringify!(#fields),
                        indent + 1
                    )?;
                )*
                Ok(())
            }
            fn diff_named_indent<D: Differ, W: std::fmt::Write>(
                &self,
                other: &Self,
                w: &mut W,
                name: &str,
                indent: usize,
            ) -> Result<(), std::fmt::Error> {
                if !self.has_diff(other) {
                    return w.write_fmt(format_args!("{0:width$}{1}: unchanged", " ", name, width = indent));
                }
                w.write_fmt(format_args!("{0:width$}{1}", " ", name, width = indent))?;
                #(
                    w.write_str("\n")?;
                    self.#fields.diff_named_indent::<D, W>(
                        &other.#fields,
                        w,
                        stringify!(#fields),
                        indent + 1
                    )?;
                )*
                Ok(())
            }
            fn entry(&self) -> String {
                let mut res = String::new();
                res.push_str(&stringify!(#name).to_lowercase());
                res.push_str(": ");
                #(
                    res.push_str("\n");
                    res.push_str(&self.#fields.entry());
                )*
                res
            }
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn derive_enum(input: &DeriveInput) -> proc_macro::TokenStream {
    let name = &input.ident;
    let generics = &input.generics;
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let expanded = quote! {
        impl #impl_generics Diff for #name #ty_generics #where_clause {
            fn diff<D: Differ, W: std::fmt::Write>(
                &self,
                other: &Self,
                w: &mut W
            ) -> Result<(), std::fmt::Error> {
                D::write_state(&(self, other).into(), w)?;
                Ok(())
            }
            fn diff_named_indent<D: Differ, W: std::fmt::Write>(
                &self,
                other: &Self,
                w: &mut W,
                name: &str,
                indent: usize,
            ) -> Result<(), std::fmt::Error> {
                w.write_fmt(format_args!("{:width$}", " ", width = indent))?;
                Self::diff_named::<D, W>(self, other, w, name)?;
                Ok(())
            }
            fn entry(&self) -> String {
                format!("{}::{:?}", stringify!(#name), self)
            }
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn get_fields(data: &Data) -> Vec<&Field> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => fields.named.iter().collect(),
            _ => panic!("Only named fields are supported"),
        },
        _ => panic!("Only structs are supported"),
    }
}
