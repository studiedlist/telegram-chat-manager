= Open source alternative to Telegram chat managers

:data-uri:
:reproducible:

:fn-duration: pass:c,q[footnote:duration[ Duration format is `X[y|w|d|h|m|s\]*`. Where ``X`` is any number followed by one of: ``y``ears, ``w``eeks, ``d``ays, ``h``ours, ``m``inutes, ``s``econds. This sequence can be repeated any number of times. Plain number represents seconds]]

== Features

* offline captcha
* join request handling
* autocommands
* ban, mute and kick by reply or user id
* delete messages by reply and count or by message id range
* user reports
* rich configuration
** tune and toggle features
** configure bot via private messages
** per-chat configuration

== Commands

=== Admin commands

Either reply to message or specify user id to trigger these commands

`/ban [id]`

`/mute <duration> [id]` {fn-duration}

`/kick [id]`

=== General commands


`/id` - reply to message to get sender id, or send it without reply to get your id. +
This command can be disabled, made available only to admins, or to all chat members

=== Captcha commands

`/regenerate` - use it if captcha is not readable. Maximum regeneration count is configurable

=== Reports

`/report [message_id]` - reply to message or specify message id to report message to administrators +
This command is available to all chat members

Reports are anonymous: only admins can see who reported whom

=== Raid mode

`/raid [true/false]` - turn on (true), off (false) or check current state of raid mode +
(without argument)

While raid mode is enabled, anyone who joins the chat assumed to be an attacker and will be +
instantly banned

Raid mode is turned off by default

State of raid mode is reset on bot start/restart

=== Private chat commands

`/start` - print start message

`/manage <id>` - select chat to manage

`/config` - show current chat config. +
After triggering this command, send config file in order to check it and review changes

`/save` - save previously reviewed version of config
