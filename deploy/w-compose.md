1. Build image: `nix build .#docker`
2. Load image in `docker/podman: docker load < result`
3. Check image id: `docker images -a | awk '/chat-manager/ {print $3}'`
4. Change IMAGE_ID in docker-compose.yml
5. Change VOLUME to dir with config (/cfg in this example)
6. Change BOT_TOKEN to real bot token
7. Run container: `docker-compose up`
