1. Build image: `nix build .#docker`
2. Load image in docker/podman: `docker load < result`
3. Check image id: `docker images -a | awk '/chat-manager/ {print $3}'`
4. Run container: `docker run -e "TELOXIDE_TOKEN=<INSERT TOKEN HERE>" -v /path/to/config.yaml:/data/config.yaml <IMAGE_ID_FROM_PREV_STEP>`
