use quote::quote;
use syn::parse_macro_input;
use syn::Attribute;
use syn::Data;
use syn::DataEnum;
use syn::DataStruct;
use syn::DeriveInput;
use syn::Expr;
use syn::Field;
use syn::Fields;
use syn::GenericArgument;
use syn::Lit;
use syn::Meta;
use syn::Path;
use syn::PathArguments;
use syn::Type;
use syn::Variant;

#[proc_macro_derive(SetField, attributes(set_field, skip_field))]
pub fn set_field_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    match &input.data {
        Data::Struct(s) => derive_set_field_struct(&input, s),
        Data::Enum(e) => derive_from_str_enum(&input, e),
        _ => panic!("Only structs are supported"),
    }
}

fn derive_set_field_struct(input: &DeriveInput, st: &DataStruct) -> proc_macro::TokenStream {
    let fields = match &st.fields {
        Fields::Named(f) => f
            .named
            .iter()
            .filter(|f| !f.attrs.iter().any(|a| a.path().is_ident("skip_field"))),
        _ => panic!("Only named fields are supported"),
    };
    let (supported, unsupported): (Vec<_>, Vec<_>) = fields.partition(|f| is_field_supported(f));
    let supported: Vec<_> = supported.into_iter().map(|f| &f.ident).collect();
    let (unsupported, unsupported_ty): (Vec<_>, Vec<_>) =
        unsupported.into_iter().map(|f| (&f.ident, &f.ty)).unzip();
    let name = &input.ident;
    let expanded = quote! {
        impl SetField for #name {
            fn set_field(&mut self, name: &str, value: String) -> Result<(), Error> {
                fn split(s: &str) -> Result<(&str, String), &str> {
                    if s.contains('.') {
                        let mut s = s.split(".");
                        let c: &str = s.next().unwrap();
                        let n: String = itertools::intersperse(s, ".").collect();
                        Ok((c, n))
                    } else {
                        Err(&s)
                    }
                }
                match split(name) {
                    #(
                        Ok((stringify!(#unsupported), next)) => self.#unsupported.set_field(&next, value),
                        Err(stringify!(#unsupported)) => Err(anyhow::anyhow!("Value not supported")),
                    )*
                    #(
                        Err(stringify!(#supported)) => {
                            self.#supported = value.parse()?;
                            Ok(())
                        }
                    )*
                    Err(v) | Ok((v, _)) => Err(anyhow::anyhow!("Unknown value: {}", v)),
                }
            }
            fn supported() -> Vec<String> {
                let mut res = vec![];
                #(
                    res.push(stringify!(#supported).to_string());
                )*
                #(
                    let mut fields = <#unsupported_ty>::supported()
                        .iter()
                        .map(|f| format!("{}.{}", stringify!(#unsupported), f))
                        .collect();
                    res.append(&mut fields);
                )*
                res
            }
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn derive_from_str_enum(input: &DeriveInput, st: &DataEnum) -> proc_macro::TokenStream {
    let name = &input.ident;
    let (supported, unsupported): (Vec<_>, Vec<_>) = st
        .variants
        .iter()
        .filter(|v| !v.attrs.iter().any(|a| a.path().is_ident("skip_field")))
        .partition(|v| is_variant_supported(v));

    let expanded = quote! {
        impl std::str::FromStr for #name {
            type Err = anyhow::Error;

            fn from_str(s: &str) -> Result<Self, Self::Err> {
                let s: &str = &s.to_lowercase();
                match s {
                    #(
                    s if s == stringify!(#supported).to_lowercase() => Ok(Self::#supported),
                    )*
                    #(
                    s if s == stringify!(#unsupported).to_lowercase() => Err(anyhow::anyhow!("Variant not supported")),
                    )*
                    _ => Err(anyhow::anyhow!("Unknown variant")),
                }
            }
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn is_variant_supported(v: &Variant) -> bool {
    match v.fields {
        Fields::Unit => true,
        _ => false,
    }
}

fn is_field_supported(f: &Field) -> bool {
    f.attrs.iter().any(|a| a.path().is_ident("set_field"))
}

#[proc_macro_derive(Description, attributes(ignore))]
pub fn description_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    match &input.data {
        Data::Struct(s) => derive_description_struct(&input, s),
        Data::Enum(e) => derive_description_enum(&input, e),
        _ => panic!("Only structs and enums are supported"),
    }
}

fn derive_description_struct(input: &DeriveInput, st: &DataStruct) -> proc_macro::TokenStream {
    let fields: Vec<_> = match &st.fields {
        Fields::Named(f) => f
            .named
            .iter()
            .filter(|f| !f.attrs.iter().any(|a| a.path().is_ident("ignore")))
            .collect(),
        _ => panic!("Only named fields are supported"),
    };
    let (commented, uncommented): (Vec<_>, Vec<_>) = fields
        .iter()
        .map(|f| (f, parse_doc_comment(&f.attrs)))
        .partition(|(_, c)| c.is_some());
    let (pattern, pattern_comment): (Vec<_>, Vec<_>) = commented
        .iter()
        .chain(uncommented.iter())
        .filter_map(|(f, c)| {
            if let Type::Path(path) = &f.ty {
                Some((f, &path.path, c))
            } else {
                None
            }
        })
        .filter(|(_, path, _)| is_pattern(path))
        .filter_map(|(f, _, c)| {
            Some((
                (&f.ty, f.ident.as_ref()?),
                c.as_ref().expect("Patterns must be commented"),
            ))
        })
        .unzip();
    let (pattern_ty, pattern): (Vec<_>, Vec<_>) = pattern.into_iter().unzip();
    let (commented, comment): (Vec<_>, Vec<_>) = commented
        .clone()
        .into_iter()
        .filter_map(|(f, c)| {
            if let Type::Path(path) = &f.ty {
                Some((f, &path.path, c))
            } else {
                None
            }
        })
        .filter(|(_, path, _)| !is_pattern(path))
        .map(|(f, _, c)| (f, c))
        .filter_map(|(f, c)| f.ident.as_ref().map(|i| ((&f.ty, i), c)))
        .unzip();
    let (ty, ident): (Vec<_>, Vec<_>) = commented.into_iter().unzip();
    let (uncommented_ident, uncommented_ty): (Vec<_>, Vec<_>) = uncommented
        .clone()
        .into_iter()
        .filter_map(|(f, c)| {
            if let Type::Path(path) = &f.ty {
                Some((f, &path.path, c))
            } else {
                None
            }
        })
        .filter(|(_, path, _)| !is_pattern(path))
        .map(|(f, _, c)| (f, c))
        .filter_map(|(f, _)| (f.ident.as_ref().map(|i| (i, &f.ty))))
        .unzip();
    let name = &input.ident;
    let expanded = quote! {
        fn description<D: Descriptor>() -> String {
            let mut res = String::new();
            #(
                res.push_str(&D::entry(
                    stringify!(#ident),
                    Some(stringify!(#ty)),
                    Some(#comment),
                    None
                ));
            )*
            #(
                let outer = <#pattern_ty>::outer_description::<D>();
                let d = D::entry(
                    stringify!(#pattern),
                    Some(stringify!(#pattern_ty)),
                    Some(#pattern_comment),
                    outer.as_deref()
                );
                res.push_str(&d);
            )*
            fn pad_str(s: &str, i: usize) -> String {
                s.replace("\n", &format!("\n{0:>width$}", " ", width = i))
            }
            #(
                let mut description = <#uncommented_ty>::description::<D>();
                let d;
                while description.starts_with('\n') {
                    description = description.replacen('\n', " ", 1);
                }
                if description.contains('\n') {
                    description = pad_str(&description, 2);
                }
                d = D::map(
                    stringify!(#uncommented_ident),
                    stringify!(#uncommented_ty),
                    &description,
                    <#uncommented_ty>::outer_description::<D>().as_deref(),
                );
                res.push_str(&d);
            )*
            res.replace("\n\n", "\n").replace("\n", "  \n")
        }
    };
    let outer = parse_doc_comment(&input.attrs).map(|comment| {
        quote! {
           fn outer_description<D: Descriptor>() -> Option<String> {
                Some(stringify!(#comment).to_string())
           }
        }
    });
    let expanded = quote! {
        impl Description for #name {
            #expanded
            #outer
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn derive_description_enum(input: &DeriveInput, en: &DataEnum) -> proc_macro::TokenStream {
    let fields: Vec<_> = en
        .variants
        .iter()
        .filter(|f| !f.attrs.iter().any(|a| a.path().is_ident("ignore")))
        .collect();
    let (commented, uncommented): (Vec<_>, Vec<_>) = fields
        .iter()
        .map(|f| (f, parse_doc_comment(&f.attrs)))
        .partition(|(_, c)| c.is_some());
    let (ident, comment): (Vec<_>, Vec<String>) = commented
        .into_iter()
        .filter_map(|(f, c)| Some((&f.ident, c?)))
        .unzip();
    let uncommented: Vec<_> = uncommented.into_iter().map(|(f, _)| &f.ident).collect();
    let name = &input.ident;
    let expanded = quote! {
        fn description<D: Descriptor>() -> String {
            let mut variants = vec![];
            #(
                variants.push((stringify!(#ident), Some(#comment)));
            )*
            #(
                variants.push((stringify!(#uncommented), None));
            )*
            D::enum_variants(variants)
        }
    };
    let outer = parse_doc_comment(&input.attrs).map(|comment| {
        quote! {
           fn outer_description<D: Descriptor>() -> Option<String> {
                Some(stringify!(#comment).to_string())
           }
        }
    });
    let expanded = quote! {
        impl Description for #name {
            #expanded
            #outer
        }
    };
    proc_macro::TokenStream::from(expanded)
}

fn parse_doc_comment(attrs: &[Attribute]) -> Option<String> {
    let attr = attrs.iter().find(|a| a.path().is_ident("doc"))?;
    let meta = match &attr.meta {
        Meta::NameValue(meta) => meta,
        _ => return None,
    };
    let lit = match &meta.value {
        Expr::Lit(lit) => lit,
        _ => return None,
    };
    match &lit.lit {
        Lit::Str(s) => Some(s.value().trim().to_string()),
        _ => None,
    }
}

fn is_pattern(path: &Path) -> bool {
    path.segments.iter().any(|s| {
        s.ident == "Pattern"
            || match &s.arguments {
                PathArguments::AngleBracketed(args) => args.args.iter().any(|arg| {
                    matches!(
                        arg,
                        GenericArgument::Type(Type::Path(path)) if is_pattern(&path.path)
                    )
                }),
                _ => false,
            }
    })
}
