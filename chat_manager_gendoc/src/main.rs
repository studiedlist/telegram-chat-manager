use chat_manager::config::description::Description;
use chat_manager::config::description::GendocDescriptor;
use chat_manager::config::Config;
use clap::Parser;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use chat_manager::CRATE_VERSION as CHAT_MANAGER_VERSION;

#[derive(Parser, Debug)]
pub struct Arguments {
    #[arg(short, long)]
    output: Option<PathBuf>,
    #[arg(short = 'w', long)]
    overwrite: bool,
}
fn pad_str(s: &str, i: usize) -> String {
    s.replace('\n', &format!("\n{0:>width$}", " ", width = i))
}

pub fn main() -> Result<(), anyhow::Error> {
    let args = Arguments::parse();
    let meta = format!("<doc><version>{CHAT_MANAGER_VERSION}</version></doc>");
    let res = format!(
        "{meta}
        <include ../doc/meta.html>
        <include ../doc/welcome.md>
        <include ../doc/configuration_overview.md>
        <include ../doc/common_types.md>
        <h1>Configuration reference</h1>
        <include ../doc/configuration.md>
        {}
        <include ../doc/features.md>",
        pad_str(&Config::description::<GendocDescriptor>(), 1)
    );
    if let Some(path) = &args.output {
        let mut f = match File::open(path) {
            Ok(_) if !args.overwrite => return Err(anyhow::anyhow!("File already exists")),
            _ => File::create(path)?,
        };
        f.write_all(res.as_bytes())?;
    } else {
        println!("{res}");
    }
    Ok(())
}
