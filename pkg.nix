{ lib
, rustPlatform
, pkg-config
, openssl
}:

rustPlatform.buildRustPackage rec {
  pname = "telegram-chat-manager";
  version = "0.0.1";

  src = ./.;

  cargoLock = {
    lockFile = ./Cargo.lock;
    allowBuiltinFetchGit = true;
  };

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ openssl ];

  outputs = [ "out" "dev" ];

  meta = with lib; {
    description = "";
    homepage = "https://gitlab.com/studiedlist/telegram-chat-manager";
    changelog = "";
    # license = with licenses; [  ];
    maintainers = with maintainers; [ _3JlOy-PYCCKUi ];
  };
}
