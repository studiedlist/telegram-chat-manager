pub mod chat;
mod dto;
pub mod extension;
pub mod set_field;
mod yaml;

pub use yaml::parse_config;

#[cfg(feature = "description")]
pub mod description;
#[cfg(feature = "description")]
use chat_manager_derive::Description;
#[cfg(feature = "description")]
use description::Description;
#[cfg(feature = "description")]
use description::Descriptor;

use captcha::filters::Cow;
use captcha::filters::Dots;
use captcha::filters::Grid;
use captcha::filters::Noise;
use captcha::filters::Wave;
use captcha::Captcha;
use chrono::Duration;
use config_diff::macros::Diff;
use config_diff::Diff;
use config_diff::Differ;
use config_pattern::Arguments;
use config_pattern::Pattern;
use derive_more::Deref;
use derive_more::Display;
use derive_more::From;
use extension::ConfigExtension;
use once_cell::sync::Lazy;
use teloxide::types::Administrator;
use teloxide::types::ChatId;
use teloxide::types::ChatMember;
use teloxide::types::ChatMemberKind;
use teloxide::types::Me;
use teloxide::types::Message;
use teloxide::types::Owner;
use teloxide_extras::extractors::extract_chat;

pub const DEFAULT_RETRY_LIMIT: usize = 3;
pub static DEFAULT_TIME_LIMIT: Lazy<Duration> = Lazy::new(|| Duration::seconds(30));
pub const DEFAULT_REGENERATE_LIMIT: usize = 3;
pub static DEFAULT_TIMEOUT: Lazy<Duration> = Lazy::new(|| Duration::minutes(10));
pub static DEFAULT_POLL_TIMEOUT: Lazy<Duration> = Lazy::new(|| Duration::minutes(2));
pub const DEFAULT_REPORT_NOTIFY: &str =
    "\\#Report `{}` was sent, Administrators and Moderators, see report in Recent actions";
pub static DEFAULT_REPORT_ACTIONS: Lazy<Vec<ReportAction>> = Lazy::new(|| {
    vec![
        ReportAction::Ban,
        ReportAction::Mute {
            time: Duration::minutes(30),
        },
        ReportAction::Delete,
    ]
});

#[derive(Clone, Debug, Deref)]
pub struct Regex(regex::Regex);

impl Regex {
    pub fn new(re: &str) -> Result<Self, regex::Error> {
        regex::Regex::new(re).map(Self)
    }
}

impl PartialEq for Regex {
    fn eq(&self, other: &Self) -> bool {
        self.as_str() == other.as_str()
    }
}

pub mod arguments {
    use crate::format_user;
    use config_diff::macros::Diff;
    use config_diff::Diff;
    use config_diff::Differ;
    use config_pattern::macros::Args;
    use config_pattern::Argument;
    use derive_more::From;
    use derive_new::new;
    use teloxide::types::User;
    use uuid::Uuid;

    #[derive(Diff, Clone, Args, Debug, new, PartialEq, Eq)]
    pub struct Username {
        username: String,
    }

    impl From<&User> for Username {
        fn from(u: &User) -> Self {
            let username = format_user(u);
            Self { username }
        }
    }

    impl From<&str> for Username {
        fn from(s: &str) -> Self {
            let username = s.to_string();
            Self { username }
        }
    }

    #[derive(Diff, Clone, Args, Debug, new, PartialEq, Eq)]
    pub struct Reason {
        reason: String,
    }

    impl From<&str> for Reason {
        fn from(s: &str) -> Self {
            let reason = s.to_string();
            Self { reason }
        }
    }

    impl From<String> for Reason {
        fn from(reason: String) -> Self {
            Self { reason }
        }
    }

    #[derive(Diff, Clone, Args, Debug, PartialEq, Eq)]
    pub struct RetryMessage {
        current: String,
        max: String,
    }

    impl RetryMessage {
        pub fn new(current: usize, max: usize) -> Self {
            Self {
                current: current.to_string(),
                max: max.to_string(),
            }
        }
    }

    #[derive(Diff, Clone, Args, Debug, PartialEq, Eq)]
    pub struct RegenerateMessage {
        username: String,
        current: String,
        max: String,
    }

    impl RegenerateMessage {
        pub fn new(current: usize, max: usize, username: String) -> Self {
            Self {
                current: current.to_string(),
                max: max.to_string(),
                username,
            }
        }
    }

    #[derive(Diff, Clone, Args, Debug, PartialEq, Eq)]
    pub struct ActionOnUser {
        username: String,
        action: String,
    }

    impl ActionOnUser {
        pub fn new(user: &User, action: String) -> Self {
            Self {
                username: format_user(user),
                action,
            }
        }
    }

    #[derive(Diff, Clone, Args, Debug, PartialEq, Eq, From)]
    pub struct Id {
        id: String,
    }

    impl From<Uuid> for Id {
        fn from(id: Uuid) -> Self {
            let id = id.to_string();
            Self { id }
        }
    }
}

use arguments::*;

/// Bot configuration
#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct Config {
    /// Displayed when user enters chat
    pub greeting: Option<Pattern<Username>>,
    /// Displayed when user leaves chat
    pub goodbye: Option<Pattern<Username>>,
    pub timeout: TimeoutConfig,
    pub admin: AdminConfig,
    pub rules: Option<RulesConfig>,
    pub captcha: Option<CaptchaConfig>,
    pub command: CommandConfig,
    pub report: Option<ReportConfig>,
    pub join_request: Option<JoinRequestConfig>,
    /// Autocommand list
    pub autocommands: Option<Vec<AutoCommand>>,
    pub antispam: Option<AntispamConfig>,
}

impl Config {
    pub fn status(&self) -> String {
        fn push_status(text: &mut String, name: &str, val: &str) {
            text.push_str(name);
            text.push_str(": ");
            text.push_str(val);
            text.push('\n');
        }
        fn push_status_enum<V>(text: &mut String, name: &str, val: &Option<V>) {
            match val {
                Some(_) => push_status(text, name, "enabled"),
                None => push_status(text, name, "disabled"),
            }
        }
        fn push_status_pattern<A: Arguments>(
            text: &mut String,
            name: &str,
            val: &Option<Pattern<A>>,
        ) {
            match val {
                Some(p) => push_status(text, name, &format!("\"{}\"", p.text())),
                None => push_status(text, name, "disabled"),
            }
        }
        let mut res = String::new();
        push_status_enum(&mut res, "Captcha", &self.captcha);

        res.push_str("General commands:");
        res.push_str("\n  ");
        let status = match self.command.id {
            Some(AccessLevel::Admin(_)) => "admins only",
            Some(AccessLevel::User(_)) => "all users",
            None => "disabled",
        };
        push_status(&mut res, "id", status);

        push_status_pattern(&mut res, "Greeting", &self.greeting);
        push_status_pattern(&mut res, "Goodbye", &self.goodbye);

        res.push_str("Timeout:");
        res.push_str("\n  default_timeout: ");
        res.push_str(&self.timeout.default_timeout.to_string());

        res
    }
    pub fn join_requests_enabled(self: Arc<Self>) -> Option<Arc<JoinRequestConfig>> {
        self.join_request.clone().map(Arc::new)
    }
    pub fn antispam_enabled(self: Arc<Self>) -> Option<Arc<AntispamConfig>> {
        self.antispam.clone().map(Arc::new)
    }
    pub fn captcha_enabled(self: Arc<Self>) -> Option<Arc<CaptchaConfig>> {
        self.captcha.clone().map(Arc::new)
    }
    pub fn reports_enabled(self: Arc<Self>) -> Option<Arc<ReportConfig>> {
        self.report.clone().map(Arc::new)
    }
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct TimeoutConfig {
    /// Default timeout for incoming updates
    pub default_timeout: Duration,
}

impl Default for TimeoutConfig {
    fn default() -> Self {
        Self {
            default_timeout: *Lazy::force(&DEFAULT_TIMEOUT),
        }
    }
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct CaptchaConfig {
    /// Timeout for captcha related updates
    pub captcha_timeout: Option<Duration>,
    /// Caption for captcha image
    pub captcha_caption: Pattern<Username>,
    /// Message displayed when user fails to solve captcha
    pub failure_message: Option<Pattern<Username>>,
    /// Message displayed when user solve captcha
    pub success_message: Option<Pattern<Username>>,
    /// Message displayed when user fails to solve captcha
    pub retry_message: Pattern<RetryMessage>,
    /// Message displayed when captcha is regenerated
    pub regenerate_message: Pattern<RegenerateMessage>,
    /// Message sent after regenerate limit reached
    pub regenerate_limit_reached: String,
    /// Maximum number of times user can retry solving captcha
    pub retry_limit: usize,
    /// User will be banned if he fails to solve captcha in given time limit
    pub time_limit: Duration,
    /// Maximum number of times user can regenerate captcha
    pub regenerate_limit: usize,
    /// Time for which user will be banned if he fails to solve captcha
    pub ban_time: Option<Duration>,
    pub gen_options: GenOptions,
}

impl Default for CaptchaConfig {
    #[allow(clippy::unwrap_used)]
    fn default() -> Self {
        Self {
            captcha_timeout: None,
            captcha_caption: "Hello {}, please confirm that you are human".try_into().unwrap(),
            failure_message: None,
            success_message: None,
            retry_message: "Try again. Retry {current} of {max}".try_into().unwrap(),
            regenerate_message: "{username}, please confirm that you are human\nCaptcha regenerated {current} of {} times".try_into().unwrap(),
            regenerate_limit_reached: "You cannot regenerate captcha anymore".to_string(),
            retry_limit: 5,
            time_limit: Duration::seconds(60),
            regenerate_limit: 3,
            ban_time: None,
            gen_options: GenOptions::default(),
        }
    }
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct GenOptions {
    /// Number of generated chars
    pub chars: u32,
    /// Size of captcha image
    pub view: Option<(u32, u32)>,
    /// Filters for captcha
    pub filters: Vec<Filter>,
}

#[derive(Diff, Clone, Debug, From, PartialEq)]
pub enum Filter {
    Cow(Cow),
    Dots(Dots),
    Grid(Grid),
    Noise(Noise),
    Wave(Wave),
}

impl Filter {
    pub fn apply<'a>(&self, c: &'a mut Captcha) -> &'a mut Captcha {
        match self {
            Self::Cow(cow) => c.apply_filter(cow.clone()),
            Self::Dots(dots) => c.apply_filter(dots.clone()),
            Self::Grid(grid) => c.apply_filter(grid.clone()),
            Self::Noise(noise) => c.apply_filter(noise.clone()),
            Self::Wave(wave) => c.apply_filter(wave.clone()),
        }
    }
}

impl Default for GenOptions {
    fn default() -> Self {
        Self {
            chars: 5,
            view: Some((220, 120)),
            filters: vec![Filter::Noise(Noise::new(0.6))],
        }
    }
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Default, Debug, Clone, PartialEq, Eq)]
pub struct AdminConfig {
    /// Message sent after banning user
    pub banned: Option<Pattern<Username>>,
    /// Message sent after kicking user
    pub kicked: Option<Pattern<Username>>,
    /// Message sent after muting user
    pub muted: Option<Pattern<Username>>,
    /// Reason formatting
    pub reason: Option<Pattern<Reason>>,
    pub kick_behavior: KickBehavior,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Display, Diff, Debug, Default, Clone, PartialEq, Eq)]
pub enum KickBehavior {
    /// Use Telegram kick method (behaves like ban)
    #[default]
    Telegram,
    /// Use Telegram unban method (behaves like you would expect kick to)
    KickAndUnban,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Display, Diff, Clone, Debug, PartialEq, Eq)]
pub enum AccessLevel {
    /// Allowed only to admins
    Admin(AdminAccess),
    /// Allowed to users
    User(UserAccess),
}

impl Default for AccessLevel {
    fn default() -> Self {
        Self::Admin(AdminAccess::Admin)
    }
}

impl AccessLevel {
    pub fn has_access(&self, other: &AccessLevel) -> bool {
        match (self, other) {
            (Self::Admin(a), Self::Admin(b)) => a.has_access(b),
            (Self::Admin(_), Self::User(_)) => true,
            (Self::User(a), Self::User(b)) => a.has_access(b),
            (Self::User(_), Self::Admin(_)) => false,
        }
    }
    pub fn has_permissions(&self, permissions: Vec<AdminPermission>) -> bool {
        let other = match permissions.len() {
            0 => AccessLevel::Admin(AdminAccess::Admin),
            _ => AccessLevel::Admin(AdminAccess::Permissions(AdminPermissions(permissions))),
        };
        self.has_access(&other)
    }
    pub fn has_permission(&self, perm: AdminPermission) -> bool {
        let other = AccessLevel::Admin(AdminAccess::Permissions(AdminPermissions(vec![perm])));
        self.has_access(&other)
    }
}

impl From<ChatMember> for AccessLevel {
    fn from(m: ChatMember) -> Self {
        match m.kind {
            ChatMemberKind::Owner(o) => Self::Admin(AdminAccess::from(o)),
            ChatMemberKind::Administrator(a) => Self::Admin(AdminAccess::from(a)),
            ChatMemberKind::Member
            | ChatMemberKind::Restricted(_)
            | ChatMemberKind::Left
            | ChatMemberKind::Banned(_) => Self::User(UserAccess::User),
        }
    }
}

#[derive(Display, Diff, Clone, Debug, PartialEq, Eq)]
pub enum UserAccess {
    User,
}

impl UserAccess {
    pub fn has_access(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::User, Self::User) => true,
        }
    }
}

#[derive(Display, Diff, Clone, Debug, PartialEq, Eq)]
pub enum AdminAccess {
    Permissions(AdminPermissions),
    Owner,
    Admin,
}

impl AdminAccess {
    pub fn has_access(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Permissions(a), Self::Permissions(b)) => b.0.iter().all(|b| a.0.contains(b)),
            (Self::Permissions(_), Self::Admin) => true,
            (Self::Admin, Self::Admin) => true,
            (Self::Admin, Self::Permissions(_)) => false,
            (Self::Owner, _) => true,
            (_, Self::Owner) => false,
        }
    }
}

impl From<Owner> for AdminAccess {
    fn from(_: Owner) -> AdminAccess {
        Self::Owner
    }
}

impl From<Administrator> for AdminAccess {
    fn from(a: Administrator) -> Self {
        let mut permissions = vec![];
        if a.can_restrict_members {
            permissions.push(AdminPermission::CanRestrictMembers);
        }
        if a.can_change_info {
            permissions.push(AdminPermission::CanChangeInfo);
        }
        if a.can_manage_chat {
            permissions.push(AdminPermission::CanManageChat);
        }
        if a.can_delete_messages {
            permissions.push(AdminPermission::CanDeleteMessages);
        }
        if a.can_manage_video_chats {
            permissions.push(AdminPermission::CanManageVideoChats);
        }
        if a.can_pin_messages {
            permissions.push(AdminPermission::CanPinMessages);
        }
        if a.can_manage_topics {
            permissions.push(AdminPermission::CanManageTopics);
        }
        if a.can_promote_members {
            permissions.push(AdminPermission::CanPromoteMembers);
        }
        match permissions.len() {
            0 => Self::Admin,
            _ => Self::Permissions(AdminPermissions(permissions)),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct AdminPermissions(Vec<AdminPermission>);

impl std::fmt::Display for AdminPermissions {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let text: String =
            itertools::intersperse(self.0.iter().map(ToString::to_string), ", ".to_string())
                .collect();
        formatter.write_fmt(format_args!("[ {text} ]"))
    }
}

#[derive(Display, Diff, Clone, Debug, PartialEq, Eq)]
pub enum AdminPermission {
    CanRestrictMembers,
    CanChangeInfo,
    CanManageChat,
    CanDeleteMessages,
    CanManageVideoChats,
    CanPinMessages,
    CanManageTopics,
    CanPromoteMembers,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, Default, PartialEq, Eq)]
pub struct CommandConfig {
    /// Shows id of target user
    pub id: Option<AccessLevel>,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq, Eq)]
pub struct ReportConfig {
    /// Message sent when report is created
    pub report_notify: Pattern<Id>,
    /// Actions available at report notification
    pub actions: Vec<ReportAction>,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq, Eq)]
pub enum ReportAction {
    Ban,
    Mute { time: Duration },
    Delete,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct JoinRequestConfig {
    pub method: ApprovalMethod,
}

/// Method used to accept or deny join requests
#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub enum ApprovalMethod {
    /// Make user solve captcha
    Captcha(Option<CaptchaConfig>),
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct AutoCommand {
    /// Actions performed when command is triggered
    pub actions: Vec<Action>,
    /// Trigger for command
    pub trigger: Trigger,
}

impl AutoCommand {
    pub fn generate_rules_entry(&self) -> String {
        format!(
            "{}: {}",
            self.trigger,
            itertools::intersperse(
                self.actions.iter().map(ToString::to_string),
                " ".to_string()
            )
            .collect::<String>()
        )
    }
}

pub struct RulesFormatting {
    pub action: Action,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub enum Action {
    Ban {
        reason: Option<String>,
    },
    Answer {
        text: Pattern<Username>,
    },
    Mute {
        time: Duration,
        reason: Option<String>,
    },
    DeleteMessage {
        reason: Option<String>,
    },
    Kick {
        reason: Option<String>,
    },
}

impl std::fmt::Display for Action {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Ban { .. } => fmt.write_str("ban"),
            Self::Answer { .. } => fmt.write_str("verbal warning"),
            Self::Mute { .. } => fmt.write_str("mute"),
            Self::DeleteMessage { .. } => fmt.write_str("message deletion"),
            Self::Kick { .. } => fmt.write_str("kick"),
        }
    }
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub enum Trigger {
    Regex(Regex),
    Audio,
    Document,
    Animation,
    Game,
    Sticker,
    Video,
    Voice,
    VideoNote,
    Contact,
    Location,
    Venue,
    Poll,
}

impl Trigger {
    pub fn matches(&self, msg: &Message) -> bool {
        match self {
            Self::Regex(re) => msg
                .text()
                .or(msg.caption())
                .is_some_and(|text| re.is_match(text)),
            Self::Audio => msg.audio().is_some(),
            Self::Document => msg.document().is_some(),
            Self::Animation => msg.animation().is_some(),
            Self::Game => msg.game().is_some(),
            Self::Sticker => msg.sticker().is_some(),
            Self::Video => msg.video().is_some(),
            Self::Voice => msg.voice().is_some(),
            Self::VideoNote => msg.video_note().is_some(),
            Self::Contact => msg.contact().is_some(),
            Self::Location => msg.location().is_some(),
            Self::Venue => msg.venue().is_some(),
            Self::Poll => msg.poll().is_some(),
        }
    }
}

impl std::fmt::Display for Trigger {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Regex(regex) => fmt.write_fmt(format_args!("\"{}\"", regex.as_str())),
            Self::Audio => fmt.write_str("audio message"),
            Self::Document => fmt.write_str("document"),
            Self::Animation => fmt.write_str("animation"),
            Self::Game => fmt.write_str("game"),
            Self::Sticker => fmt.write_str("sticker"),
            Self::Video => fmt.write_str("video"),
            Self::Voice => fmt.write_str("voice"),
            Self::VideoNote => fmt.write_str("video note"),
            Self::Contact => fmt.write_str("contact"),
            Self::Location => fmt.write_str("location"),
            Self::Venue => fmt.write_str("venue"),
            Self::Poll => fmt.write_str("poll"),
        }
    }
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct AntispamConfig {
    /// Allowed number of messages per second
    pub limit: u8,
    /// Actions performed when limit is exceeded
    pub actions: Vec<Action>,
}

#[cfg_attr(feature = "description", derive(Description))]
#[derive(Diff, Clone, Debug, PartialEq)]
pub struct RulesConfig {
    /// Rules of chat
    pub rules: Option<String>,
    /// Enable to add generated section to rules based on autocommands of your chat
    pub generate_from_autocommands: bool,
}

use chat::ChatConfigRepository;
use config_extension::Extender;
use std::path::PathBuf;
use std::sync::Arc;
use teloxide::types::Update;

static CONFIG: Lazy<Config> = Lazy::new(|| {
    // No error handling needed since panic at startup is acceptable
    #[allow(clippy::unwrap_used)]
    parse_config(&PathBuf::from("config.yaml"))
        .map_err(|err| {
            err.location()
                .map(|location| {
                    anyhow::anyhow!(
                        "Invalid config\nAt line {}, column {}\nCaused by:\n{err}",
                        location.line(),
                        location.column()
                    )
                })
                .unwrap_or_else(|| anyhow::anyhow!("Invalid config\nCaused by:\n{err}"))
        })
        .unwrap()
});

pub fn merge_config_from_upd(upd: Update, repo: Arc<dyn ChatConfigRepository>) -> Arc<Config> {
    let base = Lazy::force(&CONFIG);
    let id = match extract_chat(&upd).map(|c| c.id) {
        Some(id) => id,
        None => return Arc::new(base.clone()),
    };
    merge_config_from_chat_id(id, repo)
}

pub fn merge_config_from_chat_id(
    chat_id: ChatId,
    repo: Arc<dyn ChatConfigRepository>,
) -> Arc<Config> {
    let base = Lazy::force(&CONFIG);
    let extension = match repo.get_one(&chat_id) {
        Ok(Some(e)) => e,
        Ok(None) => return Arc::new(base.clone()),
        Err(err) => {
            log::error!("Cannot get config extension. Err is:\n{err}");
            return Arc::new(base.clone());
        }
    };
    merge_config(extension.config, base)
}

fn merge_config(ex: ConfigExtension, base: &Config) -> Arc<Config> {
    match ex.try_extend(base) {
        Ok(c) => Arc::new(c),
        Err(err) => {
            log::error!("Cannot extend config. Err is:\n{err}");
            Arc::new(base.clone())
        }
    }
}

pub fn base_config() -> Arc<Config> {
    Arc::new(Lazy::force(&CONFIG).clone())
}

pub fn init_config() {
    Lazy::force(&CONFIG);
}

use teloxide::utils::command::ParseError;

pub enum CommandError {
    BotCannotBeTarget,
    ParseError(ParseError),
}

impl From<ParseError> for CommandError {
    fn from(e: ParseError) -> Self {
        Self::ParseError(e)
    }
}

pub trait OptionalCommands: Sized {
    fn parse(
        s: &str,
        me: &Me,
        msg: &Message,
        config: Arc<Config>,
    ) -> Result<Option<Self>, CommandError>;
}
