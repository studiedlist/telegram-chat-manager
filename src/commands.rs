#[cfg(test)]
mod tests;

use crate::config::CommandError;
use crate::config::Config;
use crate::config::OptionalCommands;
use std::sync::Arc;
use teloxide::types::BotCommand;
use teloxide::types::ChatId;
use teloxide::types::Me;
use teloxide::types::Message;
use teloxide::utils::command::BotCommands;
use teloxide::utils::command::CommandDescription;
use teloxide::utils::command::CommandDescriptions;
use teloxide::utils::command::ParseError;

#[derive(BotCommands, Clone, Debug)]
#[command(rename_rule = "snake_case")]
pub enum GeneralCommand {
    Id,
    Rules,
}

impl OptionalCommands for GeneralCommand {
    fn parse(
        s: &str,
        me: &Me,
        _msg: &Message,
        config: Arc<Config>,
    ) -> Result<Option<Self>, CommandError> {
        if config.command.id.is_none() {
            return Ok(None);
        }
        let bot_username = me.username.clone().unwrap_or("".to_string());
        Ok(<Self as BotCommands>::parse(s, &bot_username).map(Some)?)
    }
}

impl GeneralCommand {
    pub fn is_id(self) -> bool {
        matches!(self, Self::Id)
    }
    pub fn is_rules(self) -> bool {
        matches!(self, Self::Rules)
    }
}

#[derive(Clone, Debug)]
pub enum PrivateChatCommand {
    Start,
    StartChat(ChatId),
    Manage(ChatId),
    Config,
    Save,
    Status,
    SetProp(String, String),
}

impl BotCommands for PrivateChatCommand {
    fn parse(s: &str, _bot_username: &str) -> Result<Self, ParseError> {
        let parse_id = |x: Option<&str>| {
            x.map(|x| {
                x.parse::<i64>()
                    .map(ChatId)
                    .map_err(Box::new)
                    .map_err(|err| ParseError::IncorrectFormat(err))
            })
            .transpose()
        };
        let mut split = s.split(' ');
        match split.next() {
            Some("/start") => match parse_id(split.next())? {
                None => Ok(Self::Start),
                Some(x) => Ok(Self::StartChat(x)),
            },
            Some("/config") => Ok(Self::Config),
            Some("/save") => Ok(Self::Save),
            Some("/status") => Ok(Self::Status),
            Some("/manage") => {
                let n = split.next();
                match parse_id(n)? {
                    Some(id) => Ok(Self::Manage(id)),
                    None => Err(ParseError::TooFewArguments {
                        expected: 1,
                        found: 0,
                        message: "You must specify chat id".to_string(),
                    }),
                }
            }
            Some("/set_prop") => {
                let args = match split.next() {
                    Some(args) => split
                        .next()
                        .map(|x| format!("{args}{x}"))
                        .unwrap_or(args.to_string()),
                    None => {
                        return Err(ParseError::TooFewArguments {
                            expected: 2,
                            found: 0,
                            message: "You must specify name and value".to_string(),
                        })
                    }
                };
                if !args.contains(':') {
                    return Err(ParseError::IncorrectFormat(
                        anyhow::anyhow!("Arguments must be in form `name: value`").into(),
                    ));
                }
                let mut split = args.split(':');
                let (name, value) = (split.next(), split.next());
                match name.zip(value) {
                    Some((name, value)) => Ok(Self::SetProp(name.to_string(), value.to_string())),
                    None => Err(ParseError::Custom(
                        anyhow::anyhow!("Bug: cannot parse set_props args").into(),
                    )),
                }
            }
            Some(cmd) => Err(ParseError::UnknownCommand(cmd.to_string())),
            None => Err(ParseError::UnknownCommand("".to_string())),
        }
    }

    fn descriptions() -> CommandDescriptions<'static> {
        CommandDescriptions::new(&[
            CommandDescription {
                prefix: "/",
                command: "start",
                description: "Show start message",
            },
            CommandDescription {
                prefix: "/",
                command: "manage",
                description: "Manage selected chat",
            },
            CommandDescription {
                prefix: "/",
                command: "config",
                description: "Show current chat config",
            },
            CommandDescription {
                prefix: "/",
                command: "save",
                description: "Save updated version of chat config",
            },
            CommandDescription {
                prefix: "/",
                command: "status",
                description: "Show current chat feature status",
            },
            CommandDescription {
                prefix: "/",
                command: "set_prop",
                description: "Set config property",
            },
        ])
    }

    fn bot_commands() -> Vec<BotCommand> {
        vec![
            BotCommand {
                command: "start".to_string(),
                description: "Show start message".to_string(),
            },
            BotCommand {
                command: "manage".to_string(),
                description: "Manage selected chat".to_string(),
            },
            BotCommand {
                command: "config".to_string(),
                description: "Show current chat config".to_string(),
            },
            BotCommand {
                command: "save".to_string(),
                description: "Save updated version of chat config".to_string(),
            },
            BotCommand {
                command: "status".to_string(),
                description: "Show current chat feature status".to_string(),
            },
            BotCommand {
                command: "set_prop".to_string(),
                description: "Set config property".to_string(),
            },
        ]
    }
}

impl PrivateChatCommand {
    pub fn is_start(self) -> bool {
        matches!(self, Self::Start)
    }
    pub fn is_start_chat(self) -> Option<ChatId> {
        match self {
            Self::StartChat(id) => Some(id),
            _ => None,
        }
    }
    pub fn is_manage(self) -> bool {
        matches!(self, Self::Manage(_))
    }
    pub fn is_config(self) -> bool {
        matches!(self, Self::Config)
    }
    pub fn is_save(self) -> bool {
        matches!(self, Self::Save)
    }
    pub fn is_status(self) -> bool {
        matches!(self, Self::Status)
    }
    pub fn is_set_prop(self) -> bool {
        matches!(self, Self::SetProp(..))
    }
}
