use super::UserCaptcha;
use anyhow::Error;
use teloxide::types::MessageId;
use typesafe_repository::async_ops::*;
use typesafe_repository::prelude::*;

pub trait CaptchaRepository:
    Repository<UserCaptcha, Error = Error>
    + Add<UserCaptcha>
    + Get<UserCaptcha>
    + GetBy<UserCaptcha, MessageId>
    + Update<UserCaptcha>
    + Remove<UserCaptcha>
    + Send
    + Sync
{
}

impl<T> CaptchaRepository for T where
    T: Repository<UserCaptcha, Error = Error>
        + Add<UserCaptcha>
        + Get<UserCaptcha>
        + GetBy<UserCaptcha, MessageId>
        + Update<UserCaptcha>
        + Remove<UserCaptcha>
        + Send
        + Sync
{
}
