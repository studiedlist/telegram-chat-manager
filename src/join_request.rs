use crate::captcha::repository::CaptchaRepository;
use crate::captcha::send_captcha;
use crate::captcha::CaptchaEvent;
use crate::config::ApprovalMethod;
use crate::config::CaptchaConfig;
use crate::config::Config;
use crate::config::JoinRequestConfig;
use crate::ok_or_tx;
use crate::EventRx;
use crate::EventTx;
use anyhow::anyhow;
use anyhow::Context;
use anyhow::Error;
use std::sync::Arc;
use teloxide::prelude::*;
use teloxide::types::ChatInviteLink;
use teloxide::types::ChatJoinRequest;
use teloxide::types::User;
use tokio::sync::mpsc;
use tokio::time::sleep;
use typesafe_repository::RefIdentity;

#[derive(Clone, Debug)]
pub enum RequestEvent {
    Request(ChatJoinRequest),
}

pub async fn request_wait_for_captcha(
    req: ChatJoinRequest,
    rx: Arc<EventRx<CaptchaEvent>>,
    chat_tx: Arc<EventTx<RequestEvent>>,
    bot: Bot,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
) -> Result<(), Error> {
    let rx = rx.resubscribe();
    chat_tx.send(RequestEvent::Request(req.clone()))?;
    let ok_or_tx = move |res| ok_or_tx(err_tx.clone(), Some(req.chat.id), res);
    tokio::spawn(async move {
        if ok_or_tx(wait_for_captcha_event(rx, bot, req).await)
            .await
            .is_none()
        {}
    });
    Ok(())
}

pub async fn wait_for_captcha_event(
    mut rx: EventRx<CaptchaEvent>,
    bot: Bot,
    req: ChatJoinRequest,
) -> Result<(), Error> {
    loop {
        let event = rx.recv().await?;
        match event {
            CaptchaEvent::Completed(c) if *c.user_id() == req.from.id => {
                bot.approve_chat_join_request(req.chat.id, req.from.id)
                    .await?;
                bot.send_message(
                    *c.chat_id(),
                    "You have solved captcha and your chat join request have been approved",
                )
                .await?;
                return Ok(());
            }
            CaptchaEvent::Timeout(c) if *c.user_id() == req.from.id => {
                bot.decline_chat_join_request(req.chat.id, req.from.id)
                    .await?;
                bot.send_message(
                    *c.chat_id(),
                    "You have failed to solve captcha and your chat join request have been declined",
                )
                .await?;
                return Ok(());
            }
            _ => (),
        };
    }
}

pub async fn start_chat(
    msg: Message,
    bot: Bot,
    config: Arc<Config>,
    request_config: Arc<JoinRequestConfig>,
    chat_id: ChatId,
    repo: Arc<dyn CaptchaRepository>,
    chat_rx: Arc<EventRx<RequestEvent>>,
    captcha_tx: Arc<EventTx<CaptchaEvent>>,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
) -> Result<(), Error> {
    let ok_or_tx = move |r| ok_or_tx(err_tx.clone(), Some(msg.chat.id), r);
    let user = msg
        .from()
        .cloned()
        .ok_or(anyhow!("Message with no sender"))?;
    tokio::spawn(async move {
        if ok_or_tx(send_link(chat_id, bot.clone(), msg.clone()).await)
            .await
            .is_none()
        {
            return;
        }
        if ok_or_tx(wait_for_chat_join_request(chat_rx, user.clone()).await)
            .await
            .is_none()
        {
            return;
        }
        match &request_config.method {
            ApprovalMethod::Captcha(c) => {
                let captcha_config = c
                    .as_ref()
                    .or(config.captcha.as_ref())
                    .cloned()
                    .unwrap_or_default();
                let res =
                    handle_captcha(repo, captcha_tx, msg.chat.id, user, &captcha_config, bot).await;
                if ok_or_tx(res).await.is_none() {}
            }
        }
    });
    Ok(())
}

pub async fn send_link(chat_id: ChatId, bot: Bot, msg: Message) -> Result<(), Error> {
    let link = generate_link(chat_id, bot.clone()).await?;
    bot.send_message(
        msg.chat.id,
        format!(
            "Use link to send join request to proceed\n{}",
            link.invite_link
        ),
    )
    .await?;
    Ok(())
}

pub async fn wait_for_chat_join_request(
    chat_rx: Arc<EventRx<RequestEvent>>,
    user: User,
) -> Result<(), Error> {
    let mut chat_rx = chat_rx.resubscribe();
    loop {
        match chat_rx.recv().await? {
            RequestEvent::Request(req) if req.from == user => return Ok(()),
            _ => continue,
        }
    }
}

pub async fn handle_captcha(
    repo: Arc<dyn CaptchaRepository>,
    captcha_tx: Arc<EventTx<CaptchaEvent>>,
    chat_id: ChatId,
    user: User,
    captcha_config: &CaptchaConfig,
    bot: Bot,
) -> Result<(), Error> {
    let caption = "Please solve captcha to get access to chat";
    let captcha = send_captcha(chat_id, &user, &bot, captcha_config, caption).await?;
    repo.add(captcha.clone()).await?;
    sleep(captcha_config.time_limit.to_std().context("Bug")?).await;
    repo.remove(captcha.id_ref()).await?;
    captcha_tx.send(CaptchaEvent::Timeout(captcha.clone()))?;
    Ok(())
}

pub async fn generate_link(chat_id: ChatId, bot: Bot) -> Result<ChatInviteLink, Error> {
    Ok(bot
        .create_chat_invite_link(chat_id)
        .creates_join_request(true)
        .await?)
}
