use anyhow::Error;
use config_extension::Entry;
use config_extension::OptionalEntry;

pub trait SetField {
    fn set_field(&mut self, name: &str, value: String) -> Result<(), Error>;
    fn supported() -> Vec<String>;
}

impl<T> SetField for OptionalEntry<T>
where
    T: SetField + Clone,
{
    fn set_field(&mut self, name: &str, value: String) -> Result<(), Error> {
        match self {
            Self::Some(t) => t.set_field(name, value),
            _ => Err(anyhow::anyhow!("Value not supported")),
        }
    }
    fn supported() -> Vec<String> {
        T::supported()
    }
}

impl<T> SetField for Entry<T>
where
    T: SetField + Clone,
{
    fn set_field(&mut self, name: &str, value: String) -> Result<(), Error> {
        match self {
            Self::Some(t) => t.set_field(name, value),
            _ => Err(anyhow::anyhow!("Value not supported")),
        }
    }
    fn supported() -> Vec<String> {
        T::supported()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chat_manager_derive::SetField;

    #[test]
    fn sets_field() {
        #[derive(SetField)]
        struct Foo {
            #[set_field]
            bar: String,
        }
        let mut f = Foo {
            bar: "bar".to_string(),
        };
        f.set_field("bar", "not bar".to_string()).unwrap();
        assert_eq!("not bar".to_string(), f.bar);
    }

    #[test]
    fn sets_nested_field() {
        #[derive(SetField)]
        struct Foo {
            bar: Bar,
        }
        #[derive(SetField)]
        struct Bar {
            #[set_field]
            value: String,
        }
        let mut f = Foo {
            bar: Bar {
                value: "bar".to_string(),
            },
        };
        f.set_field("bar.value", "not bar".to_string()).unwrap();
        assert_eq!("not bar".to_string(), f.bar.value);
    }

    #[test]
    fn sets_enum_variant() {
        #[derive(SetField)]
        struct Wrapper {
            #[set_field]
            test: Test,
        }
        #[derive(Debug, PartialEq, SetField)]
        enum Test {
            Foo,
            Bar,
        }
        let mut w = Wrapper { test: Test::Foo };
        w.set_field("test", "bar".to_string()).unwrap();
        assert_eq!(Test::Bar, w.test);
    }

    #[test]
    fn supported_list_is_correct() {
        #[derive(SetField)]
        struct Foo {
            #[set_field]
            test: String,
            bar: Bar,
        }
        #[derive(SetField)]
        struct Bar {
            #[set_field]
            value: String,
        }

        let expected = vec!["bar.value".to_string(), "test".to_string()].sort();
        assert_eq!(expected, Foo::supported().sort());

        let expected = vec!["value".to_string()];
        assert_eq!(expected, Bar::supported());
    }
}
