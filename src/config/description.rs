pub trait Description {
    fn outer_description<D: Descriptor>() -> Option<String> {
        None
    }
    fn description<D: Descriptor>() -> String {
        "No description provided".to_string()
    }
}

impl<T: Description> Description for Option<T> {
    fn description<D: Descriptor>() -> String {
        T::description::<D>()
    }
    fn outer_description<D: Descriptor>() -> Option<String> {
        T::outer_description::<D>()
    }
}

use config_pattern::Arguments;
use config_pattern::Pattern;

impl<A: Arguments> Description for Pattern<A> {
    fn outer_description<D: Descriptor>() -> Option<String> {
        let names: Vec<_> = A::names().into_iter().map(|a| a.into_inner()).collect();
        Some(D::arguments(names))
    }
}

pub trait Descriptor {
    fn entry(
        ident: &str,
        ty: Option<&str>,
        description: Option<&str>,
        notes: Option<&str>,
    ) -> String;
    fn map(ident: &str, ty: &str, content: &str, notes: Option<&str>) -> String;
    fn variant(ident: &str, description: Option<&str>) -> String;
    fn enum_variants(variants: Vec<(&str, Option<&str>)>) -> String;
    fn arguments<V: std::fmt::Debug>(args: Vec<V>) -> String {
        format!("available arguments: {args:?}")
    }
}

pub struct MarkdownDescriptor;

impl MarkdownDescriptor {
    fn format_ty(ty: &str) -> String {
        Self::escape(&ty.replace(" ", ""))
    }
    fn escape(s: &str) -> String {
        s.replace("_", "\\_")
    }
}

impl Descriptor for MarkdownDescriptor {
    fn entry(
        ident: &str,
        ty: Option<&str>,
        description: Option<&str>,
        notes: Option<&str>,
    ) -> String {
        static DEFAULT_DESCRIPTION: &str = "no description provided";
        let ident = Self::escape(ident);
        let ty = ty.clone().map(|ty| Self::format_ty(ty));
        let d = description
            .filter(|s| !s.is_empty())
            .unwrap_or(DEFAULT_DESCRIPTION);
        match (ty, notes) {
            (Some(ty), Some(notes)) => {
                format!("\n- {ident}: `{ty}`\n  {notes}\n  description: {d}")
            }
            (None, Some(notes)) => format!("\n- {ident}:\n  {notes}\n  description: {d}"),
            (Some(ty), None) => format!("\n- {ident}: `{ty}`\n  description: {d}"),
            (None, None) => format!("\n- {ident}\n  description: {d}"),
        }
    }
    fn map(ident: &str, ty: &str, content: &str, notes: Option<&str>) -> String {
        let ident = Self::escape(ident);
        let ty = Self::format_ty(ty);
        if let Some(notes) = notes {
            format!("\n- {ident}: `{ty}`\n  {notes}{content}")
        } else {
            format!("\n- {ident}: `{ty}`{content}")
        }
    }
    fn variant(ident: &str, description: Option<&str>) -> String {
        let mut res = format!("\n- {ident}");
        if let Some(d) = description {
            res.push_str(&format!(": {d}"));
        }
        res
    }
    fn enum_variants(variants: Vec<(&str, Option<&str>)>) -> String {
        let mut res = String::new();
        for (i, d) in variants {
            res.push_str(&Self::variant(i, d));
        }
        res
    }
}

pub struct GendocDescriptor;

impl GendocDescriptor {
    fn format_ty(ty: &str) -> String {
        ty.replace(" ", "")
    }
    fn escape(v: &str) -> String {
        v.replace('<', "&lt;").replace('>', "&gt;")
    }
    fn increment_nesting(v: &str) -> String {
        v.replace("h5", "h6")
            .replace("h4", "h5")
            .replace("h3", "h4")
            .replace("h2", "h3")
            .replace("h1", "h2")
    }
}

impl Descriptor for GendocDescriptor {
    fn entry(
        ident: &str,
        ty: Option<&str>,
        description: Option<&str>,
        notes: Option<&str>,
    ) -> String {
        static DEFAULT_DESCRIPTION: &str = "no description provided";
        let ty = ty.map(Self::format_ty);
        let description = description
            .filter(|s| !s.is_empty())
            .map(Self::increment_nesting)
            .map(|x| x.replace("\n", ""))
            .unwrap_or_else(|| DEFAULT_DESCRIPTION.to_string());
        let mut res = format!("<h2>{ident}</h2>");
        if let Some(ty) = ty {
            res.push_str(&format!("Type: <tt>{ty}</tt>"));
        }
        if let Some(notes) = notes {
            res.push_str(&format!("<p>{notes}</p>"));
        }
        res.push_str(&format!("<p>{description}</p>"));
        res
    }
    fn map(ident: &str, ty: &str, content: &str, notes: Option<&str>) -> String {
        let content = Self::increment_nesting(content).replace("\n", "");
        let mut res = format!("<h2>{ident}</h2>");
        if ty.contains("Option") {
            res.push_str("<p>Optional</p>");
        }
        if let Some(notes) = notes {
            res.push_str(&format!("<p>{notes}</p>"));
        }
        res.push_str(&format!("<p>{content}</p>"));
        res
    }
    fn arguments<V: std::fmt::Debug>(args: Vec<V>) -> String {
        format!("available arguments: <tt>{args:?}</tt>")
    }
    fn variant(ident: &str, description: Option<&str>) -> String {
        let mut res = format!("<li>{ident}");
        if let Some(d) = description {
            res.push_str(&format!("<p>{d}</p>"));
        }
        res.push_str("</li>");
        res
    }
    fn enum_variants(variants: Vec<(&str, Option<&str>)>) -> String {
        let mut res = "<ul>".to_string();
        for (i, d) in variants {
            res.push_str(&Self::variant(i, d));
        }
        res.push_str("</ul>");
        res
    }
}
