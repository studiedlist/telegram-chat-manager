use super::dto::ConfigDto;
use super::Config;
use serde::de::Error as SerdeErr;
use serde_yaml::Error;
use std::path::Path;

pub fn parse_config(p: &Path) -> Result<Config, Error> {
    let content = std::fs::read_to_string(p).map_err(Error::custom)?;
    let c: ConfigDto = serde_yaml::from_str(&content)?;
    c.try_into().map_err(Error::custom)
}
