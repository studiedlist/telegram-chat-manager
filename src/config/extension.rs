use super::arguments::Id;
use super::arguments::Reason;
use super::arguments::RegenerateMessage;
use super::arguments::RetryMessage;
use super::arguments::Username;
use super::dto::AccessLevelDto;
use super::dto::ActionDto;
use super::dto::ApprovalMethodDto;
use super::dto::AutoCommandDto;
use super::dto::CaptchaFilterDto;
use super::dto::KickBehaviorDto;
use super::dto::ReportActionDto;
use super::set_field::SetField;
use super::AdminConfig;
use super::AntispamConfig;
use super::CaptchaConfig;
use super::CommandConfig;
use super::Config;
use super::GenOptions;
use super::JoinRequestConfig;
use super::KickBehavior;
use super::ReportAction;
use super::ReportConfig;
use super::RulesConfig;
use super::TimeoutConfig;
use super::DEFAULT_REGENERATE_LIMIT;
use super::DEFAULT_REPORT_NOTIFY;
use super::DEFAULT_RETRY_LIMIT;
use super::DEFAULT_TIMEOUT;
use super::DEFAULT_TIME_LIMIT;
use crate::parse_duration;
use anyhow::Error;
use chat_manager_derive::SetField;
use config_extension::Entry;
use config_extension::Extender;
use config_extension::OptionalEntry;
use config_pattern::Pattern;
use once_cell::sync::Lazy;
use serde::Deserialize;
use serde::Serialize;

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct ConfigExtension {
    #[set_field]
    pub greeting: OptionalEntry<Pattern<Username>>,
    #[set_field]
    pub goodbye: OptionalEntry<Pattern<Username>>,
    pub timeout: OptionalEntry<TimeoutExtension>,
    pub admin: Entry<AdminConfigExtension>,
    pub rules: OptionalEntry<RulesConfigExtension>,
    pub captcha: OptionalEntry<CaptchaConfigExtension>,
    pub command: Entry<CommandConfigExtension>,
    pub report: OptionalEntry<ReportConfigExtension>,
    pub join_request: OptionalEntry<JoinRequestConfigExtension>,
    #[skip_field]
    pub autocommands: OptionalEntry<Vec<AutoCommandDto>>,
    pub antispam: OptionalEntry<AntispamConfigExtension>,
}

impl Default for ConfigExtension {
    fn default() -> Self {
        Self {
            greeting: OptionalEntry::Super,
            goodbye: OptionalEntry::Super,
            timeout: OptionalEntry::Some(TimeoutExtension::default()),
            admin: Entry::Some(AdminConfigExtension::default()),
            rules: OptionalEntry::Super,
            captcha: OptionalEntry::Some(CaptchaConfigExtension::default()),
            command: Entry::Some(CommandConfigExtension::default()),
            report: OptionalEntry::Some(ReportConfigExtension::default()),
            join_request: OptionalEntry::Some(JoinRequestConfigExtension::default()),
            autocommands: OptionalEntry::Super,
            antispam: OptionalEntry::Super,
        }
    }
}

impl Extender<Config> for ConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &Config) -> Result<Config, Error> {
        let greeting = self.greeting.unzip().unwrap_or(c.greeting.clone());
        let goodbye = self.goodbye.unzip().unwrap_or(c.goodbye.clone());
        let admin = match self.admin {
            Entry::Some(x) => x.try_extend(&c.admin)?,
            Entry::Super => c.admin.clone(),
        };
        let captcha = match self.captcha {
            OptionalEntry::Some(x) => c.captcha.clone().map(|c| x.try_extend(&c)).transpose()?,
            OptionalEntry::Super => c.captcha.clone(),
            OptionalEntry::None => None,
        };
        let command = match self.command {
            Entry::Some(x) => x.try_extend(&c.command)?,
            Entry::Super => c.command.clone(),
        };
        let timeout = match self.timeout {
            OptionalEntry::Some(x) => x.try_extend(&c.timeout)?,
            OptionalEntry::None => TimeoutConfig::default(),
            OptionalEntry::Super => c.timeout.clone(),
        };
        let report = match self.report {
            OptionalEntry::Some(x) => c.report.clone().map(|r| x.try_extend(&r)).transpose()?,
            OptionalEntry::None => None,
            OptionalEntry::Super => c.report.clone(),
        };
        let join_request = match self.join_request {
            OptionalEntry::Some(x) => c
                .join_request
                .clone()
                .map(|r| x.try_extend(&r))
                .transpose()?,
            OptionalEntry::None => None,
            OptionalEntry::Super => c.join_request.clone(),
        };
        let autocommands = match self.autocommands {
            OptionalEntry::Some(x) => Some(
                x.into_iter()
                    .map(TryInto::try_into)
                    .collect::<Result<_, _>>()?,
            ),
            OptionalEntry::None => None,
            OptionalEntry::Super => c.autocommands.clone(),
        };
        let antispam = match self.antispam {
            OptionalEntry::Some(antispam) => c
                .antispam
                .as_ref()
                .map(|c| antispam.try_extend(c))
                .transpose()?,
            OptionalEntry::None => None,
            OptionalEntry::Super => c.antispam.clone(),
        };
        let rules = match self.rules {
            OptionalEntry::Some(rules) => {
                c.rules.as_ref().map(|c| rules.try_extend(c)).transpose()?
            }
            OptionalEntry::None => None,
            OptionalEntry::Super => c.rules.clone(),
        };

        Ok(Config {
            timeout,
            greeting,
            goodbye,
            admin,
            rules,
            captcha,
            command,
            report,
            join_request,
            autocommands,
            antispam,
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct TimeoutExtension {
    #[set_field]
    pub default_timeout: OptionalEntry<String>,
}

impl Default for TimeoutExtension {
    fn default() -> Self {
        Self {
            default_timeout: OptionalEntry::Super,
        }
    }
}

impl Extender<TimeoutConfig> for TimeoutExtension {
    type Error = Error;

    fn try_extend(self, c: &TimeoutConfig) -> Result<TimeoutConfig, Error> {
        let default_timeout = match self.default_timeout {
            OptionalEntry::Some(x) => parse_duration(&x)?,
            OptionalEntry::None => *Lazy::force(&DEFAULT_TIMEOUT),
            OptionalEntry::Super => c.default_timeout,
        };
        Ok(TimeoutConfig { default_timeout })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct AdminConfigExtension {
    #[set_field]
    pub banned: OptionalEntry<Pattern<Username>>,
    #[set_field]
    pub kicked: OptionalEntry<Pattern<Username>>,
    #[set_field]
    pub muted: OptionalEntry<Pattern<Username>>,
    #[set_field]
    pub reason: OptionalEntry<Pattern<Reason>>,
    #[set_field]
    pub kick_behavior: OptionalEntry<KickBehaviorDto>,
}

impl Default for AdminConfigExtension {
    fn default() -> Self {
        Self {
            banned: OptionalEntry::Super,
            kicked: OptionalEntry::Super,
            muted: OptionalEntry::Super,
            kick_behavior: OptionalEntry::Super,
            reason: OptionalEntry::Super,
        }
    }
}

impl Extender<AdminConfig> for AdminConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &AdminConfig) -> Result<AdminConfig, Error> {
        let banned = self.banned.unzip().unwrap_or(c.banned.clone());
        let kicked = self.kicked.unzip().unwrap_or(c.kicked.clone());
        let muted = self.muted.unzip().unwrap_or(c.muted.clone());
        let kick_behavior = match self.kick_behavior {
            OptionalEntry::Some(x) => x.into(),
            OptionalEntry::Super => c.kick_behavior.clone(),
            OptionalEntry::None => KickBehavior::default(),
        };
        let reason = self.reason.unzip().unwrap_or(c.reason.clone());
        Ok(AdminConfig {
            banned,
            kicked,
            muted,
            kick_behavior,
            reason,
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct CaptchaConfigExtension {
    #[set_field]
    pub captcha_timeout: OptionalEntry<String>,
    #[set_field]
    pub captcha_caption: Entry<Pattern<Username>>,
    #[set_field]
    pub failure_message: OptionalEntry<Pattern<Username>>,
    #[set_field]
    pub success_message: OptionalEntry<Pattern<Username>>,
    #[set_field]
    pub retry_message: Entry<Pattern<RetryMessage>>,
    #[set_field]
    pub regenerate_message: Entry<Pattern<RegenerateMessage>>,
    #[set_field]
    pub regenerate_limit_reached: Entry<String>,
    #[set_field]
    pub retry_limit: OptionalEntry<usize>,
    #[set_field]
    pub time_limit: OptionalEntry<String>,
    #[set_field]
    pub regenerate_limit: OptionalEntry<usize>,
    #[set_field]
    pub ban_time: Entry<String>,
    pub gen_options: OptionalEntry<GenOptionsExtension>,
}

impl Default for CaptchaConfigExtension {
    fn default() -> Self {
        Self {
            captcha_timeout: OptionalEntry::Super,
            captcha_caption: Entry::Super,
            failure_message: OptionalEntry::Super,
            success_message: OptionalEntry::Super,
            retry_message: Entry::Super,
            regenerate_message: Entry::Super,
            regenerate_limit_reached: Entry::Super,
            regenerate_limit: OptionalEntry::Super,
            retry_limit: OptionalEntry::Super,
            time_limit: OptionalEntry::Super,
            ban_time: Entry::Super,
            gen_options: OptionalEntry::Some(GenOptionsExtension::default()),
        }
    }
}

impl Extender<CaptchaConfig> for CaptchaConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &CaptchaConfig) -> Result<CaptchaConfig, Error> {
        let captcha_caption = self
            .captcha_caption
            .option()
            .unwrap_or(c.captcha_caption.clone());
        let failure_message = self.failure_message.unwrap_or(c.failure_message.clone());
        let success_message = self.success_message.unwrap_or(c.success_message.clone());
        let retry_message = self
            .retry_message
            .option()
            .unwrap_or(c.retry_message.clone());
        let regenerate_message = self
            .regenerate_message
            .option()
            .unwrap_or(c.regenerate_message.clone());
        let regenerate_limit_reached = self
            .regenerate_limit_reached
            .option()
            .unwrap_or(c.regenerate_limit_reached.clone());
        let captcha_timeout = match self.captcha_timeout {
            OptionalEntry::Some(x) => Some(parse_duration(&x)?),
            OptionalEntry::None => None,
            OptionalEntry::Super => c.captcha_timeout,
        };
        let retry_limit = match self.retry_limit {
            OptionalEntry::Some(x) => x,
            OptionalEntry::None => DEFAULT_RETRY_LIMIT,
            OptionalEntry::Super => c.retry_limit,
        };
        let time_limit = match self.time_limit {
            OptionalEntry::Some(x) => parse_duration(&x)?,
            OptionalEntry::None => *Lazy::force(&DEFAULT_TIME_LIMIT),
            OptionalEntry::Super => c.time_limit,
        };
        let regenerate_limit = match self.regenerate_limit {
            OptionalEntry::Some(x) => x,
            OptionalEntry::None => DEFAULT_REGENERATE_LIMIT,
            OptionalEntry::Super => c.regenerate_limit,
        };
        let ban_time = self
            .ban_time
            .option()
            .map(|x| parse_duration(&x))
            .transpose()?;
        let gen_options = self
            .gen_options
            .unzip()
            .flatten()
            .map(|g| g.try_extend(&c.gen_options))
            .transpose()?
            .unwrap_or(c.gen_options.clone());
        Ok(CaptchaConfig {
            captcha_timeout,
            captcha_caption,
            failure_message,
            success_message,
            retry_message,
            regenerate_message,
            regenerate_limit_reached,
            retry_limit,
            time_limit,
            regenerate_limit,
            ban_time,
            gen_options,
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct GenOptionsExtension {
    #[set_field]
    pub chars: Entry<u32>,
    #[skip_field]
    pub view: OptionalEntry<(u32, u32)>,
    #[skip_field]
    pub filters: Entry<Vec<CaptchaFilterDto>>,
}

impl Default for GenOptionsExtension {
    fn default() -> Self {
        Self {
            chars: Entry::Super,
            view: OptionalEntry::Super,
            filters: Entry::Super,
        }
    }
}

impl Extender<GenOptions> for GenOptionsExtension {
    type Error = Error;

    fn try_extend(self, g: &GenOptions) -> Result<GenOptions, Error> {
        let chars = self.chars.option().unwrap_or(g.chars);
        let view = self.view.unzip().unwrap_or(g.view);
        let filters = match self.filters {
            Entry::Some(x) => x
                .into_iter()
                .map(TryInto::try_into)
                .collect::<Result<_, Error>>()?,
            Entry::Super => g.filters.clone(),
        };
        Ok(GenOptions {
            chars,
            view,
            filters,
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct CommandConfigExtension {
    #[set_field]
    pub id: OptionalEntry<AccessLevelDto>,
}

impl Default for CommandConfigExtension {
    fn default() -> Self {
        Self {
            id: OptionalEntry::Super,
        }
    }
}

impl Extender<CommandConfig> for CommandConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &CommandConfig) -> Result<CommandConfig, Error> {
        Ok(CommandConfig {
            id: self
                .id
                .unzip()
                .map(|x| x.map(Into::into))
                .unwrap_or_else(|| c.id.clone()),
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct ReportConfigExtension {
    #[set_field]
    pub report_notify: OptionalEntry<Pattern<Id>>,
    #[skip_field]
    pub actions: Entry<Vec<ReportActionDto>>,
}

impl Default for ReportConfigExtension {
    fn default() -> Self {
        Self {
            report_notify: OptionalEntry::Super,
            actions: Entry::Super,
        }
    }
}

impl Extender<ReportConfig> for ReportConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &ReportConfig) -> Result<ReportConfig, Error> {
        let actions = self
            .actions
            .option()
            .map(|x| {
                x.into_iter()
                    .map(TryInto::<ReportAction>::try_into)
                    .collect::<Result<_, _>>()
            })
            .transpose()?
            .unwrap_or_else(|| c.actions.clone());
        let report_notify = match self.report_notify {
            OptionalEntry::Some(r) => r,
            OptionalEntry::None => DEFAULT_REPORT_NOTIFY.try_into()?,
            OptionalEntry::Super => c.report_notify.clone(),
        };
        Ok(ReportConfig {
            report_notify,
            actions,
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct JoinRequestConfigExtension {
    #[set_field]
    pub method: Entry<ApprovalMethodDto>,
}

impl Default for JoinRequestConfigExtension {
    fn default() -> Self {
        Self {
            method: Entry::Super,
        }
    }
}

impl Extender<JoinRequestConfig> for JoinRequestConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &JoinRequestConfig) -> Result<JoinRequestConfig, Error> {
        let method = self
            .method
            .option()
            .map(TryInto::try_into)
            .transpose()?
            .unwrap_or_else(|| c.method.clone());
        Ok(JoinRequestConfig { method })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct AntispamConfigExtension {
    #[set_field]
    pub limit: Entry<u8>,
    #[skip_field]
    pub actions: Entry<Vec<ActionDto>>,
}

impl Extender<AntispamConfig> for AntispamConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &AntispamConfig) -> Result<AntispamConfig, Error> {
        let actions = match self.actions {
            Entry::Some(actions) => actions
                .into_iter()
                .map(TryInto::try_into)
                .collect::<Result<_, _>>()?,
            Entry::Super => c.actions.clone(),
        };
        let limit = match self.limit {
            Entry::Some(limit) => limit,
            Entry::Super => c.limit,
        };
        Ok(AntispamConfig { limit, actions })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub struct RulesConfigExtension {
    #[set_field]
    pub rules: OptionalEntry<String>,
    #[set_field]
    pub generate_from_autocommands: OptionalEntry<bool>,
}

impl Extender<RulesConfig> for RulesConfigExtension {
    type Error = Error;

    fn try_extend(self, c: &RulesConfig) -> Result<RulesConfig, Error> {
        let rules = match self.rules {
            OptionalEntry::Some(rules) => Some(rules),
            OptionalEntry::None => None,
            OptionalEntry::Super => c.rules.clone(),
        };
        let generate_from_autocommands = match self.generate_from_autocommands {
            OptionalEntry::Some(g) => g,
            OptionalEntry::None => c.generate_from_autocommands,
            OptionalEntry::Super => c.generate_from_autocommands,
        };
        Ok(RulesConfig {
            rules,
            generate_from_autocommands,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn serialization_invertible() {
        let e = ConfigExtension::default();
        let result = serde_yaml::to_string(&e).unwrap();
        let e1: ConfigExtension = serde_yaml::from_str(&result).unwrap();
        let result1 = serde_yaml::to_string(&e1).unwrap();

        assert_eq!(result, result1);
    }

    mod command_config {
        use super::*;
        use crate::config::AccessLevel;

        #[test]
        fn extends_super() {
            let do_test = |input| {
                let base = CommandConfig { id: None };
                let e: CommandConfigExtension = serde_yaml::from_str(input).unwrap();
                let base = e.try_extend(&base).unwrap();
                assert!(base.id.is_none());
            };
            do_test("id: super");
            do_test("id:");
            do_test("");
        }

        #[test]
        fn overrides_super() {
            let yaml = "
                id: admin
            ";
            let base = CommandConfig { id: None };
            let e: CommandConfigExtension = serde_yaml::from_str(yaml).unwrap();
            let base = e.try_extend(&base).unwrap();
            assert!(matches!(base.id, Some(AccessLevel::Admin(_))));
        }
    }
}
