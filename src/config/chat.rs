use super::extension::ConfigExtension;
use anyhow::Error;
use derive_new::new;
use std::fs::File;
use std::io;
use std::io::BufWriter;
use std::path::PathBuf;
use teloxide::types::ChatId;
use typesafe_repository::macros::Id;
use typesafe_repository::ops::Get;
use typesafe_repository::ops::Remove;
use typesafe_repository::ops::Save;
use typesafe_repository::prelude::*;

#[derive(Id)]
#[id(id_ref, id_get)]
pub struct ChatConfig {
    pub id: ChatId,
    pub config: ConfigExtension,
}

impl ChatConfig {
    pub fn new(id: ChatId, config: ConfigExtension) -> Self {
        Self { id, config }
    }
}

pub trait ChatConfigRepository:
    Repository<ChatConfig, Error = Error>
    + Get<ChatConfig>
    + Save<ChatConfig>
    + Remove<ChatConfig>
    + Send
    + Sync
{
}

#[derive(new)]
pub struct FilesystemChatConfigRepository {
    config_dir: PathBuf,
}

impl Default for FilesystemChatConfigRepository {
    fn default() -> Self {
        Self {
            config_dir: PathBuf::from("cfg.d"),
        }
    }
}

impl FilesystemChatConfigRepository {
    const CONFIG_EXTENSION: &'static str = "yaml";

    fn get_config_path(&self, id: &IdentityOf<ChatConfig>) -> PathBuf {
        self.config_dir
            .join(format!("{}.{}", id, Self::CONFIG_EXTENSION))
    }
}

impl Repository<ChatConfig> for FilesystemChatConfigRepository {
    type Error = Error;
}

impl Get<ChatConfig> for FilesystemChatConfigRepository {
    fn get_one(&self, id: &IdentityOf<ChatConfig>) -> Result<Option<ChatConfig>, Error> {
        let filename = self.get_config_path(id);
        let file = match File::open(filename) {
            Ok(f) => f,
            Err(err) if err.kind() == io::ErrorKind::NotFound => return Ok(None),
            Err(err) => return Err(err.into()),
        };
        let reader = std::io::BufReader::new(file);
        let config: ConfigExtension = serde_yaml::from_reader(reader)?;
        let id = *id;
        Ok(Some(ChatConfig { id, config }))
    }
}

impl Save<ChatConfig> for FilesystemChatConfigRepository {
    fn save(&self, c: ChatConfig) -> Result<(), Error> {
        let filename = self.get_config_path(&c.id);
        let file = match File::create(&filename) {
            Ok(f) => f,
            Err(err) if err.kind() == io::ErrorKind::NotFound => {
                std::fs::create_dir_all(self.config_dir.clone())?;
                File::create(filename)?
            }
            Err(err) => return Err(err.into()),
        };
        let writer = BufWriter::new(file);
        serde_yaml::to_writer(writer, &c.config)?;
        Ok(())
    }
}

impl Remove<ChatConfig> for FilesystemChatConfigRepository {
    fn remove(&self, id: &IdentityOf<ChatConfig>) -> Result<(), Error> {
        let filename = self.get_config_path(id);
        std::fs::remove_file(filename)?;
        Ok(())
    }
}

impl ChatConfigRepository for FilesystemChatConfigRepository {}
