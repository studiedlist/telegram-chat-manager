#![allow(clippy::from_over_into)]

use super::arguments::Id;
use super::arguments::Reason;
use super::arguments::RegenerateMessage;
use super::arguments::RetryMessage;
use super::arguments::Username;
use super::AccessLevel;
use super::Action;
use super::AdminAccess;
use super::AdminConfig;
use super::AdminPermission;
use super::AdminPermissions;
use super::AntispamConfig;
use super::ApprovalMethod;
use super::AutoCommand;
use super::CaptchaConfig;
use super::CommandConfig;
use super::Config;
use super::Filter;
use super::GenOptions;
use super::JoinRequestConfig;
use super::KickBehavior;
use super::Regex;
use super::ReportAction;
use super::ReportConfig;
use super::RulesConfig;
use super::TimeoutConfig;
use super::Trigger;
use super::UserAccess;
use super::DEFAULT_REGENERATE_LIMIT;
use super::DEFAULT_REPORT_ACTIONS;
use super::DEFAULT_REPORT_NOTIFY;
use super::DEFAULT_RETRY_LIMIT;
use super::DEFAULT_TIMEOUT;
use super::DEFAULT_TIME_LIMIT;
use crate::parse_duration;
use anyhow::Context;
use anyhow::Error;
use captcha::filters::Cow;
use captcha::filters::Dots;
use captcha::filters::Grid;
use captcha::filters::Noise;
use captcha::filters::Wave;
use chat_manager_derive::SetField;
use config_pattern::Pattern;
use once_cell::sync::Lazy;
use serde::Deserialize;
use serde::Serialize;
use std::str::FromStr;

pub fn try_into<T, P, E>(x: Option<T>) -> Result<Option<P>, E>
where
    T: TryInto<P, Error = E>,
{
    x.map(TryInto::try_into).transpose()
}

fn err_context<T>(x: Result<T, Error>, name: &str) -> Result<T, Error> {
    x.context(format!("`{name}` is invalid"))
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ConfigDto {
    pub greeting: Option<Pattern<Username>>,
    pub goodbye: Option<Pattern<Username>>,
    pub timeout: Option<TimeoutConfigDto>,
    pub admin: Option<AdminConfigDto>,
    pub rules: Option<RulesConfigDto>,
    pub captcha: Option<CaptchaConfigDto>,
    pub command: Option<CommandConfigDto>,
    pub report: Option<ReportConfigDto>,
    pub join_request: Option<JoinRequestConfigDto>,
    pub autocommands: Option<Vec<AutoCommandDto>>,
    pub antispam: Option<AntispamConfigDto>,
}

impl TryInto<Config> for ConfigDto {
    type Error = Error;
    fn try_into(self) -> Result<Config, Error> {
        let autocommands = self
            .autocommands
            .map(|x| {
                x.into_iter()
                    .map(TryInto::try_into)
                    .collect::<Result<_, _>>()
            })
            .transpose()?;
        Ok(Config {
            greeting: self.greeting,
            goodbye: self.goodbye,
            timeout: try_into(self.timeout)?.unwrap_or_default(),
            admin: try_into(self.admin)?.unwrap_or_default(),
            rules: try_into(self.rules)?,
            captcha: try_into(self.captcha)?,
            command: self.command.map(Into::into).unwrap_or_default(),
            report: try_into(self.report)?,
            join_request: try_into(self.join_request)?,
            autocommands,
            antispam: try_into(self.antispam)?,
        })
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct TimeoutConfigDto {
    pub default_timeout: Option<String>,
}

impl TryInto<TimeoutConfig> for TimeoutConfigDto {
    type Error = Error;

    fn try_into(self) -> Result<TimeoutConfig, Error> {
        Ok(TimeoutConfig {
            default_timeout: self
                .default_timeout
                .map(|x| parse_duration(&x))
                .transpose()?
                .unwrap_or(*Lazy::force(&DEFAULT_TIMEOUT)),
        })
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct AdminConfigDto {
    pub banned: Option<Pattern<Username>>,
    pub kicked: Option<Pattern<Username>>,
    pub muted: Option<Pattern<Username>>,
    pub reason: Option<Pattern<Reason>>,
    pub kick_behavior: Option<KickBehaviorDto>,
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
pub enum KickBehaviorDto {
    Telegram,
    KickAndUnban,
}

impl Into<KickBehavior> for KickBehaviorDto {
    fn into(self) -> KickBehavior {
        match self {
            Self::Telegram => KickBehavior::Telegram,
            Self::KickAndUnban => KickBehavior::KickAndUnban,
        }
    }
}

impl TryInto<AdminConfig> for AdminConfigDto {
    type Error = Error;
    fn try_into(self) -> Result<AdminConfig, Error> {
        Ok(AdminConfig {
            banned: self.banned,
            kicked: self.kicked,
            muted: self.muted,
            kick_behavior: self.kick_behavior.map(Into::into).unwrap_or_default(),
            reason: self.reason,
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CaptchaConfigDto {
    pub captcha_timeout: Option<String>,
    pub captcha_caption: Pattern<Username>,
    pub failure_message: Option<Pattern<Username>>,
    pub success_message: Option<Pattern<Username>>,
    pub retry_message: Pattern<RetryMessage>,
    pub regenerate_message: Pattern<RegenerateMessage>,
    pub regenerate_limit_reached: String,
    pub retry_limit: Option<usize>,
    pub time_limit: Option<String>,
    pub regenerate_limit: Option<usize>,
    pub ban_time: Option<String>,
    pub gen_options: Option<GenOptionsDto>,
}

impl TryInto<CaptchaConfig> for CaptchaConfigDto {
    type Error = Error;
    fn try_into(self) -> Result<CaptchaConfig, Error> {
        fn context<T>(x: Result<T, Error>, name: &str) -> Result<T, Error> {
            err_context(x, &format!("captcha.{name}"))
        }
        Ok(CaptchaConfig {
            captcha_timeout: context(
                self.captcha_timeout.map(|x| parse_duration(&x)).transpose(),
                "captcha_timeout",
            )?,
            captcha_caption: self.captcha_caption,
            failure_message: self.failure_message,
            success_message: self.success_message,
            retry_message: self.retry_message,
            regenerate_message: self.regenerate_message,
            regenerate_limit_reached: self.regenerate_limit_reached,
            retry_limit: self.retry_limit.unwrap_or(DEFAULT_RETRY_LIMIT),
            regenerate_limit: self.regenerate_limit.unwrap_or(DEFAULT_REGENERATE_LIMIT),
            time_limit: context(
                self.time_limit.map(|x| parse_duration(&x)).transpose(),
                "time_limit",
            )?
            .unwrap_or_else(|| *Lazy::force(&DEFAULT_TIME_LIMIT)),
            ban_time: context(
                self.ban_time.map(|x| parse_duration(&x)).transpose(),
                "ban time",
            )?,
            gen_options: try_into(self.gen_options)?.unwrap_or_default(),
        })
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct GenOptionsDto {
    pub chars: u32,
    pub view: Option<(u32, u32)>,
    pub filters: Vec<CaptchaFilterDto>,
}

impl TryInto<GenOptions> for GenOptionsDto {
    type Error = Error;
    fn try_into(self) -> Result<GenOptions, Error> {
        fn context<T>(x: Result<T, Error>, name: &str) -> Result<T, Error> {
            err_context(x, &format!("gen_options.{name}"))
        }
        Ok(GenOptions {
            chars: self.chars,
            view: self.view,
            filters: context(
                self.filters.into_iter().map(TryInto::try_into).collect(),
                "filters",
            )?,
        })
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum CaptchaFilterDto {
    Cow {
        circles: Option<u32>,
        min_radius: Option<u32>,
        max_radius: Option<u32>,
    },
    Dots {
        n: u32,
        min_radius: Option<u32>,
        max_radius: Option<u32>,
    },
    Grid {
        x_gap: u32,
        y_gap: u32,
    },
    Noise {
        prob: f32,
    },
    Wave {
        f: f64,
        amp: f64,
    },
}

impl TryInto<Filter> for CaptchaFilterDto {
    type Error = Error;
    fn try_into(self) -> Result<Filter, Error> {
        Ok(match self {
            Self::Cow {
                circles,
                min_radius,
                max_radius,
            } => {
                let mut f = Cow::new();
                if let Some(c) = circles {
                    f = f.circles(c);
                }
                if let Some(min) = min_radius {
                    f = f.min_radius(min);
                }
                if let Some(max) = max_radius {
                    f = f.max_radius(max);
                }
                f.into()
            }
            Self::Dots {
                n,
                min_radius,
                max_radius,
            } => {
                let mut f = Dots::new(n);
                if let Some(min) = min_radius {
                    f = f.min_radius(min);
                }
                if let Some(max) = max_radius {
                    f = f.max_radius(max);
                }
                f.into()
            }
            Self::Grid { x_gap, y_gap } => Grid::new(x_gap, y_gap).into(),
            Self::Noise { prob } => Noise::new(prob).into(),
            Self::Wave { f, amp } => Wave::new(f, amp).into(),
        })
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase", untagged)]
pub enum AccessLevelDto {
    Admin(AdminAccessDto),
    User(UserAccessDto),
}

impl Into<AccessLevel> for AccessLevelDto {
    fn into(self) -> AccessLevel {
        match self {
            Self::Admin(a) => AccessLevel::Admin(a.into()),
            Self::User(u) => AccessLevel::User(u.into()),
        }
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum AdminAccessDto {
    Permissions(AdminPermissionsDto),
    Owner,
    Admin,
}

impl Into<AdminAccess> for AdminAccessDto {
    fn into(self) -> AdminAccess {
        match self {
            Self::Permissions(p) => AdminAccess::Permissions(p.into()),
            Self::Owner => AdminAccess::Owner,
            Self::Admin => AdminAccess::Admin,
        }
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum UserAccessDto {
    User,
}

impl Into<UserAccess> for UserAccessDto {
    fn into(self) -> UserAccess {
        match self {
            Self::User => UserAccess::User,
        }
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(transparent)]
pub struct AdminPermissionsDto(Vec<AdminPermissionDto>);

impl Into<AdminPermissions> for AdminPermissionsDto {
    fn into(self) -> AdminPermissions {
        AdminPermissions(self.0.into_iter().map(Into::into).collect())
    }
}

impl FromStr for AdminPermissionsDto {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.replace('+', ",");
        let split = s.split(',');
        let v = split
            .map(str::trim)
            .map(str::parse)
            .collect::<Result<_, _>>()?;
        Ok(Self(v))
    }
}

impl TryFrom<String> for AdminPermissionsDto {
    type Error = <Self as FromStr>::Err;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        s.parse()
    }
}

#[derive(SetField, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum AdminPermissionDto {
    CanRestrictMembers,
    CanChangeInfo,
}

impl Into<AdminPermission> for AdminPermissionDto {
    fn into(self) -> AdminPermission {
        match self {
            AdminPermissionDto::CanRestrictMembers => AdminPermission::CanRestrictMembers,
            AdminPermissionDto::CanChangeInfo => AdminPermission::CanChangeInfo,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct CommandConfigDto {
    pub id: Option<AccessLevelDto>,
    pub rules: Option<String>,
}

impl Into<CommandConfig> for CommandConfigDto {
    fn into(self) -> CommandConfig {
        CommandConfig {
            id: self.id.map(Into::into),
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ReportConfigDto {
    pub report_notify: Option<Pattern<Id>>,
    pub actions: Option<Vec<ReportActionDto>>,
}

impl TryInto<ReportConfig> for ReportConfigDto {
    type Error = Error;

    fn try_into(self) -> Result<ReportConfig, Error> {
        let actions = self
            .actions
            .map(|x| {
                x.into_iter()
                    .map(TryInto::<ReportAction>::try_into)
                    .collect::<Result<_, _>>()
            })
            .transpose()?
            .unwrap_or_else(|| Lazy::force(&DEFAULT_REPORT_ACTIONS).clone());
        let report_notify = match self.report_notify {
            Some(r) => r,
            None => DEFAULT_REPORT_NOTIFY.try_into()?,
        };
        Ok(ReportConfig {
            report_notify,
            actions,
        })
    }
}

#[derive(SetField, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case", tag = "type")]
pub enum ReportActionDto {
    Ban,
    Mute { time: String },
    Delete,
}

impl TryInto<ReportAction> for ReportActionDto {
    type Error = Error;

    fn try_into(self) -> Result<ReportAction, Error> {
        Ok(match self {
            Self::Ban => ReportAction::Ban,
            Self::Mute { time } => ReportAction::Mute {
                time: parse_duration(&time)?,
            },
            Self::Delete => ReportAction::Delete,
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct JoinRequestConfigDto {
    pub method: ApprovalMethodDto,
}

impl TryInto<JoinRequestConfig> for JoinRequestConfigDto {
    type Error = Error;

    fn try_into(self) -> Result<JoinRequestConfig, Error> {
        Ok(JoinRequestConfig {
            method: self.method.try_into()?,
        })
    }
}

#[derive(SetField, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[serde(deny_unknown_fields)]
pub enum ApprovalMethodDto {
    Captcha(Option<CaptchaConfigDto>),
}

impl Default for ApprovalMethodDto {
    fn default() -> Self {
        Self::Captcha(None)
    }
}

impl TryInto<ApprovalMethod> for ApprovalMethodDto {
    type Error = Error;

    fn try_into(self) -> Result<ApprovalMethod, Error> {
        Ok(match self {
            Self::Captcha(config) => {
                ApprovalMethod::Captcha(config.map(TryInto::try_into).transpose()?)
            }
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AutoCommandDto {
    pub actions: Vec<ActionDto>,
    pub trigger: TriggerDto,
}

impl TryInto<AutoCommand> for AutoCommandDto {
    type Error = Error;

    fn try_into(self) -> Result<AutoCommand, Error> {
        let actions = self
            .actions
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<_, _>>()?;
        Ok(AutoCommand {
            actions,
            trigger: self.trigger.try_into()?,
        })
    }
}

#[derive(SetField, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[serde(deny_unknown_fields)]
pub enum ActionDto {
    Ban {
        reason: Option<String>,
    },
    Answer {
        text: Pattern<Username>,
    },
    Mute {
        time: String,
        reason: Option<String>,
    },
    DeleteMessage {
        reason: Option<String>,
    },
    Kick {
        reason: Option<String>,
    },
}

impl TryInto<Action> for ActionDto {
    type Error = Error;

    fn try_into(self) -> Result<Action, Error> {
        Ok(match self {
            Self::Ban { reason } => Action::Ban { reason },
            Self::Answer { text } => Action::Answer { text },
            Self::Mute { time, reason } => Action::Mute {
                time: parse_duration(&time)?,
                reason,
            },
            Self::DeleteMessage { reason } => Action::DeleteMessage { reason },
            Self::Kick { reason } => Action::Kick { reason },
        })
    }
}

#[derive(SetField, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case", tag = "type")]
#[serde(deny_unknown_fields)]
pub enum TriggerDto {
    Regex { regex: String },
    Audio,
    Document,
    Animation,
    Game,
    Sticker,
    Video,
    Voice,
    VideoNote,
    Contact,
    Location,
    Venue,
    Poll,
}

impl TryInto<Trigger> for TriggerDto {
    type Error = Error;

    fn try_into(self) -> Result<Trigger, Error> {
        Ok(match self {
            Self::Regex { regex } => Trigger::Regex(Regex::new(&regex)?),
            Self::Audio => Trigger::Audio,
            Self::Document => Trigger::Document,
            Self::Animation => Trigger::Animation,
            Self::Game => Trigger::Game,
            Self::Sticker => Trigger::Sticker,
            Self::Video => Trigger::Video,
            Self::Voice => Trigger::Voice,
            Self::VideoNote => Trigger::VideoNote,
            Self::Contact => Trigger::Contact,
            Self::Location => Trigger::Location,
            Self::Venue => Trigger::Venue,
            Self::Poll => Trigger::Poll,
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AntispamConfigDto {
    pub limit: u8,
    pub actions: Vec<ActionDto>,
}

impl TryInto<AntispamConfig> for AntispamConfigDto {
    type Error = Error;

    fn try_into(self) -> Result<AntispamConfig, Error> {
        Ok(AntispamConfig {
            limit: self.limit,
            actions: self
                .actions
                .into_iter()
                .map(TryInto::try_into)
                .collect::<Result<_, _>>()?,
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RulesConfigDto {
    rules: Option<String>,
    generate_from_autocommands: bool,
}

impl TryInto<RulesConfig> for RulesConfigDto {
    type Error = Error;

    fn try_into(self) -> Result<RulesConfig, Error> {
        Ok(RulesConfig {
            rules: self.rules,
            generate_from_autocommands: self.generate_from_autocommands,
        })
    }
}
