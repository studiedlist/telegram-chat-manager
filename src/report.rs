use crate::ban_user;
use crate::callback::CallbackData;
use crate::config::AccessLevel;
use crate::config::AdminPermission;
use crate::config::CommandError;
use crate::config::Config;
use crate::config::OptionalCommands;
use crate::config::ReportAction;
use crate::config::ReportConfig;
use crate::format_user;
use crate::mute_user;
use anyhow::anyhow;
use anyhow::Error;
use chrono::Duration;
use std::sync::Arc;
use teloxide::prelude::*;
use teloxide::types::ForwardedFrom;
use teloxide::types::InlineKeyboardButton;
use teloxide::types::InlineKeyboardMarkup;
use teloxide::types::Me;
use teloxide::types::MessageId;
use teloxide::types::ParseMode;
use teloxide::types::ReplyMarkup;
use teloxide::types::User;
use teloxide::utils::command::BotCommands;
use teloxide::utils::markdown::escape;
use uuid::Uuid;

pub const ACTION_BUTTONS_PER_ROW: usize = 2;

#[derive(BotCommands, Clone, Debug)]
#[command(rename_rule = "snake_case")]
pub enum ReportCommand {
    Report,
}

impl ReportCommand {
    pub fn is_report(self) -> bool {
        matches!(self, Self::Report)
    }
}

impl OptionalCommands for ReportCommand {
    fn parse(
        s: &str,
        me: &Me,
        _msg: &Message,
        config: Arc<Config>,
    ) -> Result<Option<Self>, CommandError> {
        if config.report.is_none() {
            return Ok(None);
        }
        let bot_username = me.username.clone().unwrap_or("".to_string());
        Ok(<Self as BotCommands>::parse(s, &bot_username).map(Some)?)
    }
}

pub async fn report_command(
    bot: Bot,
    msg: Message,
    report_config: Arc<ReportConfig>,
) -> Result<(), Error> {
    bot.delete_message(msg.chat.id, msg.id).await?;

    let sender = msg.from().ok_or(anyhow!("Report command with no sender"))?;

    let mut binding = Uuid::encode_buffer();
    let report_id = Uuid::new_v4().simple().encode_lower(&mut binding);

    let reported_user_id: Option<u64>;

    let reported_message_id = match msg.reply_to_message() {
        None => {
            // report by message id
            let text = msg.text().map(|t| t.split(' ').skip(1));
            let mut msg_text = text.ok_or(anyhow!("Report command without text"))?;
            MessageId(msg_text.next().and_then(|s| s.parse().ok()).ok_or(anyhow!(
                "You must reply to message or specify message id to perform this command"
            ))?)
        }
        Some(m) => m.id,
    };

    bot.delete_message(
        msg.chat.id,
        bot.send_message(msg.chat.id, format!("`{}`", report_id)) // TODO: improve message
            .parse_mode(ParseMode::MarkdownV2)
            .await?
            .id,
    )
    .await?;

    let r_msg = bot
        .forward_message(msg.chat.id, msg.chat.id, reported_message_id)
        .await?;
    bot.delete_message(r_msg.chat.id, r_msg.id).await?;

    let reported_user_name = match r_msg
        .forward_from()
        .ok_or(anyhow!("Unknown message author"))?
    {
        ForwardedFrom::User(u) => {
            reported_user_id = Some(u.id.0);
            format_user(u)
        }
        ForwardedFrom::Chat(_) => {
            // TODO: handle messages from chats/channels
            bot.delete_message(r_msg.chat.id, r_msg.id).await?;
            return Err(anyhow!("Reporting chat/channel messages not supported yet"));
        }
        ForwardedFrom::SenderName(n) => {
            reported_user_id = None;
            n.to_string()
        }
    };

    let reported_message_url = format!(
        "https://t.me/c/{}/{}",
        msg.chat.id.to_string().replace("-100", ""),
        reported_message_id
    );

    let reported_user_id = match reported_user_id {
        None => "not available".to_string(),
        Some(x) => x.to_string(),
    };
    let report_text = format!(
        "report id: `{}`\nreported message: {}\nreport author:\n\tid: `{}`\n\tname: `{}`\nreported user:\n\tid: `{}`\n\tname: `{}`\n"
            ,escape(report_id) // report id
            ,escape(&reported_message_url) // reported message
            ,escape(&sender.id.0.to_string()) // report author id
            ,escape(&format_user(sender)) // report author name
            ,escape(&reported_user_id) // reported author id
            ,escape(&reported_user_name) // reported author name
        );
    bot.delete_message(
        msg.chat.id,
        bot.send_message(msg.chat.id, report_text)
            .parse_mode(ParseMode::MarkdownV2)
            .await?
            .id,
    )
    .await?;

    let button = InlineKeyboardButton::callback;
    let mut buttons = vec![];
    let actions = &report_config.actions;
    for a in actions {
        let button = match a {
            ReportAction::Ban => button(
                "Ban user",
                format!("{BAN_COMMAND} {reported_user_id} {report_id}"),
            ),
            ReportAction::Mute { .. } => button(
                "Mute user",
                format!("{MUTE_COMMAND} {reported_user_id} {report_id}"),
            ),
            ReportAction::Delete => button(
                "Delete message",
                format!("{DELETE_COMMAND} {reported_message_id} {report_id}"),
            ),
        };
        buttons.push(button);
    }

    let notify_msg_text = report_config.report_notify.apply(report_id.to_string());
    let mut send_msg = bot
        .send_message(msg.chat.id, notify_msg_text)
        .parse_mode(ParseMode::MarkdownV2);
    if !buttons.is_empty() {
        let buttons = buttons.into_iter().fold(vec![vec![]], |mut r, e| {
            match r.last_mut() {
                Some(l) if l.len() < ACTION_BUTTONS_PER_ROW => l.push(e),
                _ => r.push(vec![e]),
            };
            r
        });
        send_msg = send_msg.reply_markup(ReplyMarkup::InlineKeyboard(InlineKeyboardMarkup::new(
            buttons,
        )));
    }
    send_msg.await?;
    Ok(())
}

const BAN_COMMAND: &str = "ban";
const MUTE_COMMAND: &str = "mute";
const DELETE_COMMAND: &str = "del";

pub async fn report_callback(
    data: CallbackData,
    msg: Message,
    from: User,
    bot: Bot,
    config: Arc<Config>,
) -> Result<(), Error> {
    let cmd = data.command();
    let args = data.args();
    let arg = args
        .first()
        .ok_or(anyhow!("Callback data must contain argument"))?;
    let report_id = args.get(1);
    let member = bot.get_chat_member(msg.chat.id, from.id).await?;
    let access_level = AccessLevel::from(member);
    let text = match cmd.as_str() {
        DELETE_COMMAND => {
            if !access_level.has_permission(AdminPermission::CanDeleteMessages) {
                return Err(anyhow!("You cannot delete messages"));
            }
            bot.delete_message(msg.chat.id, MessageId(arg.parse()?))
                .await?;
            let mut text = "Reported message deleted".to_string();
            if let Some(id) = report_id {
                text.push_str(&format!(" (report id is `{id}`)"));
            }
            bot.delete_message(msg.chat.id, msg.id).await?;
            Some(text)
        }
        MUTE_COMMAND => {
            if !access_level.has_permission(AdminPermission::CanRestrictMembers) {
                return Err(anyhow!("You cannot mute members"));
            }
            let user_id = UserId(arg.parse()?);
            let user = bot.get_chat_member(msg.chat.id, user_id).await?.user;
            let mut reason = "Muted due to report".to_string();
            if let Some(id) = report_id {
                reason.push_str(&format!(" `{id}`"));
            }
            mute_user(
                &Duration::minutes(30),
                &bot,
                msg.chat.id,
                &user,
                Some(reason),
                config,
            )
            .await?
        }
        BAN_COMMAND => {
            if !access_level.has_permission(AdminPermission::CanRestrictMembers) {
                return Err(anyhow!("You cannot ban members"));
            }
            let user_id = UserId(arg.parse()?);
            let user = bot.get_chat_member(msg.chat.id, user_id).await?.user;
            let mut reason = "Banned due to report".to_string();
            if let Some(id) = report_id {
                reason.push_str(&format!(" `{id}`"));
            }
            ban_user(&bot, &msg.chat, &user, Some(reason), config).await?
        }
        _ => return Err(anyhow!("Invalid callback command")),
    };

    if let Some(text) = text {
        bot.send_message(msg.chat.id, text)
            .parse_mode(ParseMode::MarkdownV2)
            .await?;
    }
    Ok(())
}
