pub mod repository;

use crate::config::arguments;
use crate::config::CaptchaConfig;
use crate::config::GenOptions;
use crate::format_user;
use crate::Admin;
use crate::EventTx;
use anyhow::anyhow;
use anyhow::Error;
use captcha::Captcha;
use futures::future::join_all;
use repository::CaptchaRepository;
use std::env;
use std::fs;
use std::future::IntoFuture;
use std::path::PathBuf;
use std::sync::Arc;
use teloxide::prelude::*;
use teloxide::types::ChatId;
use teloxide::types::InputFile;
use teloxide::types::MessageId;
use teloxide::types::ParseMode;
use teloxide::types::User;
use teloxide::utils::command::BotCommands;
use teloxide::utils::markdown::escape;
use teloxide::ApiError;
use teloxide::RequestError;
use typesafe_repository::macros::Id;
use typesafe_repository::GetIdentity;
use typesafe_repository::Identity;
use typesafe_repository::IdentityBy;
use typesafe_repository::RefIdentity;
use uuid::Uuid;

#[derive(Clone, Debug)]
pub enum CaptchaEvent {
    Completed(UserCaptcha),
    Timeout(UserCaptcha),
}

#[derive(BotCommands, Clone, Debug)]
#[command(rename_rule = "lowercase")]
pub enum CaptchaCommand {
    Regenerate,
    Cancel,
}

impl CaptchaCommand {
    pub fn is_regenerate(self) -> bool {
        matches!(self, Self::Regenerate)
    }
    pub fn is_cancel(self) -> bool {
        matches!(self, Self::Cancel)
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct CaptchaId(pub ChatId, pub UserId);

#[derive(Id, Clone, Debug)]
#[Id(ref_id, get_id)]
pub struct UserCaptcha {
    pub captcha_message: MessageId,
    pub answer: String,
    pub message_ids: Vec<MessageId>,
    pub retries: usize,
    pub regenerated: usize,
    pub id: CaptchaId,
}

impl UserCaptcha {
    pub fn new(
        answer: String,
        chat: ChatId,
        user: UserId,
        captcha_message: MessageId,
        message_ids: Vec<MessageId>,
        retries: usize,
        regenerated: usize,
    ) -> Self {
        Self {
            answer,
            message_ids,
            retries,
            regenerated,
            captcha_message,
            id: CaptchaId(chat, user),
        }
    }
    pub fn user_id(&self) -> &UserId {
        &self.id.1
    }
    pub fn chat_id(&self) -> &ChatId {
        &self.id.0
    }
}

impl IdentityBy<MessageId> for UserCaptcha {
    fn id_by(&self) -> MessageId {
        self.captcha_message
    }
}

pub async fn send_captcha(
    id: ChatId,
    user: &User,
    bot: &Bot,
    captcha_config: &CaptchaConfig,
    caption: &str,
) -> Result<UserCaptcha, Error> {
    let (path, answer) = gen_captcha(&captcha_config.gen_options)?;
    let captcha = InputFile::file(&path);
    let msg = bot
        .send_photo(id, captcha)
        .caption(caption)
        .parse_mode(ParseMode::MarkdownV2)
        .await?;
    if let Err(err) = fs::remove_file(path) {
        log::error!("Unable to remove temporary captcha file\n{err:?}");
    }
    Ok(UserCaptcha::new(
        answer.to_lowercase(),
        id,
        user.id,
        msg.id,
        vec![msg.id],
        0,
        0,
    ))
}

pub async fn check_captcha(
    msg: Message,
    repo: Arc<dyn CaptchaRepository>,
    bot: Bot,
    mut captcha: UserCaptcha,
    tx: Arc<EventTx<CaptchaEvent>>,
    captcha_config: Arc<CaptchaConfig>,
) -> Result<(), Error> {
    match msg.from().zip(msg.text()) {
        Some((_, text)) if text.trim().to_lowercase() == captcha.answer => {
            repo.remove(captcha.id_ref()).await?;
            let futures = captcha
                .message_ids
                .iter()
                .map(|id| bot.delete_message(msg.chat.id, *id).into_future());
            join_all(futures).await;
            bot.delete_message(msg.chat.id, msg.id).await?;
            tx.send(CaptchaEvent::Completed(captcha))?;
        }
        Some((user, _)) => {
            captcha.message_ids.push(msg.id);
            if captcha.retries >= captcha_config.retry_limit {
                return failure(bot, msg.chat.id, user, repo, captcha, captcha_config).await;
            }
            captcha.retries += 1;
            let p = &captcha_config.retry_message;
            let args = arguments::RetryMessage::new(captcha.retries, captcha_config.retry_limit);
            let warning = bot
                .send_message(msg.chat.id, escape(&p.apply(args)))
                .reply_to_message_id(msg.id)
                .await?;
            captcha.message_ids.push(warning.id);
            repo.update(captcha).await?;
        }
        _ => {
            captcha.retries += 1;
            repo.update(captcha).await?;
            bot.delete_message(msg.chat.id, msg.id).await?;
        }
    }
    Ok(())
}

pub async fn success(
    captcha_config: Arc<CaptchaConfig>,
    bot: Bot,
    msg: Message,
) -> Result<(), Error> {
    let user = msg
        .from()
        .ok_or(anyhow!("Captcha solved but message have no sender"))?;
    if let Some(p) = captcha_config.success_message.as_ref() {
        let args = arguments::Username::new(format_user(user));
        bot.send_message(msg.chat.id, p.apply(args)).await?;
    }
    Ok(())
}

pub async fn failure(
    bot: Bot,
    chat: ChatId,
    user: &User,
    repo: Arc<dyn CaptchaRepository>,
    captcha: UserCaptcha,
    captcha_config: Arc<CaptchaConfig>,
) -> Result<(), Error> {
    match bot.get_chat_member(chat, user.id).await {
        Ok(_) => (),
        Err(RequestError::Api(ApiError::UserNotFound)) => {
            let futures = captcha
                .message_ids
                .iter()
                .map(|id| bot.delete_message(chat, *id).into_future());
            join_all(futures).await;
            return Ok(());
        }
        Err(err) => {
            log::warn!("Unable to get chat member\n{err}");
        }
    }
    #[cfg(not(debug_assertions))]
    {
        let req = bot.ban_chat_member(chat, user.id);
        if let Some(duration) = captcha_config.ban_time.as_ref() {
            let date = chrono::Utc::now() + *duration;
            req.until_date(date).await?;
        }
    }
    #[cfg(debug_assertions)]
    bot.unban_chat_member(chat, user.id).await?;
    let pattern = &captcha_config.failure_message;

    if let Some(p) = pattern {
        let args = arguments::Username::new(format_user(user));
        bot.send_message(chat, escape(&p.apply(args))).await?;
    }
    let futures = captcha
        .message_ids
        .iter()
        .map(|id| bot.delete_message(chat, *id).into_future());
    join_all(futures).await;
    repo.remove(captcha.id_ref()).await?;
    Ok(())
}

pub async fn captcha_command(
    cmd: CaptchaCommand,
    mut captcha: UserCaptcha,
    repo: Arc<dyn CaptchaRepository>,
    bot: Bot,
    msg: Message,
    captcha_config: Arc<CaptchaConfig>,
) -> Result<(), Error> {
    if let Some(user) = msg.from() {
        match cmd {
            CaptchaCommand::Regenerate if captcha.regenerated < captcha_config.regenerate_limit => {
                regenerate_captcha(
                    &captcha,
                    repo.clone(),
                    bot,
                    msg.chat.id,
                    user.clone(),
                    captcha_config,
                )
                .await?;
                captcha.message_ids.push(msg.id);
                repo.update(captcha).await?;
            }
            CaptchaCommand::Regenerate => {
                captcha.message_ids.push(msg.id);
                let s = &captcha_config.regenerate_limit_reached;
                let warning = bot
                    .send_message(msg.chat.id, escape(&s))
                    .reply_to_message_id(msg.id)
                    .await;
                match warning {
                    Ok(warning) => captcha.message_ids.push(warning.id),
                    Err(err) => return Err(err.into()),
                }
                repo.update(captcha).await?;
            }
            _ => unreachable!(),
        }
    }
    Ok(())
}

pub async fn regenerate_captcha(
    captcha: &UserCaptcha,
    repo: Arc<dyn CaptchaRepository>,
    bot: Bot,
    id: ChatId,
    user: User,
    captcha_config: Arc<CaptchaConfig>,
) -> Result<(), Error> {
    let mut captcha = captcha.clone();
    let (path, answer) = gen_captcha(&captcha_config.gen_options)?;
    captcha.answer = answer;
    captcha.regenerated += 1;
    let c = InputFile::file(&path);
    let args = arguments::RegenerateMessage::new(
        captcha.regenerated,
        captcha_config.regenerate_limit,
        format_user(&user),
    );
    let caption = captcha_config.regenerate_message.apply(args);
    let msg = bot.send_photo(id, c).caption(&escape(&caption)).await?;
    captcha.message_ids.push(msg.id);
    repo.update(captcha).await?;
    Ok(())
}

pub async fn cancel(
    bot: Bot,
    msg: Message,
    repo: Arc<dyn CaptchaRepository>,
    admin: Arc<Admin>,
) -> Result<(), Error> {
    let reply = msg
        .reply_to_message()
        .ok_or(anyhow!("You have to reply to captcha message"))?;
    let captcha = repo
        .get_by(&reply.id)
        .await?
        .ok_or(anyhow!("Replied message doesn't contain captcha"))?;
    repo.remove(&captcha.id).await?;
    let futures = captcha
        .message_ids
        .iter()
        .map(|id| bot.delete_message(msg.chat.id, *id).into_future());
    join_all(futures).await;
    bot.delete_message(msg.chat.id, msg.id).await?;

    let text = match bot.get_chat_member(msg.chat.id, captcha.id.1).await {
        Ok(user) => format!(
            "Captcha for {} was cancelled by {}",
            format_user(&user.user),
            format_user(admin.inner().0)
        ),
        Err(RequestError::Api(ApiError::UserNotFound)) => format!(
            "Captcha for {} (user left chat) was cancelled by {}",
            captcha.id.1,
            format_user(admin.inner().0)
        ),
        Err(err) => return Err(err.into()),
    };

    bot.send_message(msg.chat.id, text).await?;

    Ok(())
}

pub async fn has_captcha(msg: Message, repo: Arc<dyn CaptchaRepository>) -> Option<UserCaptcha> {
    if let Some(user) = msg.from() {
        let res = repo.get_one(&CaptchaId(msg.chat.id, user.id)).await;
        match res {
            Ok(c) => c,
            Err(err) => {
                log::error!(
                    "Unable to get captcha for user {}\n{err:?}",
                    format_user(user)
                );
                None
            }
        }
    } else {
        None
    }
}

fn gen_captcha(options: &GenOptions) -> Result<(PathBuf, String), Error> {
    let dir = env::temp_dir();
    let mut binding = Uuid::encode_buffer();
    let id = Uuid::new_v4().simple().encode_lower(&mut binding);
    let captcha_path = dir.join(format!("{}.{}", id, "png"));
    let mut binding = Captcha::new();
    let mut captcha = binding.add_chars(options.chars);
    for f in &options.filters {
        captcha = f.apply(captcha);
    }
    let (w, h) = options.view.unwrap_or((220, 120));
    captcha = captcha.view(w, h);
    let msg = captcha.chars();
    captcha.save(captcha_path.as_path())?;
    Ok((captcha_path, msg.iter().collect::<String>()))
}
