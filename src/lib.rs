#![deny(
    future_incompatible,
    let_underscore,
    keyword_idents,
    elided_lifetimes_in_paths,
    meta_variable_misuse,
    noop_method_call,
    pointer_structural_match,
    unused_lifetimes,
    unused_qualifications,
    unsafe_op_in_unsafe_fn,
    clippy::undocumented_unsafe_blocks,
    clippy::debug_assert_with_mut_call,
    clippy::empty_line_after_outer_attr,
    clippy::panic,
    clippy::unwrap_used,
    clippy::expect_used,
    clippy::redundant_field_names,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::unneeded_field_pattern,
    clippy::useless_let_if_seq,
    clippy::default_union_representation,
    clippy::checked_conversions,
    clippy::dbg_macro
)]
#![warn(clippy::cloned_instead_of_copied, clippy::cognitive_complexity)]
#![allow(
    clippy::must_use_candidate,
    clippy::doc_markdown,
    clippy::missing_errors_doc,
    clippy::implicit_hasher
)]

pub mod admin;
pub mod antispam;
pub mod autocommands;
pub mod callback;
pub mod captcha;
pub mod commands;
pub mod config;
pub mod handlers;
pub mod join_request;
pub mod private_chat;
pub mod raid;
pub mod report;

use crate::callback::CallbackData;
use crate::config::CommandError;
use crate::config::OptionalCommands;
use cached::proc_macro::cached;
use cached::TimedSizedCache;
use chrono::DateTime;
use chrono::Duration;
use chrono::Utc;
use config::Config;
use config::KickBehavior;
use lazy_regex::regex;
use std::borrow::Borrow;
use std::sync::Arc;
use teloxide::net::Download;
use teloxide::prelude::*;
use teloxide::types::Chat;
use teloxide::types::ChatId;
use teloxide::types::ChatJoinRequest;
use teloxide::types::ChatMember;
use teloxide::types::ChatPermissions;
use teloxide::types::FileMeta;
use teloxide::types::Me;
use teloxide::types::Message;
use teloxide::types::MessageId;
use teloxide::types::Update;
use teloxide::types::User;
use teloxide::types::UserId;
use teloxide::utils::command::BotCommands;
use teloxide::utils::command::ParseError;
use teloxide::ApiError;
use teloxide::Bot;
use teloxide::RequestError;
use teloxide_extras::extractors::extract_chat;
use teloxide_extras::extractors::extract_date;
use teloxide_extras::filters::is_admin;
use tokio::sync::broadcast;
use tokio::sync::mpsc;

pub const CRATE_VERSION: &str = env!("CARGO_PKG_VERSION");

pub type EventTx<T> = broadcast::Sender<T>;
pub type EventRx<T> = broadcast::Receiver<T>;

pub fn parse_duration(duration: &str) -> Result<Duration, anyhow::Error> {
    let minutes = |x| 60 * x;
    let hours = |x| minutes(60) * x;
    let days = |x| hours(24) * x;
    let weeks = |x| days(7) * x;
    let years = |x| days(365) * x;

    if !duration.is_ascii() {
        return Err(anyhow::anyhow!("Not ASCII"));
    };
    let re = regex!(r"[smhdwy]");
    let duration = duration.to_lowercase();
    if re.is_match(&duration) {
        let mut res_dur = 0;
        let mut buf = String::from("");
        for char in duration.chars() {
            match (buf.parse(), char) {
                (parsed, 'y') => res_dur += years(parsed?),
                (parsed, 'w') => res_dur += weeks(parsed?),
                (parsed, 'd') => res_dur += days(parsed?),
                (parsed, 'h') => res_dur += hours(parsed?),
                (parsed, 'm') => res_dur += minutes(parsed?),
                (parsed, 's') => res_dur += parsed?,
                _ => {
                    char.to_digit(10)
                        .ok_or(anyhow::anyhow!("Wrong time format"))?;
                    buf.push(char);
                }
            }
        }
        Ok(Duration::seconds(res_dur))
    } else {
        Ok(Duration::seconds(duration.parse()?))
    }
}

pub struct NotAdmin(User, ChatId);

impl NotAdmin {
    pub async fn try_from<B: Borrow<Bot>>(
        user: User,
        chat_id: ChatId,
        bot: &B,
    ) -> Result<Option<Self>, anyhow::Error> {
        if is_admin(chat_id, user.id, bot.borrow()).await? {
            Ok(None)
        } else {
            Ok(Some(Self(user, chat_id)))
        }
    }
    pub fn inner(&self) -> (&User, ChatId) {
        (&self.0, self.1)
    }
}

pub struct Admin(User, ChatId);

impl Admin {
    pub async fn try_from<B: Borrow<Bot>>(
        user: User,
        chat_id: ChatId,
        bot: B,
    ) -> Result<Option<Self>, anyhow::Error> {
        if is_admin(chat_id, user.id, bot.borrow()).await? {
            Ok(Some(Self(user, chat_id)))
        } else {
            Ok(None)
        }
    }
    pub fn inner(&self) -> (&User, ChatId) {
        (&self.0, self.1)
    }
}

pub fn format_user(user: &User) -> String {
    user.username.clone().unwrap_or_else(|| {
        user.last_name
            .clone()
            .map(|l| format!("{} {l}", user.first_name))
            .unwrap_or_else(|| user.first_name.clone())
    })
}

pub fn not_outdated(upd: Update, config: Arc<Config>) -> bool {
    let date = match extract_date(&upd) {
        Some(d) => d,
        None => return true,
    };
    Utc::now().signed_duration_since(date) < config.timeout.default_timeout
}

pub fn private_chat(upd: Update) -> bool {
    extract_chat(&upd).is_some_and(|x| x.is_private())
}

pub fn group_chat(upd: Update) -> bool {
    extract_chat(&upd).is_some_and(|x| x.is_group() || x.is_supergroup())
}

pub fn extract_chat_id(upd: Update) -> Option<ChatId> {
    extract_chat(&upd).map(|x| x.id)
}

pub fn document(msg: Message) -> bool {
    msg.document().is_some()
}

pub fn extract_sender(msg: &Message) -> Result<&User, anyhow::Error> {
    msg.from()
        .ok_or(anyhow::anyhow!("Message with no sender\n{msg:?}"))
}

pub fn extract_sender_id(msg: &Message) -> Result<UserId, anyhow::Error> {
    extract_sender(msg).map(|u| u.id)
}

pub async fn download_file(bot: &Bot, file: &FileMeta) -> Result<String, anyhow::Error> {
    let file = bot.get_file(&file.id).await?;
    let mut res = Vec::new();
    bot.download_file(&file.path, &mut res).await?;
    let res = String::from_utf8(res)?;
    Ok(res)
}

#[must_use = "You should send returned text to chat"]
pub async fn kick_user(
    bot: &Bot,
    chat: &Chat,
    user: &User,
    reason: Option<String>,
    config: Arc<Config>,
) -> Result<Option<String>, anyhow::Error> {
    match config.admin.kick_behavior {
        KickBehavior::Telegram => {
            bot.kick_chat_member(chat.id, user.id).await?;
        }
        KickBehavior::KickAndUnban => {
            if chat.is_supergroup() {
                bot.unban_chat_member(chat.id, user.id).await?;
            } else {
                bot.kick_chat_member(chat.id, user.id).await?;
            }
        }
    }
    let text = config.admin.kicked.as_ref().map(|p| p.apply(user));
    Ok(config
        .admin
        .reason
        .as_ref()
        .zip(reason)
        .map(|(p, r)| p.apply(r))
        .zip(text)
        .map(|(reason, text)| format!("{text}\n{reason}")))
}

pub async fn unban_user(bot: &Bot, chat_id: ChatId, user_id: UserId) -> Result<(), anyhow::Error> {
    bot.unban_chat_member(chat_id, user_id)
        .only_if_banned(true)
        .await?;
    Ok(())
}

#[must_use = "You should send returned text to chat"]
pub async fn ban_user(
    bot: &Bot,
    chat: &Chat,
    user: &User,
    reason: Option<String>,
    config: Arc<Config>,
) -> Result<Option<String>, anyhow::Error> {
    #[cfg(not(debug_assertions))]
    bot.ban_chat_member(chat.id, user.id).await?;
    #[cfg(debug_assertions)]
    {
        kick_user(bot, chat, user, reason.clone(), config.clone()).await?;
    }
    let text = config.admin.banned.as_ref().map(|p| p.apply(user));
    Ok(config
        .admin
        .reason
        .as_ref()
        .zip(reason)
        .map(|(p, r)| p.apply(r))
        .zip(text)
        .map(|(reason, text)| format!("{text}\n{reason}")))
}

pub async fn unmute_user(bot: &Bot, chat: ChatId, user: &User) -> Result<(), anyhow::Error> {
    let chat = bot.get_chat(chat).await?;
    if !chat.is_supergroup() {
        return Err(anyhow::anyhow!(
            "Mute is available for supergroups only (tg api restriction)".to_string(),
        ));
    }
    let mut prem = match chat.permissions() {
        Some(perm) => perm,
        None => {
            log::error!("Chat contains no permissions\n{chat:?}");
            return Err(anyhow::anyhow!(
                "Bug: chat contains no permissions".to_string()
            ));
        }
    };
    prem.set(ChatPermissions::SEND_MESSAGES, true);
    let res = bot.restrict_chat_member(chat.id, user.id, prem).await;
    if let Err(RequestError::Api(ApiError::NotEnoughRightsToRestrict)) = res {
        return Err(anyhow::anyhow!("I cannot unmute this user"));
    }
    Ok(())
}

#[must_use = "You should send returned text to chat"]
pub async fn mute_user(
    duration: &Duration,
    bot: &Bot,
    chat: ChatId,
    user: &User,
    reason: Option<String>,
    config: Arc<Config>,
) -> Result<Option<String>, anyhow::Error> {
    if duration.num_seconds() < 30 && duration.num_seconds() != 0 {
        return Ok(Some(
            "Time to mute can be 0 (forever) or greater than 30s. 1 year and more = forever"
                .to_string(),
        ));
    };
    let chat = bot.get_chat(chat).await?;
    if !chat.is_supergroup() {
        return Ok(Some(
            "Mute is available for supergroups only (tg api restriction)".to_string(),
        ));
    }
    let mut prem = match chat.permissions() {
        Some(perm) => perm,
        None => {
            log::error!("Chat contains no permissions\n{chat:?}");
            return Ok(Some("Bug: chat contains no permissions".to_string()));
        }
    };
    prem.set(ChatPermissions::SEND_MESSAGES, false);
    let now: DateTime<Utc> = Utc::now();
    let until_date = now + *duration;
    let res = bot
        .restrict_chat_member(chat.id, user.id, prem)
        .until_date(until_date)
        .await;
    if let Err(RequestError::Api(ApiError::NotEnoughRightsToRestrict)) = res {
        return Err(anyhow::anyhow!("I cannot mute this user"));
    }
    let text = config.admin.muted.as_ref().map(|p| p.apply(user));
    Ok(config
        .admin
        .reason
        .as_ref()
        .zip(reason)
        .map(|(p, r)| p.apply(r))
        .zip(text)
        .map(|(reason, text)| format!("{text}\n{reason}")))
}

pub async fn filter_command<C: BotCommands>(msg: Message, me: Me, bot: Bot) -> Option<C> {
    async fn send_message(bot: &Bot, msg: &Message, text: String) {
        let res = bot
            .send_message(msg.chat.id, text)
            .reply_to_message_id(msg.id)
            .await;
        if let Err(err) = res {
            log::error!("Unable to send parsing error: {err:?}");
        }
    }
    let send_message = |text| send_message(&bot, &msg, text);
    match C::parse(msg.text()?, me.username.as_ref()?) {
        Ok(cmd) => Some(cmd),
        Err(err) => {
            match err {
                ParseError::IncorrectFormat(err) => send_message(err.to_string()).await,
                ParseError::TooFewArguments { message, .. } => send_message(message).await,
                ParseError::TooManyArguments { message, .. } => send_message(message).await,
                err => log::debug!(
                    "Unable to parse command {}: {err}",
                    std::any::type_name::<C>()
                ),
            }
            None
        }
    }
}

pub async fn parse_callback_data(
    q: CallbackQuery,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, anyhow::Error)>>,
) -> Option<CallbackData> {
    ok_or_tx(
        err_tx,
        q.message.map(|m| m.chat.id),
        q.data.map(TryInto::try_into).transpose(),
    )
    .await
    .flatten()
}

pub async fn filter_optional_command<C: OptionalCommands>(
    msg: Message,
    me: Me,
    bot: Bot,
    config: Arc<Config>,
) -> Option<C> {
    async fn send_message(bot: &Bot, msg_id: MessageId, chat_id: ChatId, text: &str) {
        let res = bot
            .send_message(chat_id, text)
            .reply_to_message_id(msg_id)
            .await;
        if let Err(err) = res {
            log::error!("Unable to send parsing error: {err:?}");
        }
    }
    let send_message = |text| send_message(&bot, msg.id, msg.chat.id, text);
    match C::parse(msg.text()?, &me, &msg, config) {
        Ok(cmd) => cmd,
        Err(err) => {
            match err {
                CommandError::ParseError(ParseError::IncorrectFormat(err)) => {
                    send_message(&err.to_string()).await
                }
                CommandError::ParseError(ParseError::TooFewArguments { message, .. }) => {
                    send_message(&message).await
                }
                CommandError::ParseError(ParseError::TooManyArguments { message, .. }) => {
                    send_message(&message).await
                }
                CommandError::ParseError(err) => {
                    log::debug!(
                        "Error parsing command {}\n{err}",
                        std::any::type_name::<C>()
                    );
                }
                CommandError::BotCannotBeTarget => {
                    send_message("I cannot be target of this command").await
                }
            }
            None
        }
    }
}

pub async fn get_chat_member(
    bot: &Bot,
    chat: ChatId,
    user: UserId,
) -> Result<ChatMember, anyhow::Error> {
    match bot.get_chat_member(chat, user).await {
        Ok(member) if !member.kind.is_present() => {
            Err(anyhow::anyhow!("User is not member of chat"))
        }
        Ok(member) => Ok(member),
        Err(err) => match err {
            RequestError::Api(ApiError::UserNotFound) => Err(err.into()),
            err => Err(err.into()),
        },
    }
}

pub async fn get_user(bot: &Bot, chat: ChatId, user: UserId) -> Result<User, anyhow::Error> {
    get_chat_member(bot, chat, user).await.map(|c| c.user)
}

#[cached(
    result = true,
    type = "TimedSizedCache<ChatId, u32>",
    create = "{ TimedSizedCache::with_size_and_lifespan_and_refresh(20, 60, true) }",
    convert = r#"{ chat }"#
)]
pub async fn get_members_count(bot: &Bot, chat: ChatId) -> Result<u32, anyhow::Error> {
    Ok(bot.get_chat_member_count(chat).await?)
}

pub fn percent<A: Into<f64>, B: Into<f64>>(a: A, b: B) -> usize {
    let (a, b) = (a.into(), b.into());

    (b * 100.0 / a) as usize
}

pub async fn parse_admin(bot: Bot, msg: Message) -> Option<Arc<Admin>> {
    match Admin::try_from(msg.from()?.clone(), msg.chat.id, &bot).await {
        Ok(admin) => admin.map(Arc::new),
        Err(err) => {
            log::error!("Cannot get admins of chat {}\n{err}", msg.chat.id);
            bot.send_message(msg.chat.id, "Error: cannot get admins of chat")
                .reply_to_message_id(msg.id)
                .await
                .ok()?;
            None
        }
    }
}

pub fn ok_or_err<T>(r: Result<T, T>) -> T {
    match r {
        Ok(t) => t,
        Err(t) => t,
    }
}

pub fn chat_id_from_callback(q: CallbackQuery) -> Option<ChatId> {
    q.message.map(|m| m.chat.id)
}

pub fn chat_id_from_request(r: ChatJoinRequest) -> ChatId {
    r.chat.id
}

pub fn user_from_callback(q: CallbackQuery) -> User {
    q.from
}

pub async fn message_from_callback(
    q: CallbackQuery,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, anyhow::Error)>>,
) -> Option<Message> {
    ok_or_tx(
        err_tx,
        None, // No need to pass message since there will be none on error
        q.message.ok_or(anyhow::anyhow!("Callback with no message")),
    )
    .await
}

pub async fn parse_access_level(
    msg: &Message,
    bot: &Bot,
) -> Result<config::AccessLevel, anyhow::Error> {
    msg.from()
        .map(|from| bot.get_chat_member(msg.chat.id, from.id))
        .ok_or(anyhow::anyhow!("Message with no sender"))?
        .await
        .map(Into::into)
        .map_err(Into::into)
}

use dptree::di::DependencySupplier;
use dptree::di::Injectable;
use dptree::from_fn_with_description;
use dptree::HandlerDescription;

pub fn pass<'a, F, Input, FnArgs, Descr>(
    f: F,
) -> Endpoint<'a, Input, Result<(), teloxide_extras::error::Error>, Descr>
where
    F: Injectable<Input, Result<(), anyhow::Error>, FnArgs> + Send + Sync + 'a,
    Input: DependencySupplier<Update> + Clone + Send + 'a + std::fmt::Debug,
    Descr: HandlerDescription,
{
    let f = Arc::new(f);

    from_fn_with_description(Descr::map_async(), move |x, cont| {
        let f = Arc::clone(&f);
        async move {
            let f = f.inject(&x);
            let res = f().await;
            drop(f);
            let chat = extract_chat(<Input as DependencySupplier<Update>>::get(&x)).map(|x| x.id);

            let res =
                res.map_err(|err: anyhow::Error| teloxide_extras::error::Error::from((chat, err)));
            if res.is_err() {
                return ControlFlow::Break(res);
            }
            let intermediate = x.clone();
            match cont(intermediate).await {
                ControlFlow::Continue(x) => ControlFlow::Continue(x),
                ControlFlow::Break(res) => ControlFlow::Break(res),
            }
        }
    })
}

#[must_use]
pub async fn ok_or_tx<T, E: Into<anyhow::Error>>(
    tx: Arc<mpsc::Sender<(Option<ChatId>, anyhow::Error)>>,
    chat_id: Option<ChatId>,
    res: Result<T, E>,
) -> Option<T> {
    match res.map_err(Into::into) {
        Ok(t) => Some(t),
        Err(err) => {
            if let Err(err) = tx.send((chat_id, err)).await {
                log::error!("Unable to send error {err:?}");
            }
            None
        }
    }
}
