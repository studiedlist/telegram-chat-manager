use anyhow::anyhow;
use anyhow::Error;
use chat_manager::admin;
use chat_manager::antispam;
use chat_manager::autocommands;
use chat_manager::callback::CallbackData;
use chat_manager::captcha;
use chat_manager::captcha::repository::CaptchaRepository;
use chat_manager::captcha::CaptchaEvent;
use chat_manager::chat_id_from_callback;
use chat_manager::commands;
use chat_manager::config;
use chat_manager::config::chat::ChatConfigRepository;
use chat_manager::config::chat::FilesystemChatConfigRepository;
use chat_manager::config::Config;
use chat_manager::extract_chat_id;
use chat_manager::filter_command;
use chat_manager::filter_optional_command;
use chat_manager::handlers;
use chat_manager::handlers::ChatMemberEvent;
use chat_manager::join_request;
use chat_manager::join_request::RequestEvent;
use chat_manager::pass;
use chat_manager::private_chat;
use chat_manager::private_chat::repository::ChatAuthorityRepository;
use chat_manager::private_chat::PrivateChatCallbackData;
use chat_manager::raid::RaidRepository;
use chat_manager::report;
use chat_manager::report::ReportCommand;
use chat_manager::user_from_callback;
use chat_manager::Admin;
use dptree::filter;
use dptree::filter_map;
use dptree::filter_map_async;
use std::sync::Arc;
use teloxide::dispatching::DpHandlerDescription;
use teloxide::prelude::*;
use teloxide::types::Message;
use teloxide_extras::error;
use teloxide_extras::error::handler;
use teloxide_extras::error::ReportingErrorHandler;
use tokio::sync::broadcast;
use typesafe_repository::inmemory::InMemoryRepository;

#[tokio::main]
async fn main() -> Result<(), Error> {
    pretty_env_logger::init();
    let token = match dotenvy::var("TELOXIDE_TOKEN") {
        Err(..) => {
            log::error!("Bot token not set (TELOXIDE_TOKEN env var)\n");
            return Err(anyhow!("Bot token not set"));
        }
        Ok(v) => v,
    };
    let bot = Bot::new(token);
    config::init_config();
    let id = bot
        .get_me()
        .send()
        .await
        .expect("Error getting bot id")
        .user
        .id;
    let handler = dptree::entry()
        .branch(
            Update::filter_callback_query()
                .filter_map(chat_id_from_callback)
                .map(user_from_callback)
                .map_async(Admin::try_from::<Bot>)
                .map(config::merge_config_from_chat_id)
                .filter_map_async(chat_manager::parse_callback_data)
                .branch(
                    filter_map_async(PrivateChatCallbackData::try_parse)
                        .branch(
                            filter_map(PrivateChatCallbackData::is_status)
                                .filter_map_async(chat_manager::message_from_callback)
                                .filter_map_async(private_chat::auth_from_chat_id)
                                .chain(handler(private_chat::status)),
                        )
                        .branch(
                            filter_map(PrivateChatCallbackData::is_menu)
                                .filter_map_async(chat_manager::message_from_callback)
                                .filter_map_async(private_chat::auth_from_chat_id)
                                .chain(handler(private_chat::chat_menu)),
                        )
                        .branch(
                            filter(PrivateChatCallbackData::is_manage)
                                .chain(handler(private_chat::manage_help)),
                        ),
                )
                .branch(
                    filter(CallbackData::is_report)
                        .filter_map_async(chat_manager::message_from_callback)
                        .map(chat_manager::user_from_callback)
                        .chain(handler(report::report_callback)),
                ),
        )
        .branch(
            filter(chat_manager::group_chat)
                .map(config::merge_config_from_upd)
                .filter(chat_manager::not_outdated)
                .branch(
                    Update::filter_chat_join_request()
                        .map(chat_manager::chat_id_from_request)
                        .map(config::merge_config_from_chat_id)
                        .filter_map(Config::join_requests_enabled)
                        .chain(handler(join_request::request_wait_for_captcha)),
                )
                .branch(Update::filter_chat_member().chain(handler(handlers::chat_member_updated)))
                .branch(
                    Update::filter_message()
                        .branch(
                            filter_map(autocommands::extract_actions)
                                .chain(handler(autocommands::perform_actions)),
                        )
                        .branch(
                            filter_map(Config::captcha_enabled)
                                .filter_map_async(captcha::has_captcha)
                                .branch(
                                    filter_map_async(filter_command::<captcha::CaptchaCommand>)
                                        .chain(handler(captcha::captcha_command)),
                                )
                                .branch(
                                    pass(captcha::check_captcha).chain(handler(captcha::success)),
                                ),
                        )
                        .branch(
                            filter_map(Config::antispam_enabled)
                                .filter_map_async(antispam::is_spam)
                                .chain(handler(autocommands::perform_actions)),
                        )
                        .branch(
                            filter_map(Config::captcha_enabled)
                                .filter_map_async(filter_command::<captcha::CaptchaCommand>)
                                .filter(captcha::CaptchaCommand::is_cancel)
                                .filter_map_async(chat_manager::parse_admin)
                                .chain(handler(captcha::cancel)),
                        )
                        .branch(
                            filter_map_async(chat_manager::parse_admin)
                                .filter_map_async(filter_command::<admin::AdminCommand>)
                                .chain(handler(admin::admin_command)),
                        )
                        .branch(
                            filter_map_async(filter_optional_command::<commands::GeneralCommand>)
                                .branch(
                                    filter(commands::GeneralCommand::is_id)
                                        .chain(handler(handlers::id_command)),
                                )
                                .branch(
                                    filter(commands::GeneralCommand::is_rules)
                                        .chain(handler(handlers::rules_command)),
                                ),
                        )
                        .branch(
                            filter_map_async(filter_optional_command::<ReportCommand>).branch(
                                filter_map(Config::reports_enabled)
                                    .filter(ReportCommand::is_report)
                                    .chain(handler(report::report_command)),
                            ),
                        ),
                ),
        )
        .branch(private_chat_dispatch());
    let captcha_repository: Arc<dyn CaptchaRepository> = Arc::new(InMemoryRepository::new());
    let chat_config_repository: Arc<dyn ChatConfigRepository> =
        Arc::new(FilesystemChatConfigRepository::default());
    let chat_authority_repository: Arc<dyn ChatAuthorityRepository> =
        Arc::new(InMemoryRepository::new());
    let raid_repository: Arc<dyn RaidRepository> = Arc::new(InMemoryRepository::new());
    let antispam_repository: Arc<dyn antispam::SpamRepository> =
        Arc::new(InMemoryRepository::new());
    let mut container = DependencyMap::new();
    container.insert(id);
    container.insert(captcha_repository);
    container.insert(chat_config_repository);
    container.insert(chat_authority_repository);
    container.insert(raid_repository);
    container.insert(antispam_repository);

    let (captcha_tx, captcha_rx) = broadcast::channel::<CaptchaEvent>(50);
    container.insert(Arc::new(captcha_tx));
    container.insert(Arc::new(captcha_rx));

    let (chat_tx, chat_rx) = broadcast::channel::<RequestEvent>(50);
    container.insert(Arc::new(chat_tx));
    container.insert(Arc::new(chat_rx));

    let (chat_member_tx, chat_member_rx) = broadcast::channel::<ChatMemberEvent>(50);
    container.insert(Arc::new(chat_member_tx));
    container.insert(Arc::new(chat_member_rx));

    let err_handler = ReportingErrorHandler::new(bot.clone());

    let err_tx = err_handler.tx();
    container.insert(Arc::new(err_tx));

    Dispatcher::builder(bot, handler)
        .dependencies(container)
        .enable_ctrlc_handler()
        .default_handler(|upd| async move {
            log::debug!("Unhandled update: {upd:?}");
        })
        .error_handler(err_handler)
        .build()
        .dispatch()
        .await;
    Ok(())
}

pub fn private_chat_dispatch(
) -> Handler<'static, DependencyMap, Result<(), error::Error>, DpHandlerDescription> {
    filter(chat_manager::private_chat).branch(
        Update::filter_message()
            .branch(
                filter_map_async(captcha::has_captcha)
                    .filter_map(extract_chat_id)
                    .map(config::merge_config_from_chat_id)
                    .branch(
                        filter_map(Config::captcha_enabled)
                            .filter_map_async(filter_command::<captcha::CaptchaCommand>)
                            .chain(handler(captcha::captcha_command)),
                    )
                    .branch(handler(captcha::check_captcha)),
            )
            .branch(
                filter_map_async(filter_command::<commands::PrivateChatCommand>)
                    .branch(
                        filter(commands::PrivateChatCommand::is_start)
                            .chain(handler(private_chat::start)),
                    )
                    .branch(
                        filter_map(commands::PrivateChatCommand::is_start_chat)
                            .map(config::merge_config_from_chat_id)
                            .filter_map(Config::join_requests_enabled)
                            .chain(handler(join_request::start_chat)),
                    )
                    .branch(
                        filter(commands::PrivateChatCommand::is_manage)
                            .chain(handler(private_chat::manage)),
                    )
                    .branch(
                        filter_map(Message::from)
                            .filter_map_async(private_chat::get_auth)
                            .branch(
                                filter(commands::PrivateChatCommand::is_config)
                                    .map(chat_manager::config::base_config)
                                    .chain(handler(private_chat::config)),
                            )
                            .branch(
                                filter(commands::PrivateChatCommand::is_save)
                                    .chain(handler(private_chat::save_config)),
                            )
                            .branch(
                                filter(commands::PrivateChatCommand::is_status)
                                    .chain(handler(private_chat::status)),
                            )
                            .branch(
                                filter(commands::PrivateChatCommand::is_set_prop)
                                    .chain(handler(private_chat::set_property)),
                            ),
                    ),
            )
            .branch(
                filter(chat_manager::document)
                    .map(chat_manager::config::base_config)
                    .chain(handler(private_chat::check_config)),
            ),
    )
}
