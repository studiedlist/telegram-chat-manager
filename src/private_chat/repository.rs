use super::ChatAuthority;
use anyhow::Error;
use typesafe_repository::async_ops::Add;
use typesafe_repository::async_ops::Get;
use typesafe_repository::async_ops::Remove;
use typesafe_repository::async_ops::Save;
use typesafe_repository::async_ops::Update;
use typesafe_repository::inmemory::InMemoryRepository;
use typesafe_repository::prelude::*;

pub trait ChatAuthorityRepository:
    Repository<ChatAuthority, Error = Error>
    + Add<ChatAuthority>
    + Get<ChatAuthority>
    + Save<ChatAuthority>
    + Update<ChatAuthority>
    + Remove<ChatAuthority>
    + Send
    + Sync
{
}

impl ChatAuthorityRepository for InMemoryRepository<ChatAuthority, Error> {}
