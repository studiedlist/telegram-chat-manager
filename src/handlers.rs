use crate::ban_user;
use crate::captcha;
use crate::captcha::repository::CaptchaRepository;
use crate::captcha::CaptchaEvent;
use crate::config::arguments;
use crate::config::AutoCommand;
use crate::config::Config;
use crate::config::RulesConfig;
use crate::format_user;
use crate::ok_or_tx;
use crate::parse_access_level;
use crate::raid::RaidRepository;
use crate::EventTx;
use anyhow::anyhow;
use anyhow::Context;
use anyhow::Error;
use chrono::prelude::*;
use std::sync::Arc;
use teloxide::prelude::*;
use teloxide::types::Administrator;
use teloxide::types::ChatMember;
use teloxide::types::ChatMemberKind;
use teloxide::types::ChatMemberUpdated;
use teloxide::types::ParseMode;
use teloxide::types::UserId;
use teloxide::utils::markdown::escape;
use tokio::sync::mpsc;
use tokio::time::sleep;
use typesafe_repository::GetIdentity;

pub async fn id_command(bot: Bot, msg: Message, config: Arc<Config>) -> Result<(), Error> {
    let level = parse_access_level(&msg, &bot).await?;
    let level_matches = config
        .command
        .id
        .as_ref()
        .map(|x| level.has_access(x))
        .unwrap_or(false);
    if !level_matches {
        return Ok(());
    }
    let user = msg
        .reply_to_message()
        .and_then(Message::from)
        .or(msg.from());
    let user = user.ok_or(anyhow!("Message with no sender and reply"))?;
    let msg_id = match msg.reply_to_message() {
        None => msg.id,
        Some(m) => m.id,
    };
    let text = format!(
        "user: {}\nid: `{}`\nmessage id: `{}`",
        escape(&format_user(user)),
        user.id,
        msg_id
    );
    bot.send_message(msg.chat.id, text)
        .reply_to_message_id(msg.id)
        .parse_mode(ParseMode::MarkdownV2)
        .await?;
    Ok(())
}

pub async fn rules_command(
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    rules_config: Arc<RulesConfig>,
) -> Result<(), Error> {
    if let Some(mut rules) = rules_config.rules.clone() {
        if let Some(autocommands) = config
            .autocommands
            .as_ref()
            .filter(|_| rules_config.generate_from_autocommands)
        {
            let generated = itertools::intersperse(
                autocommands.iter().map(AutoCommand::generate_rules_entry),
                "\n".to_string(),
            )
            .collect::<String>();
            rules.push_str("\n\n");
            rules.push_str(&generated);
        }
        bot.send_message(msg.chat.id, rules)
            .reply_to_message_id(msg.id)
            .parse_mode(ParseMode::MarkdownV2)
            .await?;
    }
    Ok(())
}

#[derive(Debug, Clone)]
pub enum ChatMemberEvent {
    UserJoined(ChatMember),
    AdminDemotion(UserId, Administrator),
}

pub async fn chat_member_updated(
    bot_id: UserId,
    upd: ChatMemberUpdated,
    bot: Bot,
    captcha_repo: Arc<dyn CaptchaRepository>,
    raid_repo: Arc<dyn RaidRepository>,
    config: Arc<Config>,
    tx: Arc<EventTx<CaptchaEvent>>,
    event_tx: Arc<EventTx<ChatMemberEvent>>,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
) -> Result<(), Error> {
    let user = upd.new_chat_member.user.clone();
    if upd.from.id == bot_id || user.is_bot {
        return Ok(());
    }
    match (&upd.old_chat_member.kind, &upd.new_chat_member.kind) {
        (
            ChatMemberKind::Owner(_) | ChatMemberKind::Administrator(_) | ChatMemberKind::Member,
            ChatMemberKind::Left,
        ) => {
            if let Some(p) = &config.goodbye {
                let args = arguments::Username::new(format_user(&user));
                let msg = p.apply(args);
                bot.send_message(upd.chat.id, msg).await?;
            }
        }
        (ChatMemberKind::Administrator(admin), ChatMemberKind::Member) => {
            event_tx.send(ChatMemberEvent::AdminDemotion(user.id, admin.clone()))?;
        }
        (ChatMemberKind::Left | ChatMemberKind::Banned(_), ChatMemberKind::Member) => {
            if raid_repo.get_one(&upd.chat.id).await?.is_some() {
                if let Some(text) = ban_user(
                    &bot,
                    &upd.chat,
                    &user,
                    Some(String::from("Raid mode is *enabled*\\. \\#raid\\_ban")),
                    config.clone(),
                )
                .await?
                {
                    bot.send_message(upd.chat.id, text)
                        .parse_mode(ParseMode::MarkdownV2)
                        .await?;
                };
                return Ok(());
            };
            if let Some(captcha_config) = config.captcha.clone() {
                let timeout = captcha_config
                    .captcha_timeout
                    .map(|timeout| Utc::now().signed_duration_since(upd.date) < timeout);
                if let Some(false) = timeout {
                    log::debug!("Captcha timeout for update {upd:?}");
                }
                let caption = escape(&captcha_config.captcha_caption.apply(&user));
                let captcha =
                    captcha::send_captcha(upd.chat.id, &user, &bot, &captcha_config, &caption)
                        .await?;
                let id = captcha.id();
                captcha_repo.add(captcha).await?;
                let time_limit = captcha_config.time_limit.to_std()?;
                tokio::spawn(async move {
                    sleep(time_limit).await;
                    let res = ok_or_tx(
                        err_tx.clone(),
                        Some(upd.chat.id),
                        captcha_repo
                            .get_one(&id)
                            .await
                            .context("Unable to get captcha for user in timeout worker"),
                    )
                    .await
                    .flatten();
                    if let Some(captcha) = res {
                        let res = tx.send(CaptchaEvent::Timeout(captcha.clone()));
                        if ok_or_tx(err_tx.clone(), Some(upd.chat.id), res)
                            .await
                            .is_none()
                        {
                            return;
                        }
                        let res = captcha::failure(
                            bot,
                            upd.chat.id,
                            &user,
                            captcha_repo,
                            captcha,
                            Arc::new(captcha_config.clone()),
                        )
                        .await;
                        if ok_or_tx(err_tx.clone(), Some(upd.chat.id), res)
                            .await
                            .is_none()
                        {}
                    }
                });
            }
        }
        _ => (),
    };
    Ok(())
}
