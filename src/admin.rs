use crate::ban_user;
use crate::config::Config;
use crate::format_user;
use crate::get_chat_member;
use crate::get_user;
use crate::handlers::ChatMemberEvent;
use crate::kick_user;
use crate::mute_user;
use crate::ok_or_tx;
use crate::parse_duration;
use crate::raid::Raid;
use crate::raid::RaidRepository;
use crate::unban_user;
use crate::unmute_user;
use crate::Admin;
use crate::EventRx;
use anyhow::anyhow;
use anyhow::Error;
use chrono::Duration;
use std::sync::Arc;
use teloxide::prelude::*;
use teloxide::types::BotCommand;
use teloxide::types::ChatMember;
use teloxide::types::ChatMemberKind;
use teloxide::types::MessageId;
use teloxide::types::ParseMode;
use teloxide::utils::command::BotCommands;
use teloxide::utils::command::CommandDescription;
use teloxide::utils::command::CommandDescriptions;
use teloxide::utils::command::ParseError;
use teloxide::RequestError;
use tokio::sync::mpsc;

#[derive(Clone, Debug)]
pub enum AdminCommand {
    Ban(Option<UserId>, Option<String>),
    Kick(Option<UserId>, Option<String>),
    Mute(Option<UserId>, Duration, Option<String>),
    Unmute(Option<UserId>),
    Unban(Option<UserId>),
    Deli(u32, bool),
    Delr(i32, i32),
    Raid(RaidArgument),
    Reassign(Option<UserId>),
}

#[derive(Clone, Debug)]
pub enum DelMode {
    Deli,
    Delr,
}

#[derive(Clone, Debug)]
pub enum RaidArgument {
    Enable,
    Disable,
    Info,
}

impl BotCommands for AdminCommand {
    fn parse(s: &str, _bot_username: &str) -> Result<Self, ParseError> {
        let parse_id = |x: Option<&str>| {
            x.map(|x| {
                x.parse::<u64>()
                    .map(UserId)
                    .map_err(Box::new)
                    .map_err(|err| ParseError::IncorrectFormat(err))
            })
        };
        let mut split = s.split(' ');
        match split.next() {
            Some("/reassign") => parse_id(split.next()).transpose().map(Self::Reassign),
            Some("/ban") => {
                let next = split.next();
                match parse_id(next) {
                    Some(Ok(id)) => {
                        let reason: String = itertools::intersperse(split, " ").collect();
                        let reason = match reason.is_empty() {
                            true => None,
                            false => Some(reason),
                        };
                        Ok(Self::Ban(Some(id), reason))
                    }
                    Some(Err(_)) => {
                        let reason = next.map(ToString::to_string).map(|mut n| {
                            n.push(' ');
                            n.push_str(&itertools::intersperse(split, " ").collect::<String>());
                            n
                        });
                        Ok(Self::Ban(None, reason))
                    }
                    None => Ok(Self::Ban(None, None)),
                }
            }
            Some("/unban") => parse_id(split.next()).transpose().map(Self::Unban),
            Some("/unmute") => parse_id(split.next()).transpose().map(Self::Unmute),
            Some("/kick") => {
                let next = split.next();
                match parse_id(next) {
                    Some(Ok(id)) => {
                        let reason: String = itertools::intersperse(split, " ").collect();
                        let reason = match reason.is_empty() {
                            true => None,
                            false => Some(reason),
                        };
                        Ok(Self::Kick(Some(id), reason))
                    }
                    Some(Err(_)) => {
                        let reason = next.map(ToString::to_string).map(|mut n| {
                            n.push(' ');
                            n.push_str(&itertools::intersperse(split, " ").collect::<String>());
                            n
                        });
                        Ok(Self::Kick(None, reason))
                    }
                    None => Ok(Self::Kick(None, None)),
                }
            }
            Some("/mute") => {
                let n = split.next();
                let id = match parse_id(n).transpose() {
                    Ok(id) => id,
                    Err(err) => {
                        return n
                            .map(parse_duration)
                            .transpose()
                            .map_err(|err| ParseError::IncorrectFormat(err.into()))?
                            .map(|duration| {
                                let reason: String = itertools::intersperse(split, " ").collect();
                                let reason = match reason.is_empty() {
                                    true => None,
                                    false => Some(reason),
                                };
                                Ok(Self::Mute(None, duration, reason))
                            })
                            .transpose()?
                            .ok_or(err)
                    }
                };
                let duration = split
                    .next()
                    .map(|x| {
                        parse_duration(x).map_err(|err| ParseError::IncorrectFormat(err.into()))
                    })
                    .transpose()?;
                match duration {
                    Some(duration) => {
                        let reason: String = itertools::intersperse(split, " ").collect();
                        let reason = match reason.is_empty() {
                            true => None,
                            false => Some(reason),
                        };
                        Ok(Self::Mute(id, duration, reason))
                    }
                    None => Err(ParseError::TooFewArguments {
                        expected: 1,
                        found: 0,
                        message: "You need to specify mute time".to_string(),
                    }),
                }
            }
            Some("/del") => {
                let mode: DelMode;
                let arg1 = match split.next().map(|s| s.parse::<i32>().ok()).flatten() {
                    Some(n) => {
                        n
                    }
                    None => {
                        return Err(ParseError::IncorrectFormat(
                            anyhow!("The first argument is not a number").into()
                        ))
                    }
                };


                let arg2 = match split.next() {
                    Some(s) => match s.parse::<i32>() {
                        Err(..) => {
                            return Err(ParseError::IncorrectFormat(
                                anyhow!("The second argument is not a number").into()
                            ))
                        }
                        Ok(n) => {
                            mode = DelMode::Delr;
                            n
                        }
                    },
                    None => {
                        mode = DelMode::Deli;
                        0
                    }
                };

                match mode {
                    DelMode::Deli => {
                        let reverse = arg1 < 0;
                        let arg1 = arg1.checked_abs()
                            .map(|n| {
                                n.try_into()
                                    .map_err(|e| anyhow::Error::new(e))
                                    .map_err(Into::into)
                                    .map_err(ParseError::IncorrectFormat)
                            })
                            .ok_or(anyhow!("The first argument is too small"))
                            .map_err(Into::into)
                            .map_err(ParseError::IncorrectFormat)??;
                        Ok(Self::Deli(arg1, reverse))
                    },
                    DelMode::Delr => {
                        Ok(Self::Delr(arg1, arg2))
                    },
                }
            }
            Some("/raid") => split.next()
                .map(|x| match x.parse::<bool>()? {
                    true => Ok(RaidArgument::Enable),
                    false => Ok(RaidArgument::Disable),
                })
                .unwrap_or(Ok(RaidArgument::Info))
                .map_err(|_: std::str::ParseBoolError| {
                    ParseError::IncorrectFormat(anyhow::anyhow!("Argument is not boolean. You need to specify state of raid mode (`true` or `false`)").into())
                })
                .map(AdminCommand::Raid),
            Some(text) => Err(ParseError::UnknownCommand(text.to_string())),
            None => Err(ParseError::UnknownCommand("/".to_string())),
        }
    }
    fn descriptions() -> CommandDescriptions<'static> {
        CommandDescriptions::new(&[
            CommandDescription {
                prefix: "/",
                command: "reassign",
                description: "Reassign admin",
            },
            CommandDescription {
                prefix: "/",
                command: "ban",
                description: "Ban chat member",
            },
            CommandDescription {
                prefix: "/",
                command: "mute",
                description: "Mute chat member",
            },
            CommandDescription {
                prefix: "/",
                command: "unban",
                description: "Unban chat member",
            },
            CommandDescription {
                prefix: "/",
                command: "unmute",
                description: "Unmute chat member",
            },
            CommandDescription {
                prefix: "/",
                command: "kick",
                description: "Kick chat member",
            },
            CommandDescription {
                prefix: "/",
                command: "del",
                description: "Delete messages",
            },
        ])
    }
    fn bot_commands() -> Vec<BotCommand> {
        vec![
            BotCommand {
                command: "reassign".to_string(),
                description: "Reassign chat member".to_string(),
            },
            BotCommand {
                command: "ban".to_string(),
                description: "Ban chat member".to_string(),
            },
            BotCommand {
                command: "mute".to_string(),
                description: "Mute chat member".to_string(),
            },
            BotCommand {
                command: "unban".to_string(),
                description: "Unban chat member".to_string(),
            },
            BotCommand {
                command: "unmute".to_string(),
                description: "Unmute chat member".to_string(),
            },
            BotCommand {
                command: "kick".to_string(),
                description: "Kick chat member".to_string(),
            },
            BotCommand {
                command: "del".to_string(),
                description: "Delete messages".to_string(),
            },
        ]
    }
}

pub async fn reassign(
    bot: Bot,
    msg: Message,
    member: ChatMember,
    event_rx: Arc<EventRx<ChatMemberEvent>>,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
) -> Result<(), Error> {
    let formatted_user = format_user(&member.user);
    let chat_id = msg.chat.id;
    let user_id = member.user.id;
    let mut event_rx = event_rx.resubscribe();
    match member.kind {
        ChatMemberKind::Administrator(admin) if admin.can_be_edited => {
            return Err(anyhow!(
                "This admin has been assigned by me. No reassigning needed"
            ));
        }
        ChatMemberKind::Administrator(_) => {
            bot.send_message(chat_id, format!("Reassigning admin {formatted_user}"))
                .await?;
        }
        _ => return Err(anyhow!("This user is not an admin")),
    };
    tokio::spawn(async move {
        loop {
            let event = ok_or_tx(err_tx.clone(), Some(chat_id), event_rx.recv().await).await;
            match event {
                Some(ChatMemberEvent::AdminDemotion(id, admin)) if id == user_id => {
                    let res = bot
                        .promote_chat_member(chat_id, user_id)
                        .is_anonymous(admin.is_anonymous)
                        .can_manage_chat(admin.can_manage_chat)
                        .can_post_messages(admin.can_post_messages)
                        .can_edit_messages(admin.can_edit_messages)
                        .can_delete_messages(admin.can_delete_messages)
                        .can_manage_video_chats(admin.can_manage_video_chats)
                        .can_restrict_members(admin.can_restrict_members)
                        .can_promote_members(admin.can_promote_members)
                        .can_change_info(admin.can_change_info)
                        .can_invite_users(admin.can_invite_users)
                        .can_pin_messages(admin.can_pin_messages)
                        .can_manage_topics(admin.can_manage_topics)
                        .await;
                    if ok_or_tx(err_tx.clone(), Some(chat_id), res).await.is_some() {
                        let res = bot
                            .send_message(
                                chat_id,
                                format!("Successfully reassigned admin {formatted_user}"),
                            )
                            .await;
                        return ok_or_tx(err_tx.clone(), Some(chat_id), res).await;
                    }
                }
                _ => (),
            }
        }
    });
    Ok(())
}

pub async fn admin_command(
    msg: Message,
    cmd: AdminCommand,
    raid_repo: Arc<dyn RaidRepository>,
    _admin: Arc<Admin>,
    bot: Bot,
    event_rx: Arc<EventRx<ChatMemberEvent>>,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
    config: Arc<Config>,
) -> Result<(), Error> {
    if !teloxide_extras::filters::dptree::is_admin(msg.clone(), bot.clone()).await? {
        return Ok(());
    }
    let user = msg.reply_to_message().and_then(Message::from).map(|x| x.id);
    let delete_message = |msg_id| bot.delete_message(msg.chat.id, msg_id);
    match cmd {
        AdminCommand::Ban(id, reason) => {
            let id = id.or(user).ok_or(anyhow!(
                "You must reply to message or specify user id to perform this command"
            ))?;
            let user = get_user(&bot, msg.chat.id, id).await?;
            if let Some(text) = ban_user(&bot, &msg.chat, &user, reason, config).await? {
                bot.send_message(msg.chat.id, text).await?;
            }
        }
        AdminCommand::Kick(id, reason) => {
            let id = id.or(user).ok_or(anyhow!(
                "You must reply to message or specify user id to perform this command"
            ))?;
            let user = get_user(&bot, msg.chat.id, id).await?;
            if let Some(text) = kick_user(&bot, &msg.chat, &user, reason, config).await? {
                bot.send_message(msg.chat.id, text).await?;
            }
        }
        AdminCommand::Mute(id, duration, reason) => {
            let id = id.or(user).ok_or(anyhow!(
                "You must reply to message or specify user id to perform this command"
            ))?;
            let user = get_user(&bot, msg.chat.id, id).await?;
            if let Some(text) =
                mute_user(&duration, &bot, msg.chat.id, &user, reason, config).await?
            {
                bot.send_message(msg.chat.id, text).await?;
            }
        }
        AdminCommand::Unmute(id) => {
            let id = id.or(user).ok_or(anyhow!(
                "You must reply to message or specify user id to perform this command"
            ))?;
            let user = get_user(&bot, msg.chat.id, id).await?;
            unmute_user(&bot, msg.chat.id, &user).await?;
        }
        AdminCommand::Unban(id) => {
            let id = id.or(user).ok_or(anyhow!(
                "You must reply to message or specify user id to perform this command"
            ))?;
            let user = get_user(&bot, msg.chat.id, id).await?;
            unban_user(&bot, msg.chat.id, user.id).await?;
        }
        AdminCommand::Deli(delete_messages_count, reverse) => {
            if delete_messages_count == 0 {
                return Err(anyhow!("Index cannot be zero"));
            }
            let start_msg = msg
                .reply_to_message()
                .ok_or(anyhow!("You must reply to message to perform this command"))?;
            let mut messages_deleted = 0;
            let mut cur_index_shift: i32 = 0;
            // let update_index_shift : Box<dyn Fn(i32) -> i32>;
            if reverse {
                let mut err_count: u8 = 0;
                while messages_deleted < delete_messages_count && err_count < 25 {
                    match delete_message(MessageId(start_msg.id.0 - cur_index_shift)).await {
                        Ok(..) => {
                            err_count = 0;
                            messages_deleted += 1;
                        }
                        Err(RequestError::Api(e)) => {
                            err_count += 1;
                            log::error!("{}", e)
                        }
                        Err(e) => log::error!("{}", e),
                    }
                    cur_index_shift += 1;
                }
                // update_index_shift = Box::new(|v| { v - 1 });
            } else {
                while messages_deleted < delete_messages_count {
                    let next_msg_id = start_msg.id.0 + cur_index_shift;
                    if next_msg_id > msg.id.0 {
                        break;
                    }
                    match delete_message(MessageId(next_msg_id)).await {
                        Ok(..) => {
                            messages_deleted += 1;
                        }
                        Err(e) => {
                            log::error!("{}", e)
                        }
                    }
                    cur_index_shift += 1;
                }
                // update_index_shift = Box::new(|v| { v + 1 });
            }
        }
        AdminCommand::Delr(msg1_id, msg2_id) => {
            for msg_id in msg1_id..(msg2_id + 1) {
                if let Err(e) = delete_message(MessageId(msg_id)).await {
                    log::error!("{}", e) // TODO: catch multiple errors in row (no more messages in chat to delete)
                }
            }
        }
        AdminCommand::Raid(mode) => {
            let sender = msg.from().ok_or(anyhow!("Report command with no sender"))?;
            let text = match mode {
                RaidArgument::Enable => {
                    // enable raid mode by adding chat id to repo
                    match raid_repo.get_one(&msg.chat.id).await? {
                        None => {
                            raid_repo.add(Raid { id: msg.chat.id }).await?;
                            format!("\\#Raid mode enabled by {}", format_user(sender))
                        }
                        Some(_) => "\\#Raid mode is already enabled".to_string(),
                    }
                }
                RaidArgument::Disable => {
                    //disable raid mode by deleting chat id from repo
                    match raid_repo.get_one(&msg.chat.id).await? {
                        Some(_) => {
                            raid_repo.remove(&msg.chat.id).await?;
                            format!("\\#Raid mode disabled by {}", format_user(sender))
                        }
                        None => "\\#Raid mode is already disabled".to_string(),
                    }
                }
                RaidArgument::Info => match raid_repo.get_one(&msg.chat.id).await? {
                    Some(_) => "\\#Raid is enabled".to_string(),
                    None => "\\#Raid is disabled".to_string(),
                },
            };
            bot.send_message(msg.chat.id, text)
                .parse_mode(ParseMode::MarkdownV2)
                .await?;
        }
        AdminCommand::Reassign(id) => {
            let id = id.or(user).ok_or(anyhow!(
                "You must reply to message or specify user id to perform this command"
            ))?;
            let user = get_chat_member(&bot, msg.chat.id, id).await?;
            reassign(bot, msg, user, event_rx, err_tx).await?
        }
    }
    Ok(())
}
