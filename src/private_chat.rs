pub mod repository;

use crate::callback::CallbackData;
use crate::callback::CallbackDataClass;
use crate::commands::PrivateChatCommand;
use crate::config::chat::ChatConfig;
use crate::config::chat::ChatConfigRepository;
use crate::config::extension::ConfigExtension;
use crate::config::merge_config_from_chat_id;
use crate::config::set_field::SetField;
use crate::config::Config;
use crate::download_file;
use crate::extract_sender;
use crate::ok_or_tx;
use anyhow::anyhow;
use anyhow::Error;
use bytes::Bytes;
use config_diff::diff_to_string;
use config_diff::DefaultDiffer;
use config_extension::Extender;
use repository::ChatAuthorityRepository;
use std::sync::Arc;
use teloxide::errors::ApiError;
use teloxide::errors::RequestError;
use teloxide::prelude::*;
use teloxide::types::ChatId;
use teloxide::types::InlineKeyboardButton;
use teloxide::types::InlineKeyboardButtonKind;
use teloxide::types::InlineKeyboardMarkup;
use teloxide::types::InputFile;
use teloxide::types::ParseMode;
use teloxide::types::ReplyMarkup;
use teloxide::types::User;
use teloxide::utils::markdown::escape;
use teloxide_extras::filters::is_admin;
use tokio::sync::mpsc;
use typesafe_repository::macros::Id;
use typesafe_repository::GetIdentity;
use typesafe_repository::Identity;
use typesafe_repository::RefIdentity;

#[derive(Clone, Debug)]
pub enum PrivateChatCallbackData {
    Manage,
    Status(ChatId),
    Menu(ChatId),
}

impl PrivateChatCallbackData {
    pub async fn try_parse(
        chat_id: ChatId,
        data: CallbackData,
        err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
    ) -> Option<Self> {
        ok_or_tx(
            err_tx,
            Some(chat_id),
            Self::new(data.command().clone(), data.args()),
        )
        .await
        .flatten()
    }

    pub fn new(cmd: String, args: &[String]) -> Result<Option<Self>, Error> {
        let res = match cmd.as_str() {
            "manage" => Self::Manage,
            "status" => args
                .first()
                .map(|x| x.parse())
                .transpose()?
                .map(|id| Self::Status(ChatId(id)))
                .ok_or(anyhow!("Data must contain chat id"))?,
            "menu" => args
                .first()
                .map(|x| x.parse())
                .transpose()?
                .map(|id| Self::Menu(ChatId(id)))
                .ok_or(anyhow!("Data must contain chat id"))?,
            _ => return Ok(None),
        };
        Ok(Some(res))
    }
    pub fn is_manage(self) -> bool {
        matches!(self, Self::Manage)
    }
    pub fn is_status(self) -> Option<ChatId> {
        match self {
            Self::Status(id) => Some(id),
            _ => None,
        }
    }
    pub fn is_menu(self) -> Option<ChatId> {
        match self {
            Self::Menu(id) => Some(id),
            _ => None,
        }
    }
}

impl TryInto<String> for PrivateChatCallbackData {
    type Error = Error;

    fn try_into(self) -> Result<String, Error> {
        let (cmd, args) = match self {
            Self::Manage => ("manage", vec![]),
            Self::Status(id) => ("status", vec![id.to_string()]),
            Self::Menu(id) => ("menu", vec![id.to_string()]),
        };
        CallbackData::new(CallbackDataClass::PrivateChat, cmd.to_string(), args).map(Into::into)
    }
}

#[derive(Id, Clone)]
#[Id(get_id, ref_id)]
pub struct ChatAuthority {
    #[id]
    pub user: UserId,
    pub state: State,
    pub chat: ChatId,
}

#[derive(Clone)]
pub enum State {
    AuthorityConfirmed,
    Config(ConfigState),
}

#[derive(Clone)]
pub enum ConfigState {
    Upload,
    Diff(Box<ConfigExtension>),
}

pub async fn auth_from_chat_id(
    chat_id: ChatId,
    msg: Message,
    user: User,
    bot: Bot,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
) -> Option<ChatAuthority> {
    ok_or_tx(
        err_tx,
        Some(msg.chat.id),
        is_admin(chat_id, user.id, &bot).await,
    )
    .await
    .map(|_| ChatAuthority {
        user: user.id,
        chat: chat_id,
        state: State::AuthorityConfirmed,
    })
}

pub async fn get_auth(
    auth_repo: Arc<dyn ChatAuthorityRepository>,
    user: User,
) -> Option<ChatAuthority> {
    match auth_repo.get_one(&user.id).await {
        Ok(Some(a)) => Some(a),
        Ok(None) => None,
        Err(err) => {
            log::error!("Unable to get auth from repo: {err:?}");
            None
        }
    }
}

pub async fn start(msg: Message, bot: Bot) -> Result<(), Error> {
    bot.send_message(msg.chat.id, "Welcome")
        .reply_to_message_id(msg.id)
        .reply_markup(ReplyMarkup::InlineKeyboard(InlineKeyboardMarkup {
            inline_keyboard: vec![vec![InlineKeyboardButton {
                text: "Manage chat".to_string(),
                kind: InlineKeyboardButtonKind::CallbackData(
                    PrivateChatCallbackData::Manage.try_into()?,
                ),
            }]],
        }))
        .parse_mode(ParseMode::MarkdownV2)
        .await?;
    Ok(())
}

pub async fn invalid_start_chat(bot: Bot, msg: Message) -> Result<(), Error> {
    bot.send_message(msg.chat.id, "I cannot manage that chat")
        .await?;
    Ok(())
}

pub async fn manage_help(bot: Bot, chat_id: ChatId) -> Result<(), Error> {
    bot.send_message(
        chat_id,
        "Send `/manage <chat_id>` and I will verify your admin rights",
    )
    .await?;
    Ok(())
}

pub async fn manage(
    cmd: PrivateChatCommand,
    msg: Message,
    bot: Bot,
    repo: Arc<dyn ChatAuthorityRepository>,
) -> Result<(), Error> {
    let user = extract_sender(&msg)?;
    let chat_id = match cmd {
        PrivateChatCommand::Manage(id) => id,
        _ => unreachable!(),
    };
    let _chat = match bot.get_chat(chat_id).await {
        Ok(c) => c,
        Err(RequestError::Api(ApiError::ChatNotFound)) => return Err(anyhow!("Chat not found")),
        Err(err) => return Err(err.into()),
    };
    if is_admin(chat_id, user.id, &bot).await? {
        repo.save(ChatAuthority {
            user: user.id,
            chat: chat_id,
            state: State::AuthorityConfirmed,
        })
        .await?;
        bot.send_message(msg.chat.id, format!("Menu of chat `{chat_id}`"))
            .reply_to_message_id(msg.id)
            .parse_mode(ParseMode::MarkdownV2)
            .reply_markup(ReplyMarkup::InlineKeyboard(chat_menu_markup(chat_id)?))
            .await?;
    } else {
        bot.send_message(msg.chat.id, "You are not admin of this chat")
            .reply_to_message_id(msg.id)
            .await?;
    }
    Ok(())
}

pub fn chat_menu_markup(chat_id: ChatId) -> Result<InlineKeyboardMarkup, Error> {
    Ok(InlineKeyboardMarkup {
        inline_keyboard: vec![vec![InlineKeyboardButton {
            text: "Status".to_string(),
            kind: InlineKeyboardButtonKind::CallbackData(
                PrivateChatCallbackData::Status(chat_id).try_into()?,
            ),
        }]],
    })
}

pub async fn config(
    msg: Message,
    bot: Bot,
    mut auth: ChatAuthority,
    auth_repo: Arc<dyn ChatAuthorityRepository>,
    config_repo: Arc<dyn ChatConfigRepository>,
) -> Result<(), Error> {
    let chat_id = auth.chat;
    let config = match config_repo.get_one(&chat_id)? {
        Some(c) => c.config,
        None => ConfigExtension::default(),
    };
    let file = InputFile::memory(Bytes::from(serde_yaml::to_string(&config)?))
        .file_name("config_extension.yaml");
    bot.send_document(msg.chat.id, file)
        .reply_to_message_id(msg.id)
        .caption(&format!("Config of chat `{chat_id}`\nPlease send new version of config extension as document in order to update chat config",))
        .await?;
    auth.state = State::Config(ConfigState::Upload);
    auth_repo.update(auth).await?;
    Ok(())
}

pub async fn check_config(
    msg: Message,
    bot: Bot,
    mut auth: ChatAuthority,
    auth_repo: Arc<dyn ChatAuthorityRepository>,
    config_repo: Arc<dyn ChatConfigRepository>,
    config: Arc<Config>,
) -> Result<(), Error> {
    if let State::AuthorityConfirmed = auth.state {
        return Err(anyhow!("You haven't started work with config yet"));
    }
    let chat_id = auth.chat;
    let send_message = |text: &str| {
        bot.send_message(msg.chat.id, text)
            .reply_to_message_id(msg.id)
            .parse_mode(ParseMode::MarkdownV2)
    };
    if let Some(doc) = msg.document() {
        let config = match config_repo.get_one(&chat_id)? {
            Some(chat_config) => Arc::new(chat_config.config.try_extend(&config)?),
            None => config,
        };
        let res = download_file(&bot, &doc.file).await?;
        let extension: ConfigExtension = match serde_yaml::from_str(&res) {
            Ok(ex) => ex,
            Err(err) => {
                return Err(anyhow!(
                    "Extension is incorrect\n`{}`",
                    escape(&err.to_string())
                ))
            }
        };
        let extended = match extension.clone().try_extend(&config) {
            Ok(extended) => extended,
            Err(err) => {
                return Err(anyhow!(
                    "Extension is incorrect\n`{}`",
                    escape(&err.to_string())
                ))
            }
        };
        let diff = diff_to_string::<DefaultDiffer, _>(&*config, &extended)?;
        auth.state = State::Config(ConfigState::Diff(Box::new(extension)));
        auth_repo.update(auth).await?;
        send_message(&format!(
            "Extension is correct\nPlease review your changes\n`{}`\n\nSend `/save` to accept changes and update config extension",
            escape(&diff)
        ))
        .await?;
        Ok(())
    } else {
        send_message("Please send config file to update chat config").await?;
        Ok(())
    }
}

pub async fn save_config(
    config_repo: Arc<dyn ChatConfigRepository>,
    auth_repo: Arc<dyn ChatAuthorityRepository>,
    auth: ChatAuthority,
    msg: Message,
    bot: Bot,
) -> Result<(), Error> {
    let user = extract_sender(&msg)?;
    let send_message = |text: &str| {
        bot.send_message(msg.chat.id, text)
            .reply_to_message_id(msg.id)
    };
    let (mut auth, extension): (ChatAuthority, Box<ConfigExtension>) = match auth.state {
        State::AuthorityConfirmed => {
            return Err(anyhow!("You haven't started work with config yet"))
        }
        State::Config(ConfigState::Upload) => {
            return Err(anyhow!("You haven't uploaded config yet"))
        }
        State::Config(ConfigState::Diff(ref ex)) => {
            let ex = ex.clone();
            (auth, ex)
        }
    };
    let chat_id = auth.chat;
    if is_admin(chat_id, user.id, &bot).await? {
        config_repo.save(ChatConfig::new(chat_id, *extension))?;
        send_message("Config saved").await?;
        auth.state = State::AuthorityConfirmed;
        auth_repo.update(auth).await?;
    } else {
        return Err(anyhow!("You are not admin of this chat"));
    }
    Ok(())
}

pub async fn status(
    config_repo: Arc<dyn ChatConfigRepository>,
    auth: ChatAuthority,
    msg: Message,
    bot: Bot,
) -> Result<(), Error> {
    let config = merge_config_from_chat_id(auth.chat, config_repo);
    bot.edit_message_text(msg.chat.id, msg.id, config.status())
        .reply_markup(back_menu(auth.chat)?)
        .await?;
    Ok(())
}

pub fn back_menu(chat_id: ChatId) -> Result<InlineKeyboardMarkup, Error> {
    Ok(InlineKeyboardMarkup {
        inline_keyboard: vec![vec![InlineKeyboardButton {
            text: "Back to menu".to_string(),
            kind: InlineKeyboardButtonKind::CallbackData(
                PrivateChatCallbackData::Menu(chat_id).try_into()?,
            ),
        }]],
    })
}

pub async fn chat_menu(bot: Bot, msg: Message, auth: ChatAuthority) -> Result<(), Error> {
    bot.edit_message_text(msg.chat.id, msg.id, format!("Menu of chat `{}`", auth.chat))
        .parse_mode(ParseMode::MarkdownV2)
        .reply_markup(chat_menu_markup(auth.chat)?)
        .await?;
    Ok(())
}

pub async fn set_property(
    bot: Bot,
    msg: Message,
    auth: ChatAuthority,
    cmd: PrivateChatCommand,
    repo: Arc<dyn ChatConfigRepository>,
) -> Result<(), Error> {
    let send_message = |text| {
        bot.send_message(msg.chat.id, text)
            .reply_to_message_id(msg.id)
    };
    let (name, value) = match cmd {
        PrivateChatCommand::SetProp(name, value) => (name, value),
        _ => unreachable!(),
    };
    let chat_id = auth.chat;
    let mut c = repo
        .get_one(&chat_id)?
        .ok_or(anyhow!("This chat doesn't have custom config"))?;
    c.config
        .set_field(&name, value)
        .map_err(|err| anyhow!("Value is invalid\n{err}"))?;
    repo.save(c)?;
    send_message("Config updated succesfully").await?;
    Ok(())
}
