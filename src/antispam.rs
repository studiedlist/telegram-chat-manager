use crate::config::Action;
use crate::config::AntispamConfig;
use crate::ok_or_tx;
use anyhow::Error;
use chrono::offset::Utc;
use chrono::DateTime;
use std::sync::Arc;
use teloxide::types::ChatId;
use teloxide::types::Message;
use teloxide::types::UserId;
use tokio::sync::mpsc;
use typesafe_repository::async_ops::*;
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use typesafe_repository::GetIdentity;
use typesafe_repository::Identity;
use typesafe_repository::RefIdentity;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct SpamId(pub ChatId, pub UserId);

#[derive(Id, Clone, Debug)]
#[Id(ref_id, get_id)]
pub struct Spam {
    pub id: SpamId,
    pub last_msg_time: DateTime<Utc>,
    pub counter: u8,
}

impl Spam {
    pub fn chat_id(&self) -> &ChatId {
        &self.id.0
    }
    pub fn user_id(&self) -> &UserId {
        &self.id.1
    }
}

pub trait SpamRepository:
    Repository<Spam, Error = Error> + Add<Spam> + Get<Spam> + Update<Spam> + Remove<Spam> + Send + Sync
{
}

impl<T> SpamRepository for T where
    T: Repository<Spam, Error = Error>
        + Add<Spam>
        + Get<Spam>
        + Update<Spam>
        + Remove<Spam>
        + Send
        + Sync
{
}

pub async fn is_spam(
    msg: Message,
    repo: Arc<dyn SpamRepository>,
    err_tx: Arc<mpsc::Sender<(Option<ChatId>, Error)>>,
    config: Arc<AntispamConfig>,
) -> Option<Vec<Action>> {
    let user = msg.from()?;
    let id = SpamId(msg.chat.id, user.id);
    let ok_or_tx = |v| ok_or_tx(err_tx, Some(msg.chat.id), v);
    match ok_or_tx(repo.get_one(&id).await).await? {
        Some(s) => {
            // spam info for user & chat found
            if msg.date == s.last_msg_time {
                let new_counter = s.counter + 1;
                drop(
                    repo.update(Spam {
                        counter: new_counter,
                        ..s
                    })
                    .await,
                );
                if new_counter > config.limit {
                    Some(config.actions.clone())
                } else {
                    None
                }
            } else {
                drop(
                    repo.update(Spam {
                        last_msg_time: msg.date,
                        counter: 0,
                        ..s
                    })
                    .await,
                );
                None
            }
        }
        None => {
            drop(
                repo.add(Spam {
                    id,
                    last_msg_time: msg.date,
                    counter: 0,
                })
                .await,
            );
            None
        }
    }
}
