use crate::admin::AdminCommand;
use assert_matches::assert_matches;
use chrono::Duration;
use teloxide::types::UserId;
use teloxide::utils::command::BotCommands;
use teloxide::utils::command::ParseError;

#[test]
fn parses_admin_ban() {
    let input = "/ban";
    let res = AdminCommand::parse(input, "test");
    assert_matches!(res, Ok(AdminCommand::Ban(None, None)));
}

#[test]
fn parses_admin_ban_id() {
    let id = UserId(123456);
    let input = format!("/ban {id}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(res, Ok(AdminCommand::Ban(Some(i), None)) if i == id);
}

#[test]
fn parses_admin_ban_reason() {
    let reason = "multi word reason";
    let input = format!("/ban {reason}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(res, Ok(AdminCommand::Ban(None, Some(r))) if r == reason);
}

#[test]
fn parses_admin_kick() {
    let input = "/kick";
    let res = AdminCommand::parse(input, "test");
    assert_matches!(res, Ok(AdminCommand::Kick(None, None)));
}

#[test]
fn parses_admin_kick_id() {
    let id = UserId(123456);
    let input = format!("/kick {id}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(res, Ok(AdminCommand::Kick(Some(i), None)) if i == id);
}

#[test]
fn parses_admin_kick_reason() {
    let reason = "multi word reason";
    let input = format!("/kick {reason}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(res, Ok(AdminCommand::Kick(None, Some(r))) if r == reason);
}

#[test]
fn err_on_empty() {
    let input = "/";
    let res = AdminCommand::parse(input, "test");
    assert_matches!(res, Err(ParseError::UnknownCommand(_)));
}

#[test]
fn reason_and_id() {
    let reason = "some reason";
    let id = "123123";
    let input = format!("/ban {id} {reason}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(res, Ok(AdminCommand::Ban(Some(_), Some(r))) if r == reason);
}

#[test]
fn reason_id_and_duration() {
    let reason = "some reason";
    let id = "123123";
    let duration = "10m";
    let input = format!("/mute {id} {duration} {reason}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(
        res,
        Ok(AdminCommand::Mute(Some(_), d, Some(r))) if r == reason && d == Duration::minutes(10)
    );
}

#[test]
fn reason_and_duration_no_id() {
    let reason = "some reason";
    let duration = "10m";
    let input = format!("/mute {duration} {reason}");
    let res = AdminCommand::parse(&input, "test");
    assert_matches!(
        res,
        Ok(AdminCommand::Mute(None, d, Some(r))) if r == reason && d == Duration::minutes(10)
    );
}
