use anyhow::anyhow;
use anyhow::Error;

pub const SEPARATOR: char = ' ';
pub const MAX_DATA_LEN: usize = 64;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CallbackData {
    class: CallbackDataClass,
    command: String,
    args: Vec<String>,
}

impl CallbackData {
    pub fn new(
        class: CallbackDataClass,
        command: String,
        args: Vec<String>,
    ) -> Result<Self, Error> {
        let mut total_len = 2;
        total_len += command.as_bytes().len();
        total_len += args.iter().map(|s| s.as_bytes().len() + 1).sum::<usize>();
        if total_len > MAX_DATA_LEN {
            return Err(anyhow!("Data too long"));
        }
        Ok(Self {
            class,
            command,
            args,
        })
    }

    pub fn class(&self) -> &CallbackDataClass {
        &self.class
    }

    pub fn command(&self) -> &String {
        &self.command
    }

    pub fn args(&self) -> &Vec<String> {
        &self.args
    }
    pub fn is_report(self) -> bool {
        matches!(self.class, CallbackDataClass::Report)
    }
    pub fn is_private_chat(self) -> bool {
        matches!(self.class, CallbackDataClass::PrivateChat)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum CallbackDataClass {
    Report,
    PrivateChat,
}

impl TryFrom<String> for CallbackData {
    type Error = Error;

    fn try_from(data: String) -> Result<Self, Error> {
        let mut split = data.split(SEPARATOR);
        let bytes = split.next().ok_or(anyhow!("Data is empty"))?.as_bytes();
        let class = match bytes.first().ok_or(anyhow!("Data must have class"))? {
            b'r' => CallbackDataClass::Report,
            b'p' => CallbackDataClass::PrivateChat,
            _ => return Err(anyhow!("Unknown data class")),
        };
        let command = split
            .next()
            .ok_or(anyhow!("Data must have command"))?
            .to_string();
        let args = split.map(ToString::to_string).collect();
        Ok(Self {
            class,
            command,
            args,
        })
    }
}

#[allow(clippy::from_over_into)]
impl Into<String> for CallbackData {
    fn into(self) -> String {
        let mut res = String::new();
        let class = match self.class {
            CallbackDataClass::Report => 'r',
            CallbackDataClass::PrivateChat => 'p',
        };
        res.push(class);
        res.push(SEPARATOR);
        res.push_str(&self.command);
        for a in &self.args {
            res.push(SEPARATOR);
            res.push_str(a);
        }
        res
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use assert_matches::assert_matches;

    #[test]
    fn parses_callback_data() -> Result<(), Error> {
        let input = "r ban".to_string();
        let res: CallbackData = input.try_into()?;
        assert_eq!(
            res,
            CallbackData {
                class: CallbackDataClass::Report,
                command: "ban".to_string(),
                args: vec![]
            }
        );
        Ok(())
    }

    #[test]
    fn parses_multibyte_chars() -> Result<(), Error> {
        let input = "r тест с аргументами".to_string();
        let res: CallbackData = input.try_into()?;
        assert_eq!(
            res,
            CallbackData {
                class: CallbackDataClass::Report,
                command: "тест".to_string(),
                args: vec!["с".to_string(), "аргументами".to_string()],
            }
        );
        Ok(())
    }

    #[test]
    fn parses_args() -> Result<(), Error> {
        let input = "r mute 10s".to_string();
        let res: CallbackData = input.try_into()?;
        assert_eq!(
            res,
            CallbackData {
                class: CallbackDataClass::Report,
                command: "mute".to_string(),
                args: vec!["10s".to_string()],
            }
        );
        Ok(())
    }

    #[test]
    fn constructs_callback_string() -> Result<(), Error> {
        let input = CallbackData {
            class: CallbackDataClass::PrivateChat,
            command: "manage".to_string(),
            args: vec![],
        };
        let string: String = input.into();
        assert_eq!(string, "p manage",);
        Ok(())
    }

    #[test]
    fn serialization_invertible() -> Result<(), Error> {
        let input = CallbackData {
            class: CallbackDataClass::PrivateChat,
            command: "manage".to_string(),
            args: vec!["test".to_string()],
        };
        let string: String = input.clone().into();
        let parsed: CallbackData = string.try_into()?;
        assert_eq!(parsed, input);
        Ok(())
    }

    #[test]
    fn checks_total_len() {
        let args = vec!["with", "a", "lot", "of", "different", "arguments"]
            .into_iter()
            .map(ToString::to_string)
            .collect();
        let res = CallbackData::new(
            CallbackDataClass::Report,
            "really_looooooooooong_command".to_string(),
            args,
        );

        assert_matches!(res, Err(_));
    }

    #[test]
    fn accepts_max_len() {
        let args: Vec<_> = vec!["with", "some", "arguments", "to", "test", "length"]
            .into_iter()
            .map(ToString::to_string)
            .collect();
        let res = CallbackData::new(
            CallbackDataClass::Report,
            "not_as_looooooooong_command".to_string(),
            args.clone(),
        );

        assert_matches!(res, Ok(_));

        let res = CallbackData::new(
            CallbackDataClass::Report,
            "not_as_loooooooooong_command".to_string(),
            args,
        );

        assert_matches!(res, Err(_));
    }

    #[test]
    fn checks_total_len_with_multibyte_chars() {
        let args = vec!["with", "a", "lot", "of", "different", "arguments"]
            .into_iter()
            .map(ToString::to_string)
            .collect();
        let res = CallbackData::new(
            CallbackDataClass::Report,
            "длинная команда на русском".to_string(),
            args,
        );

        assert_matches!(res, Err(_));
    }
}
