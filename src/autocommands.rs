use crate::ban_user;
use crate::config::Action;
use crate::config::Config;
use crate::kick_user;
use crate::mute_user;
use anyhow::anyhow;
use anyhow::Error;
use std::sync::Arc;
use teloxide::prelude::*;

async fn perform_action(
    bot: &Bot,
    msg: &Message,
    action: Action,
    config: Arc<Config>,
) -> Result<(), Error> {
    let user = msg.from().ok_or(anyhow!("Message with no sender"))?;
    let text = match action {
        Action::Ban { reason } => ban_user(bot, &msg.chat, user, reason, config).await?,
        Action::Kick { reason } => kick_user(bot, &msg.chat, user, reason, config).await?,
        Action::Answer { text } => Some(text.apply(user)),
        Action::Mute { time, reason } => {
            mute_user(&time, bot, msg.chat.id, user, reason, config).await?
        }
        Action::DeleteMessage { reason } => {
            bot.delete_message(msg.chat.id, msg.id).await?;
            reason
        }
    };
    if let Some(text) = text {
        bot.send_message(msg.chat.id, text).await?;
    }
    Ok(())
}

pub async fn perform_actions(
    bot: Bot,
    msg: Message,
    actions: Vec<Action>,
    config: Arc<Config>,
) -> Result<(), Error> {
    for action in actions {
        perform_action(&bot, &msg, action, config.clone()).await?;
    }
    Ok(())
}

pub fn extract_actions(msg: Message, config: Arc<Config>) -> Option<Vec<Action>> {
    config.autocommands.as_ref().and_then(|c| {
        c.iter()
            .find(|c| c.trigger.matches(&msg))
            .map(|c| &c.actions)
            .cloned()
    })
}
