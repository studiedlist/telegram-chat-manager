use anyhow::Error;
use teloxide::types::ChatId;
use typesafe_repository::async_ops::*;
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use typesafe_repository::GetIdentity;
use typesafe_repository::Identity;
use typesafe_repository::RefIdentity;

#[derive(Id, Clone, Debug)]
#[Id(ref_id, get_id)]
pub struct Raid {
    pub id: ChatId,
}

pub trait RaidRepository:
    Repository<Raid, Error = Error> + Add<Raid> + Get<Raid> + Update<Raid> + Remove<Raid> + Send + Sync
{
}

impl<T> RaidRepository for T where
    T: Repository<Raid, Error = Error>
        + Add<Raid>
        + Get<Raid>
        + Update<Raid>
        + Remove<Raid>
        + Send
        + Sync
{
}
