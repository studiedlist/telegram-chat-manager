{
  description = "nix shell for rust";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; # unstable version of nixpkgs
    # nixpkgs.url = "github:nixos/nixpkgs/release-23.05"; # stable version of nixpkgs
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs =
    inputs @{ self
    , flake-utils
    , nixpkgs
    , ...
    }:
    let
      overlays = [
        # Other overlays
      ];
    in
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { system = system; inherit overlays; };
    in
    {
      formatter = pkgs.nixpkgs-fmt;
      devShells.default =
        pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            rust-analyzer # lsp
            rustc
            rustfmt # formatter
            cargo
            clippy # linter
            pkg-config # locate libraries
            lldb # debugger
            asciidoctor # readme
          ];
          buildInputs = with pkgs; [
            openssl.dev
          ];
        };
      packages = rec {
        telegram-chat-manager = pkgs.callPackage ./pkg.nix { };
        telegram-chat-manager-static = pkgs.pkgsStatic.callPackage ./pkg.nix { };
        default = telegram-chat-manager;
        static = telegram-chat-manager-static;
        docker =
          let
            cfgdir = "/data";
          in
          pkgs.dockerTools.buildLayeredImage {
            name = default.pname;
            tag = default.version;
            config = {
              Cmd = [ "${static}/bin/chat-manager" ];
              WorkingDir = cfgdir;
              Volumes = { "${cfgdir}" = { }; };
            };
          };
      };
    });
}

